package MJL;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

public class XsltXMLToHtmlDOM {
    private static final String MXL_FILE="src/MJL/Demo.xml";
    private static final String XSLT_FILE="src/MJL/demo-staff.xslt";

    public static void main(String[] args) throws Exception{
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        InputStream inputStream = new FileInputStream(MXL_FILE);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(inputStream);
        FileOutputStream fileOutputStream = new FileOutputStream("src/MJL/staff.html");
        transformXML(document,fileOutputStream);
    }

    private static void transformXML(Document document, OutputStream outputStream) throws Exception {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer(new StreamSource(new File(XSLT_FILE)));
        transformer.transform(new DOMSource(document),new StreamResult(outputStream));
    }
}
