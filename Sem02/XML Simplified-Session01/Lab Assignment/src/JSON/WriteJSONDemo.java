package JSON;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class WriteJSONDemo {
    public static void main(String[] args) throws Exception {
        // nv01
        JSONObject employee01 = new JSONObject();
        employee01.put("firstName","Nguyen");
        employee01.put("lastName","Thanh Dat");
        employee01.put("address","HaNoi,VN");

        JSONObject employeeObject01 = new JSONObject();
        employeeObject01.put("fptaptech",employee01);

        //nv02
        JSONObject employee02 = new JSONObject();
        employee02.put("firstName","Nguyen");
        employee02.put("lastName","Phuong Anh");
        employee02.put("address","HaNoi,VN");

        JSONObject employeeObject02 = new JSONObject();
        employeeObject02.put("fptaptech",employee02);

        //add employee to list
        JSONArray employeeList = new JSONArray();
        employeeList.add(employeeObject01);
        employeeList.add(employeeObject02);

        // write to Json
        FileWriter fileWriter = new FileWriter("src/JSON/employees.json");
        fileWriter.write(employeeList.toJSONString());
        fileWriter.flush();
    }
}
