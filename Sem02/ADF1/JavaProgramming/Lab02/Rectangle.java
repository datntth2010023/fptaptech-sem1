import java.util.Scanner;

public class  Rectangle {
    /**
     * Khai bao cac truong dung de luu tru gia tri cua cac thuoc tinh trong lop
     */
    private int width;
    private int height;
    private int area;
    private int perimeter;

    /**
     * Constructor không có đối số
     */
    public Rectangle() {
        // to do
        this.width = 3;
        this.height = 7;
        this.area = 21;
        this.perimeter = 20;
    }

    /**
     * constructor có đối số
     */
    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
        this.area = width * height;
        this.perimeter = (width + height) * 2;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int value) {
        this.width = value;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int value) {
        this.height = value;
    }

    public int getArea() {
        return this.area;
    }

    public int getPerimeter() {
        return this.perimeter;
    }

    public void display() {
        System.out.println("\n Width \t\t Height \t\t Area \t\t Perimeter");
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.printf("\n %d \t\t %d \t\t %d \t\t %d");
    }

    public static void main(String[] args) {
        Rectangle rectangle;

        rectangle = new Rectangle(5, 9);
        rectangle.display();
    }
}