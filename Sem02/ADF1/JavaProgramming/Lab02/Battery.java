/**
 * Write a description of class Battery here.
 * 
 * @author
 * @Version
 */

public class Battery {
    /**
     * Fields
     */
    private int energy;

    /**
     * Constructor for objects of class Battery
     */
    public Battery() {
        // to do:
        energy = 100;
    }

    /**
     * Method
     */
    public void setEnergy(int value) {
        energy = value;
    }

    public int setEnergy() {
        return energy;
    }

    public void decreaseEnergy() {
        energy--;
    }
}
