/**
 * Write a description of class Client here.
 * 
 * @author thdat
 * @version 0.0
 */
public class Client {
    /**
     * Constructor for object of class client
     */
    public Client() {
        //To do
    }

    public void doUnchecked(String value) {
        // can phai check exception, neu khong --> bug
        int result = canThrowUncheckedException(value);
        System.out.println("result="+result);
    }

    private int canThrowUncheckedException(String value) {
        return Integer.parseInt(value);
    }

    public void doChecked() {
        try{
            //BUoc phai check exception o day! khong cach nao khac
            canThrowCheckedException();
            System.out.println("OK");
        } catch (Exception  ex) {
            System.out.println(ex);
        }
    }

    private int canThrowCheckedException() throws Exception {
        throw new Exception("Failure");
    }
}

/**
 * Questions:
 * Distinguishing unchecked Checked Exception and Exception?
 * Trả lời: 
 * - unchecked Checked Exception: là một ngoại lệ xảy ra tại thời điểm thực thi
 * - Checked Exception: là một ngoại lệ được trình biên dịch kiểm tra (thông báo) tại thời điểm biên dịch
 * 
 * - các ngoại lệ đã kiểm tra bị buộc bởi trình biên dịch và được sử dụng để chỉ ra các điều kiện ngoại lệ nằm
 * ngoài tầm kiểm soát của chương trình, trong khi các ngoại lệ không được kiểm tra xảy ra
 * trong thời gian chạy và được sử dụng để chỉ ra lỗi lập trình.
 * 
 * Using Checked Exception and use unchecked Exception?
 * Trả lời : try " khối lệnh " catch ""
 *
 * Why should not catch (Exception ex)?
 */
