class PrintMessage { // Create class PrintMessage
    public static void main(String[] args) {
        System.out.println("Hello, world!"); // print to command screen "Hello, world!"
        System.out.println(); // print to white space
        System.out.print("Hello, world!"); // print to command screen "Hello, world!"
        System.out.println("Hello,"); // print to command screen "Hello,"
        System.out.print(" "); // print a space
        System.out.print("world"); // print to command screen "world"
        System.out.println("Hello, world!"); // print to command screen "Hello, world"
    }
}
