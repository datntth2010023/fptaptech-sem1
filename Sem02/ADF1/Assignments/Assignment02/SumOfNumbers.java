public class SumOfNumbers {
    public static void main(String[] args) {
        int number1 = 10; 
        int number2 = 20;
        int number3 = 32;
        int number4 = 47;
        int number5 = 55;
        int sum;                  // Declare an int variable called sum to hold the sum 
        sum = number1 + number2 + number3 + number4 + number5;
        System.out.print("The sum is ");
        System.out.println(sum);
    }
}