import java.util.Scanner;

class Atlantis{
    
    public static void main(String[] args) {
        // Declare and initialize the variables: Khai báo và khởi tạo các biến
        double temperatureMax, temperatureMin;
        int townPopulation, statePopulation;
        int literate, unLiterate;
        double averageTemperature, literacyPercentage;
        String plane;
        boolean urban;

        // Create the object of Scanner class: Tạo đối tượng của lớp Scanner
        Scanner input = new Scanner(System.in);

        // Nhập nhiệt độ của địa phương
        System.out.println("Enter the local maximum temperature : ");
        temperatureMax = input.nextDouble();
        System.out.println("Enter the local minimum temperate : ");
        temperatureMin = input.nextDouble();

        // Tính nhiệt độ trung bình
        averageTemperature = (temperatureMax + temperatureMin) / 2;

        // Nhập dân số thị trấn và bang;
        System.out.println("Enter the town population: ");
        townPopulation = input.nextInt();
        System.out.println("Enter the state population: ");
        statePopulation = input.nextInt();

    }
} 