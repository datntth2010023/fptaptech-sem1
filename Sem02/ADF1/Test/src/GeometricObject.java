import java.util.Date;
import java.util.Scanner;

public class GeometricObject {

    private String color;
    private boolean filled;
    Date date = new Date();

    public GeometricObject(String color, boolean filled){
        this.color = color;
        this.filled = filled;
    }

    public GeometricObject() {

    }

    public void setColor(String color) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter your color: ");
        String color = input.nextLine();
        System.out.println("Your color is: " + color);
    }
    public String getColor() {
        return color;
    }

    public void setFilled() {
        Scanner input = new Scanner(System.in);
        System.out.println("\nIs the geometry filled ? (Y/N question )");
        String f = input.next();
        boolean filled;
        if( f == "yes"){
            filled = true;
        }else if( f == "no"){
            filled = false;
        }else{
            System.out.println("Invalid answer !");
        }

    }
    public boolean isFilled() {
        return filled;
    }

    public void getDateCreated() {
        System.out.println("The current time is :" +date.toString());
    }

    public  double getArea() {
        return 0;
    }

    public double getPerimeter() {
        return 0;
    }
}

