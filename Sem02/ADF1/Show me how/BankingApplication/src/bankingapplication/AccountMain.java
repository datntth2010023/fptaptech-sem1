/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankingapplication;
import java.util.Scanner;
import java.util.Base64;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author thdat
 */
public class AccountMain {
    
    // Store hard-coded values in the respective  variables
    static String userId = "admin";
    static String password = "Admin@123";
    
    //Method to encode the password 
    public static String encodePassword()
    {
        String base64encodePassword="";
        //logic for encoding
        try
        {
            base64encodePassword = Base64.getEncoder().encodeToString(password.getBytes("utf-8"));
            System.out.println("Base64 Encoded String (Basic) :" + base64encodePassword);
        }
        catch(UnsupportedEncodingException e)
        {
            System.out.println("Error: " +e.getMessage());
        }
    return base64encodePassword;
    
    }
    
    //Method to encode the password
    public static String decodePassword()
    {
        
        String encodePassword = encodePassword();
        String base64DecodedPassword = new String();
        //logic for decoding
        try
        {
            byte[] decodePassword = Base64.getDecoder().decode(encodePassword);
            
            System.out.println("Original String: " + new String(decodePassword, "utf-8"));
            base64DecodedPassword = new String(decodePassword,"utf-8");
        }
        catch(UnsupportedEncodingException e)
        {
            System.out.println("Error: " + e.getMessage());
        }
        
        return base64DecodedPassword;
    }
    
    //Method to variables the password
    public static boolean validatePassword(Account objAccount)
    {
        boolean output;
        String decodedPassword = decodePassword();
        String inputPassword = objAccount.getPassword();
        output = decodedPassword.equals(inputPassword);
        return output;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //TODO code application logic here
        String inPassword;
        Scanner input = new Scanner(System.in);
        Account objAccount = new Account();
        
        objAccount.setUserId(userId);
        System.out.println("Enter the password: ");
        inPassword = input.nextLine();
        objAccount.setPassword(inPassword);
        if (validatePassword(objAccount))
        {
            System.out.println("Admin password is successfully validated");
            
            System.out.println(objAccount.toString());
        }
        else
        {
            System.out.println("Invalid password");
        }
    }
}
