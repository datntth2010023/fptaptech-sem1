
public class EvenNumbers {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        //Declare variables
        int i = 0, sum = 0;

        // Iterates until the sum of first 10 even numbers is computed
        //Lặp lại cho đến khi tính được tổng của 10 số chẵn đầu tiên
        while(i <= 20) {
            //check if the value contained by variables i is an even value
            // kiểm tra xem giá trị chứa trong biến i có phải là giá trị chẵn không
            if (i % 2 == 0){
                sum += i;
            }

            // Increments the counter variables by i : Tăng các biến bộ đếm lên i 
            i ++;
        } //End of the while loop

        //Display the sum of even numbers on the console
        System.out.println("Sum of first 10 even numbers is: " + sum);
    }
}
