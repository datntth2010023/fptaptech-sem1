import java.util.Scanner;

public class Factorial
{
    /**
     * @param args the command line arguments : các đối số dòng lệnh 
     */
    public static void main(String[] args) {
        //Declare variables
        int num;
        long fact = 1;

        //Create the object of Scanner class : Tạo đối tượng của lớp Scanner
        Scanner input =new Scanner(System.in);

        //Accept the value for the num variable :Chấp nhận giá trị cho biến num
        System.out.println("Enter the number for factorial calculation: ");
        num = input.nextInt();

        // Iterates the loop until num evaluates to false : Lặp lại vòng lặp cho đến khi num đánh giá thành false
        do {
            fact = fact * num;
            num--;
        } while(num >= 1);
        //Display the factorial on the console 
        System.out.println("Factorial of a Number: " + fact);
    }
}