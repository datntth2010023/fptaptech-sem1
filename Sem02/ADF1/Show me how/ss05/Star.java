import java.util.Scanner;

public class Star {
    /**
     * @param args the command line arguments : các đối số dòng lệnh 
     */
    public static void main(String[] args)
    {
        //Declare variables
        int row;

        // Create the object of Scanner class: tao doi tuong cua lop Scanner
        Scanner input = new Scanner(System.in);
        
        //Accept value for the row variables: chap nhan gia tri cua row 
        System.out.println("Enter the maximum number of rows for displaying stars:  ");
        row = input.nextInt();

        // Iterates outer-for loop until the specified rows of stars are displayed :Lặp lại vòng lặp ngoài for cho đến khi các hàng dấu sao được chỉ định được hiển thị
        for (int i = 1; i <= row; i++) {

            //Iterates for loop to display spaces on the console 
            for (int space = row ; space >= i; space --){
                System.out.print("* ");
            }

            //Takes cursor on the next line
            System.out.println();
        } // End of the outer-for loop
    }
}
