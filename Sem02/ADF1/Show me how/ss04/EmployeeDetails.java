
import java.util.Scanner;

public class EmployeeDetails {
    /**
     * @param args the command line arguments :args các đối số dòng lệnhpả
     */
    public static void main(String[] args) {
        // Declare variables: khai báo biến
        int employeeID;
        String employeeName;
        char gender;

        // Create an instance of Scanner class : Tạo một phiên bản của lớp Máy quét
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the Employee Identification Code: ");
        employeeID = scan.nextInt();

        // Validates employeeID variable : Xác thực biến ID của nhân viên
        if (employeeID > 0) {
            System.out.println("Enter the Employee Name: ");
            Scanner s = new Scanner(System.in);
            employeeName = s.nextLine();

            // Validates employeeName variables: Xác thực tên của nhân viên
            if (employeeName.length() < 40) {
                System.out.println("Enter the gender :[M/F] ");
                gender = scan.next("\\D").charAt(0);

                // Validates gender variables: xác thực giới tính của nhân viên
                if (gender == 'M' || gender == 'F') {
                    System.out.println("Employee Code: " + employeeID);
                    System.out.println("Employee Name: " + employeeName);
                    System.out.println("Employee Gender: " + gender);
                } else {
                    System.out.println("Incorrect Entry for Gender.");
                } 
            }// End of inner-if statement : Kết thúc câu lệnh internal-if
            else {
                System.out.println("Incorrect Entry for Employee Name.");
            }
        }//End of inner-if statement
        else {
            System.out.println("Incorrect Entry for Employ Identification Code.");
        }
    }
}