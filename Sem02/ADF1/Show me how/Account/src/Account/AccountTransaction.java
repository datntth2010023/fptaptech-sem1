/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Account;

import java.util.Scanner;

/**
 *
 * @author thdat
 */
/**
 * To change this license header, choose license Headers in Project properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class AccountTransaction {
// Create the variables and initialize the balance to 25000
    String accountNumber;
    double balance = 25000;
    
    /**
     * @param args the command line arguments 
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        double amountIn;
        int choice;
        
        IBankingValidation validationDeposit, validationWithdraw;// create references for BankingValidation interface
        IBankingOperation canWithdrawMoney, canDepositMoney;// create reference for BankingOperation interface
        
        Scanner input = new Scanner(System.in); // create object for Scanner to get the input 
        AccountTransaction objAccountTrans = new AccountTransaction();//Create the object for AccountTransaction class
            validationWithdraw=(bal) -> bal<=200;// Lambda expression validate is performed  
            validationDeposit=(bal) -> bal >10000 ;// Lambda expression validate is performed 
        
        canWithdrawMoney = (aa,amount)-> {return objAccountTrans.balance-amount;};//lambda expreession operation is perdormed 
        canDepositMoney = (aa,amount)->{return objAccountTrans.balance+amount;}; // lambda expression operation is performed
        System.out.println("Enter the account number");
        objAccountTrans.accountNumber = input.nextLine();// enter the account number
        System.out.println("Enter the option");
        System.out.println("1: Deposit Money");
        System.out.println("2: Wirthdraw Money");
        choice=input.nextInt();//Enter the choise

        if(choice==1)
        {
            System.out.println("Enter the amount to be deposited");
            amountIn = input.nextDouble();//enter the amount
            if(validationDeposit.validate(amountIn))//validate the deposit amount it should be less than 10000
            {
                System.out.println("Unable to perform operation deposit is more than 10000");
            
            }
            else
            {
                System.out.println("Account Number: "+objAccountTrans.accountNumber + " Balance after Deposit: " + objAccountTrans.transaction(canDepositMoney, amountIn,objAccountTrans));
            
            }
        }
        else if(choice==2)
        {
            System.out.println("Your avaiable balance is: "+objAccountTrans.balance);
            System.out.println("Enter the amount to be withdrawn");
            amountIn= input.nextDouble();// enter the amount
            double bal=objAccountTrans.transaction(canWithdrawMoney,amountIn,objAccountTrans);
            if(validationWithdraw.validate(bal))//validate the balance, it  should not be less than 200 after withdrawal
            {
                System.out.println("Unable to perform operation as after withdrawal balance would be less than 200 and your current is: " +bal);
                
            }
            else
            {
                System.out.println("Account Number: " + objAccountTrans.accountNumber + "Balance after withdrawal: "+objAccountTrans.transaction(canWithdrawMoney,amountIn,objAccountTrans));
            }
            
        }
        else
        {
            System.out.println("invalid Choice");
        }
    }
    
    @FunctionalInterface
    interface IBankingOperation//create a functional interface
    {
        double operate(AccountTransaction aa, double amount);
    
    }
    
    @FunctionalInterface
    interface IBankingValidation // acreate a functional interface
    {
        boolean validate(double amount);//abstract method
    }  
    
    public double transaction(IBankingOperation operation,double amount, AccountTransaction a)
    {
        return operation.operate(a,amount);//abstract method
    }       
}
