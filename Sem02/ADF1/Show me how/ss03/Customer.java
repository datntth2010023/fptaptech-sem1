
import java.util.Scanner;

public class Customer {
    public static void main(String[] args){
        //Declare the variables
        long customerCode;
        String customerName;
        int customerAge;
        double depositAmount;

        System.out.println("Enter the details for the new customer.");
        // Create the object of Scanner class to accept values: Tạo đối tượng của lớp Scanner để chấp nhận các giá trị
        Scanner input = new Scanner(System.in);

        //Accept values and initialize the variables: Chấp nhận giá trị và Khởi tạo các biến
        System.out.println("Enter the Four-digit Customer Code: ");
        customerCode = input.nextLong();
        System.out.println("Enter the Cusomer Fist Name: ");
        customerName = input.next();
        System.out.println("Enter the Customer Age: ");
        customerAge = input.nextInt();
        System.out.println("Enter the Amount Deposited: ");
        depositAmount = input.nextDouble();

        // Display the value of variables
        System.out.println("\nCustomer \t\t CustomerName \t\t CustomerAge \t\t DepositAmount");
        System.out.println("------------------------------------------------------" + "---------------------------------");

        System.out.format("%08d \t\t\t", customerCode);
        System.out.format("%s \t\t\t", customerName);
        System.out.format("%d \t\t\t", customerAge);
        System.out.format("%.2f\n", depositAmount);
    }
}
