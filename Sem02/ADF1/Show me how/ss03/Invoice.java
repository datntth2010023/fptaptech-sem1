
import java.util.Scanner;

public class Invoice {
    public static void main(String[] args) {
        // Declare and initialize the variables: khai báo và khởi tạo các biến
        int orderedShirts, orderedTrousers, points = 0;
        int priceOfShirt = 300;
        int priceOfTrouser = 700;
        int costOfShirts, costOfTrousers, totalCost;
        double discount, netPay;

        // Create the object of Scanner class: Tạo đối tượng của lớp Scanner
        Scanner input = new Scanner(System.in);

        // Accepts the order details of the customer: Chấp nhận thông tin đơn hàng của
        // khách hàng.
        System.out.println("Enter the number of Shirts to order: ");
        orderedShirts = input.nextInt();
        System.out.println("Enter the number of Trousers to order: ");
        orderedTrousers = input.nextInt();

        // Computes the total price of the order: Tính tổng giá của đơn đặt hàng
        costOfShirts = priceOfShirt * orderedShirts;
        costOfTrousers = priceOfTrouser * orderedTrousers;
        totalCost = costOfShirts + costOfTrousers;

        // Gives a discount only if the total cost exceeds $3000: Giảm giá chỉ khi tổng
        // chi phí vượt quá $ 3000.
        discount = totalCost > 3000 ? totalCost * 10 / 100 : 0;
        netPay = totalCost - discount;

        // Give a discount only if the netPay as points if it exceeds $3000 : Chỉ giảm
        // giá nếu netPay dưới dạng điểm nếu vượt quá $ 3000
        points = (int) (netPay > 3000 ? netPay / 100 : 0);

        // Display the invoice details
        System.out.println("\nItem \t\t Quantity \t  Price \t Total");
        System.out.println("--------------------------------------------" + "---------------------------------");
        System.out.printf("Shirts \t\t %d \t\t %d \t\t %d \n", orderedShirts, priceOfShirt, costOfShirts);
        System.out.printf("Trouser \t %d \t\t %d \t\t %d \n", orderedTrousers, priceOfTrouser, costOfTrousers);
        System.out.printf("Discount \t \t\t \t\t %.2f \n", discount);
        System.out.println("--------------------------------------------" + "---------------------------------");
        System.out.printf("Net Total \t \t\t \t\t %.2f \n", netPay);
        System.out.println("--------------------------------------------" + "---------------------------------");
        System.out.printf("Points Earned \t %d \n", points);
    }
}
