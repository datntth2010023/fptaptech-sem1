/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accountpkg;

import bankpkg.Bank;
import java.util.Scanner;

/**
 *
 * @author thdat
 */
public class TestAccount {

    public static void main(String[] args) {
        try {
            String[] accountDetails = new String[5];
            String[] bankDetails = new String[3];

            Scanner input = new Scanner(System.in);

            System.out.print("Enter Account Number: ");
            accountDetails[0] = input.nextLine();
            System.out.print("Enter Name: ");
            accountDetails[1] = input.nextLine();
            System.out.print("Enter Account Type: ");
            accountDetails[2] = input.nextLine();
            System.out.print("Enter Balance: ");
            accountDetails[3] = input.nextLine();
            System.out.print("Enter Number of Days: ");
            accountDetails[4] = input.nextLine();

            System.out.print("Enter Bank ID :");
            bankDetails[0] = input.nextLine();
            System.out.print("Enter Bank Name:");
            bankDetails[1] = input.nextLine();
            System.out.print("Enter Branch :");
            bankDetails[2] = input.nextLine();

            System.out.println("---------------------------------------");
            SavingsAccount objAccount1 = new SavingsAccount(accountDetails[0],
                    accountDetails[1], accountDetails[2],
                    Double.parseDouble(accountDetails[3]),
                    Integer.parseInt(accountDetails[4]));

            /**
             * Account objAccount1 = new Account();
             * objAccount1.addDetails(accountDetails);
             */
            objAccount1.displayDetails();
            System.out.println("----------------------------------------");

            Bank objBank1 = new Bank();
            objBank1.addBankDetails(bankDetails);
            objBank1.displayBankDetails();

            objAccount1.checkBalance(accountDetails[0]);

            System.out.print("Enter Deposit Amount: ");
            double deposit = Double.parseDouble(input.nextLine());

            objAccount1.depositCash(accountDetails[0], deposit);

            System.out.print("Enter Withdrawal Amount: ");
            double withdraw = Double.parseDouble(input.nextLine());

            objAccount1.withdrawCash(accountDetails[0], withdraw);
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Error: Array Index Out of Bounds");
        }catch (NumberFormatException ex) {
            System.out.println("Error: Illegal Argument, " + ex.getMessage());
        }
    }
}
