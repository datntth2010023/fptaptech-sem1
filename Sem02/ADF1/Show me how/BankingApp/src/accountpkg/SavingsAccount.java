/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accountpkg;

import bankpkg.ITransaction;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author thdat
 */
public class SavingsAccount extends Account implements ITransaction {

    private double dailyInterest;
    private int daysOfYears;

    public SavingsAccount(String accountNumber, String accountHolderName,
            String accountType, double balance, int dayOfYear) {

        super(accountNumber, accountHolderName, accountType, balance);
        this.daysOfYears = dayOfYear;
    }

    public double calculateInterest() {
        dailyInterest = (super.getBlance() * bankpkg.Bank.interestRate / 100)
                / daysOfYears;
        
        return new Formatter().decimalFormat(dailyInterest);

        DecimalFormat twoDForm = new DecimalFormat("#.00");

        return Double.valueOf(twoDForm.format(dailyInterest));
    }

    @Override
    public void displayDetails() {

        super.displayDetails();

        System.out.println("Dauly Interest is: $" + calculateInterest());
        System.out.println("-------------------------------------------");
    }

    @Override
    public void checkBalance(String accountNumber) {
        System.out.println("---------------------------------------------");
        System.out.println("Availbable balance: $" + super.getBalance());
        System.out.println("---------------------------------------------");
    }

    @Override
    public void depositCash(String accountNumber, double amount) {
        super.setBalance(super.getBalance() + amount);

        Calendar objNow = Calendar.getInstance();

        SimpleDateFormat objFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        System.out.println("Date-Time: " + new Formatter().formatDate());
        System.out.println("Amount deposited: $" + amount);
        System.out.println("Balaance after deposit: $" + super.getBalance());
        System.out.println("---------------------------------------------");
    }

    /**
     *
     * @param accountNumber
     * @param amount
     */
    class Formatter {

        public String formatDate() {
            Calendar objNow = Calendar.getInstance();

            SimpleDateFormat objFormat = new 
                    SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            
            return objFormat.format(objNow.getTime());
        }
    }
    
    public double decimalFormat(double value) {
        DecimalFormat twoDForm = new DecimalFormat("#.00");
        
        return Double.valueOf(twoDForm.format(value));
    }

    @Override
    public void withdrawCash(String accountNumber, double amount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
