/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankpkg;

/**
 *
 * @author thdat
 */
public interface ITransaction {
    
    public void checkBalance (String accountNumber);
    public void depositCash(String accountNumber, double amount);
    public void withdrawCash(String accountNumber, double amount);
}
    