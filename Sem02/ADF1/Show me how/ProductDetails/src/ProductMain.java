/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import static java.util.Comparator.comparing;
import java.util.DoubleSummaryStatistics;
import java.util.Map;
import java.util.Optional;
import static java.util.stream.Collectors.*;
import java.util.stream.Collectors;
/**
 *
 * @param args the command line arguments
 */
public class ProductMain {
    private static boolean flag = false;
    private static int size;
    private static String value;
    private static String productdata;
    public static void main(String[] args) throws IOException {
        
        List<String> productId = new ArrayList<String>();
        List<String> productName = new ArrayList<String>();
        List<String> brandName = new ArrayList<String>();
        List<String> productPrice = new ArrayList<String>();
       //TODO code application logic here
       //Using BufferedReader to get the input from the console
       BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
       //create ArrayList to store the products
       List<Product> list = new ArrayList<Product>();
       do
       {
           System.out.println("Enter the number of Products[Only enter numeric values]");
           value = input.readLine();
       }while (value.matches("[a-zA-z]*\\d+.*") == false);
       size = Integer.parseInt(value);
       System.out.println("Enter the details below using (,) seperator in the order of ProductID,ProductName,BrandName,ProductPrice");
       
       for (int i = 0; i < size; i++)
       {   
           try
           {
               productdata = input.readLine();
               String[] data = productdata.split(",");
               productId.add(data[0]);
               productName.add(data[1]);
               brandName.add(data[2]);
               productPrice.add(data[3]);
           }
           catch (IOException ex)
           {
               System.out.println(ex.getMessage());
           }
       
    }
    for (int i = 0; i < productName.size(); i++)
    {
        list.add(new Product(Integer.parseInt(productId.get(i)),productName.get(i), brandName.get(i), Double.parseDouble(productPrice.get(i))));
    }
          classifiedByPrice(list);//
          classifiedByProduct(list);//
          classifiedByBrand(list);//
          //
          filteringByProductName(input, productName, productId, brandName, productPrice);
    }
    
    //
    public static void classfiedByPrice(List<Product>list)
    {
        List<Double>priceList;//
        //
        //
        //
        priceList = list.stream().map(Product::getProductPrice).collect(Collectors.toList());
        //
        //
        //
        DoubleSummaryStatistics price = priceList.stream().mapToDouble((x) ->x).summaryStatistics();
        System.out.println("Maximum price in the List: " + price.getMax());
        System.out.println("Minimum price in the List: " + price.getMin());
        System.out.println("Sum of all products price: " + price.getSum());
        System.out.println("Average of all products prices: " + price.getAverage());                
    }
    //
    public static void classifiedByBrand(List<Product>list)
    {
        System.out.prinln("Display the average of price by Brand");
        //
        //
        //
        Map<String, Double> brandByPriceToAverage = list.stream().collect(groupingBy(Product::getBrandName,averagingDouble(Product::getProductPrice)));
        //
        for (Map.Entry<String,Double>me:brandByPriceToAverage.entrySet())
        {
            String pp = me.getKey();
            System.out.println(pp);
            System.out.println(me.getValue());
        }
            System.out.println("Display the Sum of price by Brand");
            //
            Map<String,Double>brandToSum = list.stream().collect(groupingBy(
            Product::getBrandName,summingDouble(Product::getProductPrice)));
            System.out.println(brandToSum);
    }
    //
    public static void classifiedByProduct(List<Product>list)
    {
        List<String>productname;
        List<Double>productccount;
        productname = list.stream().map(Product::getProductName).collect(Collectors.toList());
        productccount = list.stream().map(Product::getProductPrice).collect(Collectors.toList());
        DoubleSummaryStatistics productCount = productcount.stream().mapToDouble((x) ->x).summaryStatistics();
        System.out.println("The given Products name are: " + productname);
        System.out.println("Count of Product: " + productCount.getCount());
        System.out.println("Display the Products names with maximum price ");
        
        //
        //
        Map<String,Optional<Product>>productToHighestPrice = list.stream().collect(groupingBy(comparing(Product::getProductName, maxby(Product::getProductPrice))));
        //
        for(Map.Entry<String,Optional<Product>>me:productToHeghestPrice.entrySet())
        {
            String pp = me.getKey();
            Optional<Product> p = me.getValue();
            System.out.println(pp);
            System.out.println(p.get().getproductPrice());
        }            
    }
   public static void filteringByProductName(BufferedReader input,List<String>ProductName,List<String>productId,List<String>productPrice) throw IOException {
       System.out.println("Enter the Product Name you want to search: ");
       String search = input.readLine();
       for (int i = 0; i < productName.size(); i++) {
           String curVal = productName.get(i);
           if (curVal.contains(search)) {
               System.out.println(productId.get(i) + " " + productName.get(i) + " " + brandName.get(i) + " " + productPrice.get(i));
               flag = true;
           }
       }
       if (flag == false)
       {
           System.out.println("The Product Name is not available");
       }    
   }
}
    
