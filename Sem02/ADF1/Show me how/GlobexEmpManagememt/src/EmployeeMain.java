import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// import the new Date and Time classes
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
public class EmployeeMain {
    /**
     * @param hm
     */
    public static void main(String[] args) {
        // TODO auto-generated method stub
        try {
            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));//Create an instance of BufferedReader class to read from the console
            Map<String, LocalDate> hm = new HashMap<String, LocalDate>();//Create a HasMap instance to store employee id and date of birth
            System.out.println("Enter the total number of employee");
            int size = Integer.parseInt(input.readLine());
            for (int i = 0; i < size; i++) 
            {
                System.out.println("Enter the employee id");
                String empId = input.readLine();
                System.out.println("Enter the date of birth in this format dd/MM/yyyy");
                String strDOB = input.readLine();
                //Use DateTimeFormatter to format the given input date into specific format
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                LocalDate dob = LocalDate.parse(strDOB,formatter);//Convert the formatted string back into date
                hm.put(empId,dob);// Add the employee empId and dob into the Hashmap list
            }
            retirementList(hm); //Invoke the method retirementList with the Hashnap list
        } catch (IOException ex)
        {
            System.out.println(ex.getMessage());
        }
    }
    
    public static void retirementList(Map<String, LocalDate> hm)
    {
        LocalDate currentDate = LocalDate.now();//Retrive the current date 
        //Display the current date
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println("Current Date is:" + currentDate.format(formatter));
        LocalDate dateOfBirth; // Create a variables of type Localdate named dateOfBirth
        String empId;
        List<String> list = new ArrayList<String>();
        System.out.println("Retirement List");
        //For every employee in the list, perform this iteration
        for (Map.Entry<String, LocalDate> me : hm.entrySet())
        {
            empId = me.getKey();
            dateOfBirth = me.getValue();
            Period diff = Period.between(dateOfBirth,currentDate);//Period class is used to retrieve an amount of time with date-based values
            int diff1 = Period.between(dateOfBirth, currentDate).getYears();// Retrieve the years
            int dif4 = 66 -(int) diff1;
            int diff2 = Period.between(dateOfBirth,currentDate).getMonths();//Retrieve the months
            int dif6 = 12 -diff2;
            int age = diff.getYears();//Retrieve the age of employee
            if (age >66)// Check age is above 66
            {
                list.add(empId); //stored inlist
                System.out.println("Emp Id:" + empId + "retired already");
            }
            else
            {
                System.out.println("Emp Id: " + empId + " has " + dif4 +" years " + dif6 +" months for retirement ");               
            }
        }
    }
}
