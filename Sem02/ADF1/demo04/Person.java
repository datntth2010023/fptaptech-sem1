//Comment tren 1 line
/*
*Description pf program 
*@Author: Dat
*@version:V1.0
*/

public class Person {
    //This construction of class
    public Person(String n, int a){
        name = n;
        age = a;
    }
    //Field
    private String name;
    private int age; 

    /*
    *Methods
    */
    public String sayHello(){
        return "Hello world!";
    }
    public String getName(){
        return name;
    }
    public int getAge(){
        return age;
    }
    
    public static void main(String args[]){
        // cap phat vung nho Heap. Tao ra mot doi tuong moi
        Person person1 = new Person("Thanh Dat",18);
        Person person2 = new Person("Victoria",18);
        String message = person1.sayHello();
        String nameData = person1.getName();
        int ageData = person2.getAge();
        System.out.println(message);
        System.out.println(nameData);
        System.out.println(ageData);
    }
}
