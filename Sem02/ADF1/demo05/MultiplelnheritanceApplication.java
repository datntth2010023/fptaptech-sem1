package demo05;

import javax.crypto.spec.ChaCha20ParameterSpec;

class ParentClass{
    int a;
    void setData(int a){
        this.a = a;
    }
}
class ChildClass extends ParentClass{
    void showDataChild() {
        System.out.println("In ChildClass");
    }
}

class childChildClass extends ChildClass {
    void display() {
        System.out.println("In ChildChildClass");
    }
}

public class MultiplelnheritanceApplication {
    public static void main(String[] args) {
        childChildClass childChildClass = new childChildClass();

    }
}
