class ParentClass {
    int a;

    void setData(int a) {
        this.a = a;
    }

}

class ChildClass1 extends ParentClass {
    void showDataClass1() {
        System.out.println("ChildClass1");
        System.out.println("Value of a is: " + a);
    }
}

class ChildClass2 extends ParentClass {
    void showDataClass2() {
        System.out.println("ChildClass2");
        System.out.println("Value of a is: " + a);
    }
}

public class HierarchildInhertanceApplication {
    public static void main(String[] args) {
        ChildClass1 childClass1 = new ChildClass1();
        childClass1.setData(30);
        childClass1.showDataClass1();

        ChildClass2 childClass2 = new ChildClass2();
        childClass2.setData(12);
        childClass2.showDataClass2();
    }
}
