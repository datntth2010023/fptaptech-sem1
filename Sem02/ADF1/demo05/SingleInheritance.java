class ParentClass {
    int a;
    /**
     * This Keyword:
     * 1.tham chieu toi instance of class hien tai
     * 2.this() goi constructor cua lop hien tai
     * 3.truyen tham so vao phuong thuc , constructor
     * 4.tra ve intance hien tai
     */
    void setData(int a) {
        this.a = a;
    }
}
class ChildClass extends ParentClass {
    void showDataChild(){
        System.out.println("Value of a is: " + a);
    }
}

public class SingleInheritance {
    public static void main(String[] args) {
        ChildClass childClass = new ChildClass();
        childClass.setData(30);
        childClass.showDataChild();
    }
}