﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company
{
    class Product
    {
        static void Main(string[] args) 
        {
            Object objProductID;
            Object objProductName;
            Object objPrice;
            Object objQuantity;

            Console.Write("Enter the id of Product : ");
            objProductID = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the name of product : ");
            objProductName = Console.ReadLine();

            Console.Write("Enter price : ");
            objPrice = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter quantity : ");
            objQuantity = Convert.ToInt32(Console.ReadLine());

            int productID = (int)objProductID;
            string productName = (string)objProductName;
            double price = (double)objPrice;
            int quantity = (int)objQuantity;
            double amtPayable = (int)objQuantity * price;

            // Displaying the details of the product
            Console.WriteLine("\nProduct Details : ");
            Console.WriteLine("Product ID : " + productID);
            Console.WriteLine("Product Name : " + productName);
            Console.WriteLine("Price : $" + price);
            Console.WriteLine("Quantity : " + quantity);
            Console.WriteLine("Amt payable {0:F2} : ", amtPayable);
        }
    }
}
