﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School
{
    /// <summary>
    /// 
    /// The program demostrates the use of operators.
    /// 
    /// Class Student accepts the name an mark of student in
    /// three diffirent subject and calculates the percentage and schoolarship amount.
    /// </summary>
    class Student
    {
        /// <summary>
        /// The entry point of the application
        /// </summary>
        /// <param name="args">A list of command line arguments</param>
        static void Main(string[] args)
        {
            string studentName;
            int english, maths, science;
            float percent = 0, amount = 0;

            Console.Write("Enter the name of student : ");
            studentName = Console.ReadLine();
            Console.Write("Enter the markd in English : ");
            english = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the markd in Maths : ");
            maths = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the markd in Science : ");
            science = Convert.ToInt32(Console.ReadLine());

            if ((english >= 0 && english <= 100) && (maths >= 0 && maths <= 100) && (science >= 0 && science <= 100))
            {
                if (english >= 35 && maths >= 35 && science >= 35)
                {
                    percent = ((english + maths + science) * 100) / 300;

                    if (percent > 75)
                        amount = 1500;
                    else if (percent >= 60 && percent <= 75)
                        amount = 1000;
                    else
                        amount = 0;

                    Console.WriteLine("\nStudent Name : " + studentName);
                    Console.WriteLine("Total Marks : " + (english + maths + science));
                    Console.WriteLine("Percentage : " + percent + "%");

                    Console.WriteLine("Schoolarship Amount : " + amount + "$");
                }
                else
                {
                    Console.WriteLine("\nStudent Name : " + studentName);
                    Console.WriteLine("Result : Failed ");
                }
            }
            else 
            {
                Console.WriteLine("Invailid entry of marks");
            }
        }
    }
}
