﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School02
{
    class XMLComments
    {
        /// <summary>
        /// <c>Main</c> method is the entry point for a C# application
        /// <param name="args">A list of command line  arguments can be provided
        /// using <c>args</c></param>
        /// <return>Here the return type of Main method is void</return>
        /// </summary>
        /// <remarks>
        /// Main method can be declared with or without parameters.
        /// </remarks>
        static void Main(String[] args)
        {
            /* The WriteLine method is used to print the specified value.
             * The WriteLine method is belongs to the Console class in the 
             * System namespace.
             */
            Console.WriteLine("This program illutrates XML Comments");
        }
    }
}
