﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// The namespace school03 contains the class XMLComments and  the Main method 
namespace School03
{
    /// <summary>
    /// This program demostrates the input and output operations
    /// 
    /// Class Student accepts input from the user, calculates percentage and displays 
    /// </summary>
    /// <remarks>
    /// Students class aslo demonstrates the use of constants and literals.
    /// </remarks>
    class Student
    {
        /// <summary>
        /// The entry point for the application.
        /// </summary>
        /// <param name="args">A list of command line arguments</param>
        static void Main(string[] args)
        {
            // Declared integer constant to store value 100
            const int percentConst = 100;

            // Declared variables to store the student name
            string studentName;

            // Declared variables to store the student marks 
            int english, maths, science;

            // Declared and initializing variable to store the percentage
            float percent = 0.0F;

            //Accepting the details of the student
            Console.Write("Enter the name of the student : ");
            studentName = Console.ReadLine();

            Console.Write("Enter the marks for english : ");
            english = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the marks for maths : ");
            maths = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the marks for science : ");
            science = Convert.ToInt32(Console.ReadLine());

            // Calculating the percentage
            percent = ((english + maths + science) * percentConst) / 300;

            // displaying the details of the student
            Console.WriteLine();
            Console.WriteLine("*** Student Details ***");
            Console.WriteLine("Student Name : " + studentName);
            Console.WriteLine("Mark obtained in English : {0}", english);
            Console.WriteLine("Mark obtained in Maths : {0}", maths);
            Console.WriteLine("Mark obtained in Science : {0}", science);
            Console.WriteLine("Percent : {0:F2}", percent);
        }
    }
}
