﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School
{
    /// <summary>
    /// Student class stores and display the details of students 
    /// using differents data types.
    /// </summary>
    class Student
    {
        /// <summary>
        /// The entry point for the application.
        /// </summary>
        /// <param name="args">A list of command line arguments</param>
        static void Main(string[] args)
        {
            // Declared and initialising variables to store student details.
            int studentID = 1;
            string studentName = "David Geagre";
            byte age = 18;
            char gender = 'M';
            float percent = 75.50F;
            bool pass = true;

            // Displaying the student details
            Console.WriteLine("Student  ID : {0}", studentID);
            Console.WriteLine("Student Name: {0}", studentName);
            Console.WriteLine("Age : " + age);
            Console.WriteLine("Gender : ", + gender);
            Console.WriteLine("Percentage : {0:F2}", percent);
            Console.WriteLine("Passed : {0}", pass);
        }
    }
}
