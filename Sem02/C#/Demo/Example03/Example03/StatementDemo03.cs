﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example03
{
    class StatementDemo03
    {
        static void Main(String[] args)
        {
            int i = 10, j = 20, k = 5;
            if (i != j & i != k && j != k)
            {
                if ((i > j) && (i > k))
                {
                    Console.WriteLine("So lon nhat la : " + i);
                }
                else if ((i > j) && (i < k))
                {
                    Console.WriteLine("So lon nhat la : " + k);
                }
                else if ((i < j) && (j < k))
                {
                    Console.WriteLine("So lon nhat la : " + k);
                }
                else
                {
                    Console.WriteLine("So lon nhat la : " + j);
                }
            }
            else if ((i == j) || (i == k) || (j == k))
            {
                if ((i == j) && (i > k))
                {
                    Console.WriteLine("So lon nhat la : {0}, {1}", i, j);
                }
                else if ((i == j) && (i < k))
                {
                    Console.WriteLine("So lon nhat la : " + k);
                }
                else if ((i == k) && (i > j))
                {
                    Console.WriteLine("So lon nhat la : {0}, {1}", i, k);
                }
                else if ((i == k) && (i < j))
                {
                    Console.WriteLine("So lon nhat la : " + j);
                }
                else if ((j == k) && (i < j))
                {
                    Console.WriteLine("So lon nhat la : {0}, {1}", j, k);
                }
                else
                {
                    Console.WriteLine("So lon nhat la : " + i);
                }
            }
            else 
            {
                Console.WriteLine("So lon nhat la : {0},{1},{2}", i, j, k);
            }
        }
    }
}
