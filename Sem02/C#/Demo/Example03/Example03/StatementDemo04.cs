﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example03
{
    class StatementDemo04
    {
        static void Main(string[] args) 
        {
            int a, b, c;

            Console.Write("Enter the number : ");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            c = Convert.ToInt32(Console.ReadLine());

            int max = (a > b && b > c) ? a : (b > c ? b : c);
            // Dòng trên có nghĩa là:  Nếu a lớn hơn b và b lớn c Và điều này đúng thì Max là a
            // Nếu điều này sai thì a Nhỏ hơn b và b lớn c thì Max là b
            // Và nếu điều này vẫn sai vì b nhỏ hơn C thi max là c

            Console.WriteLine("Max : " + max);
            
        } 
    }
}
