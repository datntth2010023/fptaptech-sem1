﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example03
{
    class StatementDemo02
    {
        static void Main(String[] args) 
        {
            Console.WriteLine("Moi ban chon so");
            Console.WriteLine("1 Nau an");
            Console.WriteLine("2 Xem phim");
            Console.WriteLine("3 Write Code");
            //string yourID = Console.ReadLine();
            //int option = Convert.ToInt32(yourID);//Int32.Parse(yourID);Convert.ToInt(yourID); 

            int option = int.Parse(Console.ReadLine());
            switch (option)
            {
                case 1:
                    Console.WriteLine("Chuc ngon mieng");
                    break;
                case 2:
                    Console.WriteLine("Xem phim, giai tri cuoi tuan");
                    break;
                case 3:
                    Console.WriteLine("Coder tay to");
                    break;
                default:
                    Console.WriteLine("Di ngu");
                    break;
            }
            Console.ReadLine();
        }
    }
}
