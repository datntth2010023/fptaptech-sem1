﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example10
{
    class Program
    {
        static void Main(string[] args)
        {
            try 
            {
                var number = ReadDouble();
                Console.WriteLine(number);
            }
            catch (Exception ex) 
            when (ex is FormatException || ex is OverflowException)
            { 

            }
        }

        private static double ReadDouble()
        {
            try
            {
                string input = Console.ReadLine();
                var number = Convert.ToDouble(input);
                return number;
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                throw ex;
            }
            catch (OverflowException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                throw ex;
            }
        }
    }
}
