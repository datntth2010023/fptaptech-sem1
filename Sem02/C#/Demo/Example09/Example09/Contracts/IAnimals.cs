﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example09.Contracts
{
    interface IAnimals
    {
        string Name { get; }// read only
        string Weight { get; }
        int FoodEaten { get; }

        void Eat(IFood food);
        void ProduceSound();
        string ToString();
    }
}
