﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example06
{
    class BubbleSort
    {
        static void Main(string[] args) 
        {
            int tempSort;
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];

            for (int i = 0; i < n; i++)
            {
                array[i] = int.Parse(Console.ReadLine());
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = i + 1; j < n; j++)
                {
                    if (array[j] < array[i])
                    {
                        tempSort = array[j];
                        array[j] = array[i];
                        array[i] = tempSort;
                    }
                }
            }
            Console.WriteLine("Bubble Short : ");
            for (int i = 0; i < n; i++) 
            {
                Console.Write(array[i] + "\t");
            }
        }
    }
}
