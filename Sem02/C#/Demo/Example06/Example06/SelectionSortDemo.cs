﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example06
{
    class SelectionSortDemo
    {
        public static void selectionSort(int []array) 
        {
            int n = array.Length;
            for (int i = 0; i < n; i++)
            {
                int min = i;
                for (int j = i + 1; j < n; j++)
                {
                    if (array[j] < array[min])
                    {
                        int temp = array[min];
                        array[min] = array[j];
                        array[i] = temp;
                    }
                }
            }
        }
    }
}
