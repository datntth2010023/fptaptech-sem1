﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example06
{
    class Program
    {
        public static void printfSSortArray(int[] array) 
        {
            SelectionSortDemo selectionSort = new SelectionSortDemo();
            SelectionSortDemo.selectionSort(array);
            int n = array.Length;
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine(array[i]);
                Console.ReadLine();
            }
        }
        public static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];

            for (int i = 0; i < n; i ++)
            {
                array[i] = int.Parse(Console.ReadLine());
            }
            printfSSortArray(array);
        }
    }
}
