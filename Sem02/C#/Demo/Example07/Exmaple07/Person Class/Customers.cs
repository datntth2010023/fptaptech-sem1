﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person_Class
{
    class Customers : Person
    {
        decimal balance;
        public decimal Balance { get; set; }
         


        // Khách hàng có thể rút tiền và kiểm tra số dư
        public static void Withdraw(string[] cmdArgs, Dictionary<int, Customers>
            account)
        {
            int id = int.Parse(cmdArgs[1]);
            decimal amount = decimal.Parse(cmdArgs[2]);
            if (!account.ContainsKey(id))
            {
                Console.WriteLine("Account does not exist!!!");
            }
            else
            {
                account[id].Balance -= amount;
            }
        }


        // Hien thi so du
        public static void Print(string[] cmdArgs, Dictionary<int, Customers>
            account)
        {
            int id = int.Parse(cmdArgs[1]);
            if (!account.ContainsKey(id))
            {
                Console.WriteLine("Account does not exist!!!");
            }
            else
            {
                Console.WriteLine($"Account ID {account[id].Id}," +
                    $" Name {account[id].Name}, Balance {account[id].Balance}");
            }
        }
    }
}
