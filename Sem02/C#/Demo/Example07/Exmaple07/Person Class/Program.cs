﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person_Class
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the choice : ");
            string[] commads = Console.ReadLine()
                .Split(new string[] { " " }, StringSplitOptions.None);

            Dictionary<int, Customers> account = new Dictionary<int, Customers>();
            Dictionary<int, Displays> account1 = new Dictionary<int, Displays>();

            while (commads[0] != "Stop")
            {
                if (commads[0] == "Create")
                {
                    Employees.Create(commads, account);
                }
                else if (commads[0] == "Deposit")
                {
                    Employees.Deposit(commads, account);
                }
                else if (commads[0] == "Withdraw")
                {
                    Customers.Withdraw(commads, account);
                }
                else if (commads[0] == "Print")
                {
                    Customers.Print(commads, account);
                }
                commads = Console.ReadLine()
                    .Split(new string[] { " " }, StringSplitOptions.None);
            }
        }
    }
}
