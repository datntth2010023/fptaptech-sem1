﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person_Class
{
    class Employees : Person
    {
        // Nhân viên có thể tạo tài khoản và thêm tiền, hiển thị thông tin tài khoản kh  vào tài khoản


        // Tạo tài khoản cho khách hàng 
        public static void Create(string[] cmdArgs, Dictionary<int, Customers>
            account)
        {
            int id = int.Parse(cmdArgs[1]);
            if (account.ContainsKey(id))
            {
                Console.WriteLine("Account exist!!!");
            }
            else
            {
                Customers cust = new Customers();
                cust.Id = id;
                account.Add(id, cust);
            }
        }


        // Thêm tiền vào tài khoản cho Khách hàng 
        public static void Deposit(string[] cmdArgs, Dictionary<int, Customers>
            account)
        {
            int id = int.Parse(cmdArgs[1]);
            decimal amount = decimal.Parse(cmdArgs[2]);
            if (!account.ContainsKey(id))
            {
                Console.WriteLine("Account doesn't exist!!!");
            }
            else
            {
                account[id].Balance += amount;
            }
        }


        // Kiểm tra số dư tài khoản khách hàng
        /*public static void Print(string[] cmdArgs, Dictionary<int, Customers>
            account) 
        {
            int id = int.Parse(cmdArgs[1]);
            if (!account.ContainsKey(id))
            {
                Console.WriteLine("Account does not exist!!!");
            }
            else
            {
                Console.WriteLine($"Account ID{account[id].Id}," +
                    $"  Name {account[id].Name}, Balance {account[id].Balance}");
            }
        }*/
    }
}
