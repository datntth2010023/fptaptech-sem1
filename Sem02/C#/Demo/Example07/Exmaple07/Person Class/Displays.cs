﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person_Class
{
    class Displays 
    {
        public static void Print(string[] cmdArgs, Dictionary<int, Customers>
            account)
        {
            int id = int.Parse(cmdArgs[1]);
            if (!account.ContainsKey(id))
            {
                Console.WriteLine("Account does not exist!!!");
            }
            else
            {
                Console.WriteLine($"Account ID{account[id].Id}," +
                    $" Name {account[id].Name}," +
                    $" Address {account[id].Address}" +
                    $" Balance {account[id].Balance}");
            }
        }
    }
}
