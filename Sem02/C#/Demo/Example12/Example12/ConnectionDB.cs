﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Example12
{
    public class ConnectionDB
    {
        public static SqlConnection getConnection() 
        {
            string constr = @"Data source = MSI\SQLEXPRESS; " +
                "initial catalog=Example12;user id = sa;password = sa";
            SqlConnection conn;
            conn = new SqlConnection(constr);

            conn.Open();
            return conn;
        }
    }
}
