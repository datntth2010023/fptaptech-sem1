use Example12

create table customer(
    customerId int primary key,
	first_name varchar(50) not null,
	last_name varchar(50) not null,
)

insert into customer values(1,'Nguyen','Thanh Dat');
insert into customer values(2,'Nguyen', 'Phuong Anh');

select * from customer

