﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example12
{
    public interface ManageData
    {
        public void CreateData() { }
        public void DeleteData() { }
    }
}
