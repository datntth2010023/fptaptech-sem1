﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Example12
{
    public class Program
    {
        static void Main(string[] args)
        {
            Connect();
            DeleteCustomer();
            CreateCustomer();
            UpdateCustomer();
            GetAllCustomer();
            Console.ReadLine();
        }
        static SqlConnection Connect()
        {
            string constr = @"Data source = MSI\SQLEXPRESS; " +
                "initial catalog=Example12;user id = sa;password = sa";
            SqlConnection conn;
            conn = new SqlConnection(constr);

            conn.Open();
            return conn;
        }

        static void GetAllCustomer()
        {
            string constr = @"Data source = MSI\SQLEXPRESS; " +
                "initial catalog=Example12;user id = sa;password = sa";
            SqlConnection conn;
            conn = new SqlConnection(constr);

            conn.Open();

            SqlCommand cmd;

            SqlDataReader dataReader;
            string query = "Select * from customer";
            string output = " ";
            cmd = new SqlCommand(query, conn);

            dataReader = cmd.ExecuteReader();
            while (dataReader.Read())//ResultSet rs; rs.next()
            {
                output = output + dataReader.GetValue(0) + "  " +
                dataReader.GetValue(1) + "  " + dataReader.GetValue(2) + "\n";
            }
            Console.WriteLine(output);

            dataReader.Close();
            cmd.Dispose();
            conn.Close();
        }

        static void CreateCustomer()
        {
            SqlConnection conn1 = Connect();
            SqlCommand cmd;
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            string query = " ";
            query = "insert into customer values(5,'Nguyen','Thanh Nghia')";
            cmd = new SqlCommand(query, conn1);
            sqlDataAdapter.InsertCommand = new SqlCommand(query, conn1);
            sqlDataAdapter.InsertCommand.ExecuteNonQuery();

            cmd.Dispose();
            conn1.Close();

        }

        static void UpdateCustomer() 
        {
            SqlConnection conn = Connect();
            SqlCommand cmd;
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            string query = " ";
            query = "update customer set first_name = 'Vuong' where customerId = 3";
            cmd = new SqlCommand(query, conn);
            sqlDataAdapter.InsertCommand = new SqlCommand(query,conn);
            sqlDataAdapter.InsertCommand.ExecuteNonQuery();
            cmd.Dispose();
            conn.Close();
        }

        static void DeleteCustomer()
        {
            SqlConnection conn = Connect();
            SqlCommand cmd;
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            string query = " ";
            query = "delete from customer where customerid = 4";
            cmd = new SqlCommand(query, conn);
            sqlDataAdapter.InsertCommand = new SqlCommand(query,conn);
            sqlDataAdapter.InsertCommand.ExecuteNonQuery();
            cmd.Dispose();
            conn.Close();
        }
    }
}
