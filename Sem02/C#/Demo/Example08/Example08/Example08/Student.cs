﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example08
{
    public class StudentSystem
    {
        private Dictionary<string, Student> repo;
        //public Dictionary<string, Student> Repo { get; set; }
        public Dictionary<string, Student> Repo
        {
            get { return repo; }
            private set { repo = value; }
        }
        public StudentSystem()
        {
            this.Repo = new Dictionary<string, Student>();
        }
        //Student student = new Student("Hoang Anh", 18, 1);
        public void ParseCommand()
        {
            string[] args = Console.ReadLine().Split();
            if (args[0] == "Create")
            {
                var name = args[1];
                var age = int.Parse(args[2]);
                var grade = double.Parse(args[3]);
                if (!Repo.ContainsKey(name))
                {
                    var student = new Student(name, age, grade);
                    Repo[name] = student;
                }
                else
                {
                    Console.WriteLine("Student doesn't exist!!!");
                }
            }
            else if (args[0] == "Show")
            {
                var name = args[1];
                if (Repo.ContainsKey(name))
                {
                    var student = Repo[name];
                    string view = $"{student.Name} is {student.Age }" +
                        $" years old and grade is {student.Grade}";
                    Console.WriteLine(view);
                }
            }
            else if (args[0] == "exit") 
            {
                Environment.Exit(0);
            }
        }
    }
    public class Student
    {
        private string name;
        private readonly int age;
        private double grade;

        // public string Name {get; set;}
        public double Grade 
        {
            get { return grade; }
            set { grade = value; }
        }
        public int Age
        {
            get { return age; }
            set { grade = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public Student()
        { 
        }
        public Student(string name, int age, double grade)
        {
            this.Name = name;// Student st = new Student();st.Name
            this.Age = age;// "this" doi tuong noi tai cua lop
            this.Grade = grade;
        }
        public Student(string name)
        {
            this.Name = name;
        }

        public Student(int age)
        {
            this.age = age;
        }
    }
}
