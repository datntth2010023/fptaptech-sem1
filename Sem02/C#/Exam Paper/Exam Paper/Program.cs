﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam_Paper
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu();
        }

        static void Menu() 
        {
            int choose;
            ProductManage productManage = new ProductManage();
            do
            {
                Console.OutputEncoding = Encoding.UTF8;
                Console.WriteLine("----- QUẢN LÝ SẢN PHẨM -----");
                Console.WriteLine("==========================");
                Console.WriteLine("1. Thêm mới hồ sơ sản phẩm.");
                Console.WriteLine("2. Xóa hồ sơ sản phẩm.");
                Console.WriteLine("3. Hiển thi hồ sơ sản phẩm.");
                Console.WriteLine("0. Thoát.");


                choose = int.Parse(Console.ReadLine());
                switch(choose)
                {
                    case 1:
                        Console.Clear();
                        productManage.AddProduct();
                        break;
                    case 2:
                        Console.Clear();
                        productManage.DeleteProduct();
                        break;
                    case 3:
                        Console.Clear();
                        productManage.ShowProduct();
                        break;
                    case 0:
                        Console.WriteLine("Tạm biệt ,hẹn gặp lại <3");
                        break;
                    default:
                        Console.WriteLine("Bạn chọn sai, vui lòng chọn lại!!!");
                        break;
                }    
            }
            while (choose != 0);
        }
    }
}
