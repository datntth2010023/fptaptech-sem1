﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam_Paper
{
    class ProductManage
    {

        List<Product> productList = new List<Product>();
        static int choose;

        public void AddProduct()
        {

            string choose;

            while (true)
            {
                Product product = new Product();
                product.Input();

                productList.Add(product);
                Console.OutputEncoding = Encoding.UTF8;
                Console.WriteLine("Bạn có muốn nhập tiếp không?(Y/N)");
                choose = Console.ReadLine();
                if (choose.Equals("N")) 
                    break;

            }

        }

        public void ShowProduct()
        {
            Console.WriteLine("Tổng số sản phẩm là: {0}", productList.Count);
            foreach (var item in productList)
            {
                item.Display();
            }
        }

        public void DeleteProduct()
        {
            Console.WriteLine("Nhập mã Id sản phẩm muốn xóa :");
            int index = int.Parse(Console.ReadLine());

            productList.RemoveAt(index);
            Console.WriteLine("Xóa thành công sản phẩm");
        }
    }
}