﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Exam_Paper
{
    public class Product
    {
        string productId;
        string name;
        float price;

        public string ProductId { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }

        public Product() { }
        
        public Product(string productId, string name, float price) 
        {
            this.ProductId = productId;
            this.Name = name;
            this.Price = price;
        }

        public void Input() 
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập vào Id sản phẩm : ");
            ProductId = Console.ReadLine();
            Console.WriteLine("Nhập vào tên sản phẩm : ");
            Name = Console.ReadLine();
            Console.WriteLine("Nhập vào giá của sản phẩm : ");
            Price = float.Parse(Console.ReadLine());
        }

        public void Display()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("---------------------------");
            Console.WriteLine("ProductId : {0}",ProductId);
            Console.WriteLine("Product Name : {0}", Name);
            Console.WriteLine("Product Price : {0}",Price);
        }
    }
}
