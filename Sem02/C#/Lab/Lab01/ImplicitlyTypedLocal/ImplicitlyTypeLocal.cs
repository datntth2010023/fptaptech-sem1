﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImplicitlyTypedLocal
{
    class ImplicitlyTypeLocal
    {
        static void Main(string[] args)
        {
            var i = 5;
            var s = "hello";
            var d = 1.0;
            // i is an integer
            Console.WriteLine("i*i: " + i * i);
            // s is an string 
            Console.WriteLine("s is upper case: " + s.ToUpper());
            // d is an a bouble
            Console.WriteLine("type of d: " + d.GetType());
            Console.ReadLine();
        }
    }
}
