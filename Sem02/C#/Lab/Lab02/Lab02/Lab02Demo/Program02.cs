﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab02Demo
{
    class Program02
    {
        static void Main(string[] args) 
        {
            Console.WriteLine("So phan tu ban muon nhap : ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            Console.Write("Nhap phan tu cua Array : ");
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("\nPhan tu {0} : ", i);
                array[i] = int.Parse(Console.ReadLine());
            }

            int max = array[0];
            int secmax = array[0];
            for (int i = 0; i < n; i++) 
            {
                if (array[i] > max)
                {
                    max = array[i];
                }
            }
            for (int i = 0; i < n; i++) 
            {
                if(array[i] > secmax && array[i] < max) 
                {
                    secmax = array[i];
                }
            }
            Console.WriteLine("Phan tu lon nhat la : " + max);
            Console.WriteLine("Phan tu lon thu 2 la : " + secmax);
        }
    }
}
