﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab02Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            Console.WriteLine("So phan tu ban muon nhap : ");
            int n = int.Parse(Console.ReadLine());
            int[] array1 = new int[n];
            int[] array2 = new int[n];
            int[] array3 = new int[n];

            Console.Write("Nhap phan tu cua Array1 : ");
            for (i = 0; i < n; i++)
            {
                Console.WriteLine("Phan tu {0} : " , i);
                array1[i] = int.Parse(Console.ReadLine());
            }

            Console.Write("Nhap phan tu cua Array2 : ");
            for (i = 0; i < n; i++)
            {
                Console.WriteLine("Phan tu {0} : ", i);
                array2[i] = int.Parse(Console.ReadLine());
            }


            // sao hai mang array1 va array2 cho array3
            /*for (i = 0; i < n; i++)
            {
                array3[i] = array1[i];
            }
            for (int  j = 0; j < m; j++)
            {
                array3[j] = array2[j];
                i++;
            }*/

            Console.WriteLine("Cac phan tu cua Array1 : ");
            for (i = 0; i < n; i++)
            {
                Console.Write("{0}" + "\t" , array1[i]);
            }
            Console.WriteLine("\nCac phan tu cua Array2 : ");
            for (i = 0; i < n; i++)
            {
                Console.Write("{0}" + "\t", array2[i]);
            }
            Console.WriteLine("\nCac phan tu cua Array3 : ");
            for (i = 0; i < n; i++)
            {
                Console.Write("{0}" + "\t", array3[i]);
            }
            Console.ReadLine();
        }
    }
}
