﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayClass
{
    class ArrayClass
    {
        static void Main(string[] args)
        {
            int[] arr = new int[12] { 29, 82,42,46,54,65,50, 42, 5, 94, 19, 34 };
            Console.WriteLine("This first occurrence of 42 is at index " + Array.IndexOf(arr, 42));
            Console.WriteLine("The last occurrence of 42 is at index " + Array.LastIndexOf(arr, 42));

            int x = 0;
            while ((x = Array.IndexOf(arr, 42, x)) >= 0) 
            {
                Console.WriteLine("42 found at index " + x);
                ++x;
            }
            x = arr.Length - 1;
            while ((x = Array.LastIndexOf(arr, 42, x)) >= 0)
            {
                Console.WriteLine("42 found at index " + x);
                --x;
            }

            Console.WriteLine("Array that before sorted");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("{0} :       {1}", i + 1, arr[i]);
            }
            Array.Sort(arr);
            Console.WriteLine("Array that after sorted ");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("{0} :       {1}", i + 1, arr[i]);
            }
            Array.Reverse(arr);
            Console.WriteLine("Array that after reverse");
            for (int i = 0; i < arr.Length; i++) 
            {
                Console.WriteLine("{0} :       {1}", i + 1, arr[i]);
            }
            Console.ReadLine();
        }
    }
}
