﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab06
{
    class Program
    {
        public void Increment(ref int X);
        public static void Add02(ref int x)
        {
            x += 2;
        }
        public static void Add03(ref int x)
        {
            x += 3;
        }
        static void Main(string[] args)
        {
            Increment functionDelegate = Add02;
            functionDelegate += Add03;
            functionDelegate += Add02;
            int x = 5;
            functionDelegate(ref x);
            Console.ReadLine();
            
        }
    }
}
