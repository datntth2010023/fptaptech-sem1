﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise05
{
    class Exercise05
    {
        static void Main(string[] args)
        {
            int num, factorial = 1;

            Console.Write("Enter the number : ");
            num = Convert.ToInt32(Console.ReadLine());

            for (int i =1; i <= num; i++) {
                factorial = factorial * i;
            }
            Console.WriteLine("the factorial is: " + factorial);
        }
    }
}
