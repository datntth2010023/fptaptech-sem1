﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise01
{
    class Exercise01
    {
        static void Main(string[] args)
        {
            string name;
            string address;
            string phone;

            Console.Write("Enter the name: ");
            name = Console.ReadLine();
            Console.Write("Enter the address: ");
            address = Console.ReadLine();
            Console.Write("Enter the phone: ");
            phone = Console.ReadLine();

            Console.WriteLine("--------------------------");
            Console.WriteLine("This information: ");
            Console.WriteLine("The name: " + name);
            Console.WriteLine("The address: " + address);
            Console.WriteLine("The phone: " + phone);
            Console.WriteLine("--------------------------");
        }
    }
}
