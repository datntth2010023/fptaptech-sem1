﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise02
{
    class Exercise02
    {
        static void Main(string[] args)
        {
            int a, b, c;

            Console.Write("Enter the number: ");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            c = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("The number: {0}, {1}, {2}", a, b, c);

            if (a > b){
                if (a > c){
                    Console.WriteLine(" {0} is max number", a);
                }
                else if (a < c){
                    Console.WriteLine(" {0} is max number", c);
                }
            }
            else if (a < b) {
                if (c < b) {
                    Console.WriteLine(" {0} is max number", b);
                } 
                else if (b < c) {
                    Console.WriteLine(" {0} is max number", c);
                }
            }
        }
    }
}
