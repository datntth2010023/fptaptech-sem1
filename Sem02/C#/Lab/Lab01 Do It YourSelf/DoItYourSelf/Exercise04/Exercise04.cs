﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise04
{
    class Exercise04
    {
        static void Main(string[] args)
        {
            int num;
            Console.Write("Enter the number: ");
            num = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("The number: " + num);

            Console.WriteLine("--------------");
            Console.WriteLine("9 multiples of {0} included : ",num);
            for (int i = 1; i <= 10; ++i) { 
                Console.WriteLine(i * num);
            }
            Console.WriteLine("--------------");
        }
    }
}
