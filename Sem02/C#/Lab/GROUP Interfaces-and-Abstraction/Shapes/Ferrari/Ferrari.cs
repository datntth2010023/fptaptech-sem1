﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ferrari
{
    class Ferrari : IModel, IBrakes, IGasPedal
    {
        private string DriverName;
        public string Model { get; set; }
        public Ferrari(string driverName, string model)
        {
            DriverName = driverName;
            Model = model;
        }

        public Ferrari()
        {
        }

        public void Brakes()
        {
            Console.WriteLine("Brakes!");
        }
        public void GasPedal()
        {
            Console.WriteLine("Zadu6avam sA!");
        }
    }
}
