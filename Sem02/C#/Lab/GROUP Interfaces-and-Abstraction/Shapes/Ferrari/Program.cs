﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ferrari
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the Name Driver : ");
            string driverName = Console.ReadLine();
            Console.WriteLine("Enter the Model : ");
            string model = Console.ReadLine();
            Ferrari ferrari = new Ferrari(driverName,model);

            ferrari.Brakes();
            ferrari.GasPedal();

            /*int choice;
            do 
            {
                Console.WriteLine("Enter status car : ");
                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        ferrari.Brakes();
                        break;
                    case 2:
                        ferrari.GasPedal();
                        break;
                    case 0:
                        break;
                }
            }
            while (choice != 0);*/
        }
    }
}
