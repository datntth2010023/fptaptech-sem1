﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cars
{
    class Seat : ICar 
    {
        public string Model { get; set; }
        public string Color { get; set; }

        public Seat() { }
        public Seat(string model, string color) 
        {
            Model = model;
            Color = color;
        }

        public string Start()
        {
            return "Engine start";
        }
        public string Stop()
        {
            return "Breaaak!";
        }

        public string ToString() 
        {
            return Color + " Seat " + Model;
        }
    }
}
