﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles
{
    class Program
    {
        static void Main(string[] args)
        {
            IVehicles Car = new Car();
            IVehicles Truck = new Truck();

            Car.Input();
            Car.Display();

            Truck.Input();
            Truck.Display();
        }
    }
}
