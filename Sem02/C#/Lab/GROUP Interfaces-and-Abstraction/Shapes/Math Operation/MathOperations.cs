﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Math_Operation
{
    class MathOperations
    {
        int a, b, c;
        public MathOperations() { }
        public MathOperations(int a, int b, int c) 
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public int Add(int a, int b)
        {
            return a + b;
        }

        public double Add(double a, double b, double c)
        {
            return a + b + c;
        }

        public decimal Add(decimal a, decimal b, decimal c)
        {
            return a + b + c;
        }
    }
}
