﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees
{
    class Program
    {
        static void Main(string[] args)
        {
            EmployeeManagnement();
        }

        public static void EmployeeManagnement() 
        {
            EmployeeManager employeeManager = new EmployeeManager();
            int choice;
            do
            {
                Console.WriteLine("+=+=+  Quan ly nhan vien  +=+=+");
                Console.WriteLine("===============================");
                Console.WriteLine("1. Them moi nhan vien.");
                Console.WriteLine("2. Sua thong tin nhan vien.");
                Console.WriteLine("3. Xoa nhan vien.");
                Console.WriteLine("4. Hien thi danh sach nhan vien");

                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.Clear();
                        employeeManager.Add();
                        break;
                    case 2:
                        Console.Clear();
                        employeeManager.Update();
                        break;
                    case 3:
                        Console.Clear();
                        employeeManager.Delete();
                        break;
                    case 4:
                        Console.Clear();
                        employeeManager.Display();
                        break;
                    case 0:
                        break;
                    default:
                        break;
                }
            }
            while (choice != 0);
            
        }
    }
}
