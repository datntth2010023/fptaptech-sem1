﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Employees
{
    public  class EmployeeManager
    {
        //ArrayList empList = new ArrayList();
        //SortedList empList = new SortedList();
        Hashtable empList = new Hashtable();

        public void Add() 
        {
            string choice;
            while (true) 
            {
                Employees emp = new Employees();
                emp.Input();
                int id = int.Parse(Console.ReadLine());

                Console.WriteLine("Them moi nhan vien : ");
                empList.Add(id, emp);

                Console.WriteLine("Ban co muon nhap tiep khong?(Y/N)");
                choice = Console.ReadLine();
                if (choice == "N") break;
            }
        }
        public void Update() 
        {
            string choice;
            while (true)
            {


                Console.WriteLine("Ban co muon sua tiep khong?(Y/N)");
                choice = Console.ReadLine();
                if (choice == "N") break;
            }
        }
        public void Delete() 
        {
            string choice;
            int count;
            while (true)
            {
                Console.WriteLine("Phan tu ban muon xoa : ");
                count = int.Parse(Console.ReadLine());
                empList.Remove(count);

                Console.WriteLine("Ban co muon xoa tiep khong?(Y/N)");
                choice = Console.ReadLine();
                if (choice == "N") break;
            }

        }
        public void Display()
        {
            Console.WriteLine("Employees List : ");
            /* //ArrayList
             * foreach (Employees i in empList) 
            {
                Console.WriteLine(" " + i);
            }*/



            /*// Sort List
             * Console.WriteLine("Number of Elements in Employees List: {0}",
                empList.Count);
            for (int i = 0; i < empList.Count; i++)
            {
                Console.WriteLine("\t{0}: \t{1}", empList.GetKey(i),
                    empList.GetByIndex(i));
            }
            Console.ReadLine();*/


            // HashTable
            ICollection c = empList.Keys;
            foreach (string str in c)
            {
                Console.WriteLine(str + ": " + empList[str]);
            }
        }
    }
}
