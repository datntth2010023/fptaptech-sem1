﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees
{
    public class Employees
    {
        string name;
        int age;
        double salry;

        public string Name { get; set; }
        public int Age { get; set; }
        public double Salary { get; set; }

        public void Employee() { }
        public void Employee(string name, int age, double salary) 
        {
            this.Name = name;
            this.Age = age;
            this.Salary = salary;
        }

        public void Input() 
        {
            Console.WriteLine("Nhap ten nhan vien : ");
            Name = Console.ReadLine();
            Console.WriteLine("Nhap tuoi cua nhan vien : ");
            Age = int.Parse(Console.ReadLine());
            Console.WriteLine("Nhap so luong cua nhan vien : ");
            Salary = double.Parse(Console.ReadLine());
        }

        public override string ToString()
        {
            return String.Format("Name:{0}\t Age: {1}\t Salary: {2}$", Name, Age, Salary);
        }

    }
}
