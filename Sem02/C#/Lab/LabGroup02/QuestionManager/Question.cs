﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionManage
{
    class Question
    {
        string[] dapAn = new string[4];
        char[] dapAnDung = new char[4];
        string cauHoi;
        float diem;
        string maCauHoi;
        string maDanhMuc;

        public string MaDanhMuc { get; set; }
        public string MaCauHoi { get; set; }
        public float Diem { get; set; }
        public string CauHoi { get; set; }

        public Question() { }
        public Question(string maDanhMuc, string maCauHoi, float diem,
            string cauHoi, char[] dapAnDung, string[] dapAn)
        {
            this.MaDanhMuc = maDanhMuc;
            this.MaCauHoi = maCauHoi;
            this.CauHoi = cauHoi;
            this.Diem = diem;
            this.dapAn = dapAn;
            this.dapAnDung = dapAnDung;
        }

        public void Input()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập mã danh mục : ");
            MaDanhMuc = Console.ReadLine();
            Console.WriteLine("Nhập mã câu hỏi : ");
            MaCauHoi = Console.ReadLine();
            Console.WriteLine("Nhập câu hỏi : ");
            CauHoi = Console.ReadLine();
            Console.WriteLine("Nhập điểm : ");
            Diem = float.Parse(Console.ReadLine());

            for (int i = 0; i < dapAnDung.Length; i++)
            {
                Console.WriteLine("Nhập đáp án : ");
                dapAn[i] = Console.ReadLine();

                Console.WriteLine("Đáp án Đúng hay Sai (D/S): ");
                dapAnDung[i] = Char.Parse(Console.ReadLine());
            }
        }


        public void Display()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Mã danh mục : " + MaDanhMuc);
            Console.WriteLine("Mã câu hỏi : " + MaCauHoi);
            Console.WriteLine("Câu hỏi : " + CauHoi);
            Console.WriteLine("Điểm của câu hỏi : " + Diem);
            for (int i = 0; i < dapAnDung.Length; i++)
            {
                Console.WriteLine("Đáp án : {0}", dapAn[i]);
                Console.WriteLine("Đáp án Đúng hay Sai (D/S): {0}",
                    dapAnDung[i]);
            }
            Console.WriteLine("");
        }


        public void Edit()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập câu hỏi : ");
            cauHoi = Console.ReadLine();
            Console.WriteLine("Nhập điểm : ");
            diem = float.Parse(Console.ReadLine());
            for (int i = 0; i < dapAnDung.Length; i++)
            {
                Console.WriteLine("Nhập đáp án : ");
                dapAn[i] = Console.ReadLine();

                Console.WriteLine("Đáp án Đúng hay Sai (D/S): ");
                dapAnDung[i] = Char.Parse(Console.ReadLine());
            }    
        }
    }
}
