﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionManage
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.ViewMenu();
        }

        public static void Menu()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("-----  CHƯƠNG TRÌNH QUẢN LÝ ĐỀ THI  -----");
            Console.WriteLine("=========================================");
            Console.WriteLine("1. Quản lý câu hỏi/trả lời.");
            Console.WriteLine("2. Quản lý đề thi.");
            Console.WriteLine("3. Thoát");
            Console.WriteLine("=========================================");
        }
        public void ViewMenu()
        {
            Console.OutputEncoding = Encoding.UTF8;
            QuestionMenu questionMenu = new QuestionMenu();
            int choose;
            do
            {
                Menu();
                choose = int.Parse(Console.ReadLine());
                switch (choose) 
                {
                    case 1:
                        Console.Clear();
                        questionMenu.MenuQuestion();
                        break;
                    case 2:
                        Console.Clear();
                        questionMenu.MenuTest();
                        break;
                    case 0:
                        Console.WriteLine("Tạm biệt, hẹn gặp lại!!!");
                        break;
                    default:
                        Console.WriteLine("Lựa chọn của bạn không đúng!!!");
                        Console.WriteLine("Vui lòng chọn lại!!!");
                        break;
                }
            }
            while (choose != 0);
        }
    }
}
