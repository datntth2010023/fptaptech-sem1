﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionManage
{ 
    class QuestionMenu
    {
        List<Question> questionsList = new List<Question>();
        static int choose;


        // Quản lý câu hỏi / câu trả lời
        public void MenuQuestion()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Program program = new Program();
            do
            {
                Menu();
                Console.WriteLine("");
                choose = int.Parse(Console.ReadLine());
                switch (choose)
                {
                    case 1:
                        Console.Clear();
                        DisplayQuestion();
                        break;
                    case 2:
                        Console.Clear();
                        Edit();
                        break;
                    case 3:
                        Console.Clear();
                        Create();
                        break;
                    case 0:
                        Console.Clear();
                        program.ViewMenu();
                        break;
                    default:
                        Console.WriteLine("Lựa chọn của bạn không đúng!!!");
                        Console.WriteLine("Vui lòng chọn lại!!!");
                        break;
                }
            }
            while (choose != 0);
        }

        public static void Menu()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("-----   QUẢN LÝ CÂU HỎI / TRẢ LỜI   -----");
            Console.WriteLine("=========================================");
            Console.WriteLine("1. Xem danh sách Q/A.");
            Console.WriteLine("2. Cập nhật Q/A.");
            Console.WriteLine("3. Tạo mới một Q/A.");
            Console.WriteLine("0. Trở về menu chính");
            Console.WriteLine("=========================================");
        }


        public void Create()
        {
            Console.OutputEncoding = Encoding.UTF8;
            String choose;
            while (true)
            {
                Question question = new Question();
                question.Input();

                questionsList.Add(question);

                Console.WriteLine("Bạn Có Muốn Nhập Tiếp Không? Y/N");
                choose = Console.ReadLine();
                if (choose.Equals("N")) break;
            }
        }


        public void Edit()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập Mã Câu Hỏi Cần Sửa: ");
            string Id = Console.ReadLine();
            foreach (var item in questionsList)
            {
                if (item.MaCauHoi.Equals(Id))
                {
                    item.Edit();
                    break;
                }
                else
                {
                    Console.WriteLine("Ko TÌm Thấy Câu Hỏi!!!");
                }
            }
        }



        public void DisplayQuestion()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Tổng Số Các Câu Hỏi Là: {0}", questionsList.Count);
            foreach (var item in questionsList)
            {

                item.Display();
            }
        }


        //Quản lý đề thi
        public void MenuTest()
        {
            Program program = new Program();
            do
            {
                Console.OutputEncoding = Encoding.UTF8;
                Console.WriteLine("-----          QUẢN LÝ ĐỀ THI       -----");
                Console.WriteLine("=========================================");
                Console.WriteLine("1. Xem đề thi.");
                Console.WriteLine("2. Tạo đề thi.");
                Console.WriteLine("0. Trở về menu chính");
                Console.WriteLine("=========================================");
                choose = int.Parse(Console.ReadLine());
                switch (choose)
                {
                    case 1:
                        Console.Clear();
                        break;
                    case 2:
                        Console.Clear();
                        break;
                    case 0:
                        Console.Clear();
                        program.ViewMenu();
                        break;
                    default:
                        Console.WriteLine("Lựa chọn của bạn không đúng!!!");
                        Console.WriteLine("Vui lòng chọn lại!!!");
                        break;
                }
            }
            while (choose != 0);
        }
    }
}
