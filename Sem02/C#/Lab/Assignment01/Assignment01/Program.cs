﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment01
{
    class Program
    {
        static void Main(string[] args)
        {
            int option;
            do
            {
                Menu();
                Console.Write("Lua chon cua ban : ");
                option = Convert.ToInt32(Console.ReadLine());
                switch (option)
                {
                    case 1:
                        QlDsDoiBong();
                        break;
                    case 2:
                        QlLichThiDau();
                        break;
                    case 3:
                        QlKqThiDau();
                        break;
                    case 4:
                        BangXepHang();
                        Console.WriteLine("Bang xep hang.");
                        break;
                    case 0:
                        break;
                    default:
                        Console.WriteLine("Vui long nhap lai lua chon cua ban!!!");
                        break;
                }
                Console.Clear();
            } while (option != 0);
            Console.ReadLine();

        }
        public static void Menu()
        {
            Console.WriteLine("===================================");
            Console.WriteLine("--Chao mung đen voi V-League 2021--");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("1. Quan ly danh sach đoi bong.");
            Console.WriteLine("2. Quan ly lich thi đau.");
            Console.WriteLine("3. Quan ly ket qua thi đau.");
            Console.WriteLine("4. Bang xep hang.");
            Console.WriteLine("0. Thoat.");
            Console.WriteLine("===================================");
        }


        //Quan ly Danh sach Doi bong
        public static void QlDsDoiBong() 
        {
            int choice;
            do {
                
                Console.WriteLine("===================================");
                Console.WriteLine("1. Xem danh sach doi bong.");
                Console.WriteLine("2. Cap nhat danh sach doi bong.");
                Console.WriteLine("3. Them moi mot doi bong.");
                Console.WriteLine("4. Xem danh sach theo thu tu ma doi.");
                Console.WriteLine("5. Xem danh sach theo thu tu ten doi.");
                Console.WriteLine("0. Tro lai menu chinh.");
                Console.WriteLine("===================================");

                Console.Write("Lua chon cua ban : ");
                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("1. Xem danh sach doi bong.");
                        break;
                    case 2:
                        Console.WriteLine("2. Cap nhat danh sach doi bong.");
                        break;
                    case 3:
                        Console.WriteLine("3. Them moi mot doi bong.");
                        break;
                    case 4:
                        Console.WriteLine("4. Xem danh sach theo thu tu ma doi.");
                        break;
                    case 5:
                        Console.WriteLine("5. Xem danh sach theo thu tu ten doi.");
                        break;
                    case 0:
                        break;
                }
            } while (choice!=0);
        }


        //Quan ly lich thi dau
        public static void QlLichThiDau()
        {
            int choice;
            do
            {
                Console.WriteLine("===================================");
                Console.WriteLine("1. Xem lich thi dau.");
                Console.WriteLine("2. Cap nhat lich thi dau.");
                Console.WriteLine("3. Tao lich thi dau.");
                Console.WriteLine("0. Tro lai menu chinh.");
                Console.WriteLine("===================================");

                Console.Write("Lua chon cua ban : ");
                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("1. Xem lich thi dau.");
                        break;
                    case 2:
                        Console.WriteLine("2. Cap nhat lich thi dau.");
                        break;
                    case 3:
                        Console.WriteLine("3. Tao lich thi dau.");
                        break;
                    case 0:
                        break;
                }
            } while (choice != 0);

        }


        //Quan ly ket qua thi dau
        public static void QlKqThiDau()
        {
            int choice;
            do
            {
                Console.WriteLine("===================================");
                Console.WriteLine("1. The Cong - Viettel 3-0 HAGL.");
                Console.WriteLine("2. SLNA ?-? Hai Phong .");
                Console.WriteLine(" ...");
                Console.WriteLine("0. Tro lai menu chinh.");
                Console.WriteLine("===================================");

                Console.Write("Lua chon cua ban : ");
                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("1. The Cong Viettel 3-0 HAGL.");
                        break;
                    case 2:
                        Console.WriteLine("2. SLNA ?-? Hai Phong");
                        break;
                    case 0:
                        break;
                }
            } while (choice != 0);
        }


        public static void BangXepHang()
        {
        }
    }
}
