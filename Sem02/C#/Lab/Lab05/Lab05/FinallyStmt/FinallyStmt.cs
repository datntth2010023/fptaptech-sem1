﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace FinallyStmt
{
    class FinallyStmt
    {
        static void Main(string[] args)
        {
            FileStream outStream = null;
            FileStream inStream = null;
            try
            {
                // Mo file de ghi de du lieu
                outStream = File.OpenWrite("DestinationFile.txt");
                // Mo file de doc du lieu
                inStream = File.OpenRead("BogusInputFile.txt");
                //Cac cau lenh doc du lieu tu file
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (outStream != null)
                {
                    outStream.Close();
                    Console.WriteLine("outStream closed.");
                }
                if (inStream != null)
                {
                    inStream.Close();
                    Console.WriteLine("inStream closed.");
                }
            }
        }
    }
}
