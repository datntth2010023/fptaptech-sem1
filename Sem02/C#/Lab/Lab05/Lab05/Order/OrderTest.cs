﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customer;
using Order;

namespace Customer
{
    class Cust_details
    {
        public string strName;
        public void getName()
        {
            Console.WriteLine("Enter your name : ");
            strName = Console.ReadLine();
        }
    }
}
namespace Order
{
    class Grocer_items
    {
        public void Ord_grocery()
        {
            Cust_details objCust01 = new Cust_details();
            objCust01.getName();
            Console.WriteLine("Hello {0}", objCust01.strName);
            Console.WriteLine("You have ordered grocery");
        }
    }
    class Bakery_items
    { 
        public void Ord_Bakery()
        {
            Cust_details objCust02 = new Cust_details();
            objCust02.getName();
            Console.WriteLine("Hello {0}", objCust02.strName);
            Console.WriteLine("You have ordered bakery items");
        }
    }
    class OrderTest
    {
        public static void Main()
        {
            string choice;
            Console.WriteLine("What would you like to order? 1-Grocery 2-Bakery Items");
            choice = Console.ReadLine();
            if (choice == "1")
            {
                Grocer_items objGrocery = new Grocer_items();
                objGrocery.Ord_grocery();
            }
            else
            {
                if (choice == "2")
                {
                    Bakery_items objBakery = new Bakery_items();
                    objBakery.Ord_Bakery();
                }
                else
                {
                    Console.WriteLine("Enter either 1 or 2");
                }
            }
        }
    }
}
