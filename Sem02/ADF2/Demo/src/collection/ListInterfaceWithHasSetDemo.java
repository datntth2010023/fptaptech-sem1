package collection;
import java.util.HashSet;
import java.util.Iterator;// dung de lap
import java.util.Set;// dung de gan gia tri

public class ListInterfaceWithHasSetDemo {
    public static void main(String[] args) {
        // khoi tao ban dau 100 phan tu  se chua va tang lan chua toi da 150 phan tu (50%)
        Set<String> mySet = new HashSet<String>(100,(float) 0.5);
        mySet.add("SONY");
        mySet.add("APPLE");
        mySet.add("LG");
        mySet.add("SONY");// gia tri trung lap se bi ghi de gia tri sau se ghi de len gia tri truoc
        mySet.add("ONE PLUS");

        Iterator<String> iterator = mySet.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
