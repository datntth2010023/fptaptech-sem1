package collection;
import java.util.HashMap;

public class ListInterfaceHashMapDemo {
    public static void main(String[] args) {
        // Key and Value pairs
        HashMap<String,Float> studentMap = new HashMap<>();
        studentMap.put("QWE",15.5f);
        studentMap.put("ABC",12.3f);
        studentMap.put("ASD",10.5f);

        Float mark = studentMap.get("ASD");
        System.out.println(mark);
        studentMap.put("QWE",20.0f);

        Float mark2 = studentMap.get("QWE");
        System.out.println(mark2);
    }
}
