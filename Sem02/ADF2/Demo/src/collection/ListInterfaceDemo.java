package collection;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ListInterfaceDemo {
    public static void main(String[] args) {
        // Declare ArrayList
        /*
         * ArrayList list = new ArrayList();
         * List list = new ArrayList();
         * List<String> list = new ArrayList<String>();
         */
        List<String> myList = new ArrayList<>();
        myList.add("SONY");
        myList.add("APPLE");
        myList.add("LG");
        myList.add("ONE PLUS");

        ListIterator<String> listIterator = myList.listIterator();
        String string1 = listIterator.next();// bat dau item1 co index =0
        System.out.println(string1);
        String string2 = listIterator.next();
        System.out.println(string2);
        /*String string3 = listIterator.next();
        *System.out.println(string3);
        String string4 = listIterator.next();
        System.out.println(string4);*/

        if(listIterator.hasPrevious()) {
            String string = listIterator.previous();
            //System.out.println(listIterator.next());
            System.out.println(string);
        }
        while (listIterator.hasNext()) { // in het cac phan tu con lai trong danh sach
            String stringValue = listIterator.next();
            System.out.println(stringValue);
        }
    }
}
