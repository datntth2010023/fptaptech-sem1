package collection;
import java.util.LinkedList;


public class ListInterfaceWithLinkedList {
    public static void main(String[] args) {
        // linh hoat hon array, doi cho danh sach cac phan tu,khong bi gioi han phan tu.
        LinkedList<String> myLinkedList = new LinkedList<>();
        myLinkedList.add("SONY");
        myLinkedList.add("APPLE");
        myLinkedList.add("LG");
        System.out.println("List1: " +myLinkedList);

        myLinkedList.addFirst("ONE PLUS");
        myLinkedList.addLast("ROG PHONE 6");
        myLinkedList.add(3,"XIAOMI");
        System.out.println("List2: " +myLinkedList);
        myLinkedList.remove(4);
        Object listObject = myLinkedList.get(0);// tro vao vung o nho co index = 0
        // myLinkedList.set(2,(String)listObject + "PHONE");// chen du lieu
        System.out.println(listObject);
        System.out.println("List3: " +myLinkedList);
    }
}
