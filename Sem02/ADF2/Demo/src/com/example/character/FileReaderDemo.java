package com.example.character;

import java.io.FileReader;
import java.io.Reader;
import java.io.IOException;

public class FileReaderDemo {
    public static void main(String[] args) throws IOException{
        Reader reader = new FileReader("C:\\Users\\thdat\\OneDrive\\DocumentsJava.txt");

        try {
            char ch = (char)reader.read();
            System.out.println(ch);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            reader.close();
        }
    }
}
