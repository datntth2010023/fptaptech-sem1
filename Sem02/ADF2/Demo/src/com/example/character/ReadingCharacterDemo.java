package com.example.character;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadingCharacterDemo {
    public static void main(String[] args) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        try {
            String str= "adhjfkg";
            str = bufferedReader.readLine();
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            bufferedReader.close();
        }
    }
}
