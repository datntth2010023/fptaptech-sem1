package com.example.jdbcadv2;

import java.sql.*;

public class Connector {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();

        String url = "jdbc:sqlserver:/localhost:1433";
        String username = "sa";
        String password = "123";
        try{
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
            statement.execute("create database if not exists TestDB");
            statement.execute("use TestDB");
            statement.execute("drop table if exists users");
            statement.execute("create table users(id int not null identity primary key," +
                    " username varchar(50)," +
                    "password varchar(50))");
            statement.executeUpdate("insert into users");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void close(ResultSet resultSet){
        if (resultSet != null){
            try{
                resultSet.close();
            }catch (Exception e){}
        }
    }
    public static void close(PreparedStatement preparedStatement){
        if (preparedStatement != null){
            try{
                preparedStatement.close();
            }catch (Exception e){}
        }
    }
    public static void close(Connection connection){
        if (connection != null){
            try{
                connection.close();
            }catch (Exception e){}
        }
    }
}
