package com.example.jdbcadv.db;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Connector {
    public Connection getConnection() throws Exception{
        // 1. Register driver
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();

        // 2. Establish connection : tao ket noi
        String url = "jdbc:sqlserver:/localhost:1433;databaseName=test";
        String username = "sa";
        String password = "123";
        return  DriverManager.getConnection(url,username,password);

        // 3. Statement:Statement/preparedStatement/CallAbleStatement
        // 4.ResultSet : ket qua tra ve cua Db(1 hoac nhieu bang)
        // 5. Close: ResultSet -> Statement -> Connection
    }
    public static void close(ResultSet resultSet){
        if (resultSet != null){
            try{
                resultSet.close();
            }catch (Exception e){}
        }
    }
    public static void close(PreparedStatement preparedStatement){
        if (preparedStatement != null){
            try{
                preparedStatement.close();
            }catch (Exception e){}
        }
    }
    public static void close(Connection connection){
        if (connection != null){
            try{
                connection.close();
            }catch (Exception e){}
        }
    }
}
