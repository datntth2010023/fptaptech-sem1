package com.example.bytestream.Object;

import java.io.*;

public class ObjectSerializationDemo {
    public static void main(String[] args) throws IOException {
        ObjectInputStream ois = null;
        ObjectOutputStream oos = null;
        try{
            oos = new ObjectOutputStream(
                    new BufferedOutputStream(
                            new FileOutputStream("C:\\Users\\thdat\\OneDrive\\Documents\\ADF2\\object.dat")));

            // Create an array of 10 SerializedObjects with asc numbers
            SerializedObject[] objs = new SerializedObject[10];
            for(int i = 0; i < objs.length;i++){
                objs[i] = new SerializedObject(i);
            }
            // Write the 10 objects to file , one by one
            for (int i = 0; i < objs.length;i++){
                oos.writeObject(objs[i]);
            }
            oos.writeObject(objs);
            oos.close();

            // Read back the objects
            ois = new ObjectInputStream(
                    new BufferedInputStream(
                            new FileInputStream(
                                    "C:\\Users\\thdat\\OneDrive\\Documents\\ADF2\\object.dat")));

            SerializedObject objIn;
            for (int i = 0; i < objs.length; i++){
                objIn = (SerializedObject) ois.readObject();
                System.out.println(objIn.getNumber());
            }
            SerializedObject[] objArrayIn;
            objArrayIn = (SerializedObject[])ois.readObject();
            for (SerializedObject o : objArrayIn){
                System.out.println(o.getNumber());
            }
            ois.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException ex){
            ex.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
