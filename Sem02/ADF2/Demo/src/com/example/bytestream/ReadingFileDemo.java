package com.example.bytestream;

import java.io.*;
import java.io.IOException;

public class ReadingFileDemo {
    public static void main(String[] args) throws IOException {

        FileInputStream fileInputStream = new FileInputStream(new File("C:\\Users\\thdat\\OneDrive\\Documents\\Workspace\\F8-Shop\\The_HTML_thong_dung.txt"));
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        try {
            char ch = (char)bufferedInputStream.read();
            System.out.println(ch);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            bufferedInputStream.close();
        }
    }
}
