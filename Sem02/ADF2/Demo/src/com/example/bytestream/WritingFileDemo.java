package com.example.bytestream;

import java.io.*;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;

public class WritingFileDemo {
    public static void main(String[] args) throws IOException {
        String message = "Java change your life";
        BufferedOutputStream bufferedOutputStream = null; // clear dong ghi
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(new File("C:\\Users\\thdat\\OneDrive\\Documents\\Java.txt"));
            bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            bufferedOutputStream.write(message.getBytes());
            System.out.println(("Writing!!!"));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            bufferedOutputStream.close();
        }
    }
}
