package com.example.security;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class StoreProcedureDemo {
    public static void main(String[] args) throws Exception {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
        String url = "jdbc:sqlserver://127.0.0.1:1433;databaseName=test";
        String username = "sa";
        String password = "123";
        Connection conn = DriverManager.getConnection(url,username,password);
        System.out.println(conn);
        //goi StoreProcedure
        //String dbQuery = "select * from Users";with CallableStatement no need sql engine, use name of stored procedure only
        //1. Statement(co ban) 2.PreparedStatement(???) 3.CallableStatement(call store procedure)
        CallableStatement cs =  conn.prepareCall("{call proc_ViewUsers1}");
        ResultSet resultSet = cs.executeQuery();
        while (resultSet.next()){
            System.out.println("Name :" + resultSet.getString(2));
        }
        resultSet.close();
        cs.close();
        conn.close();
    }
}
