package com.example.security.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Connector {
    public static Connection getConnection() throws Exception{
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
        String url = "jdbc:sqlserver://127.0.0.1:1433;databaseName=test";
        String username = "sa";
        String password = "123";
        return DriverManager.getConnection(url, username, password);
    }
    public static void close(ResultSet resultSet){
        if (resultSet  != null){
            try{
                resultSet.close();
            }catch (Exception e){}
        }
    }
    public static void close(PreparedStatement preparedStatement){
        if (preparedStatement != null){
            try{
                preparedStatement.close();
            }catch (Exception e){}
        }
    }
    public static void close(Connection connection){
        if (connection != null){
            try{
                connection.close();
            }catch (Exception e){}
        }
    }
}
