package com.example.security.ui;

import com.example.security.db.PersonAccessor;
import com.example.security.entity.Person;

import java.util.*;

public class PersonManagement {
    static Scanner scanner = new Scanner(System.in);
    static PersonAccessor personAccessor = new PersonAccessor();
    static Person person = new Person();
    public PersonManagement(){}

    public static void menu(){
        System.out.println("___________________OPTIONS________________");
        System.out.println("1. Add students");
        System.out.println("2. update a students");
        System.out.println("3. Delete a students");
        System.out.println("4. Search students");
        System.out.println("5. Display add students");
        System.out.println("6. Exit");
        System.out.println("________________________________________");
    }

    public static void main(String[] args){
        int choose;
        PersonManagement personManagement = new PersonManagement();
        PersonAccessor personAccessor = new PersonAccessor();
        Person person = new Person();
        do {
            menu();
            System.out.println("Choose: ");
            choose = Integer.parseInt(scanner.nextLine());
            switch (choose){
                case 1:
                    personManagement.addStudent();
                    break;
                case 2:
                    personManagement.update();
                    break;
                case 3:
                    personManagement.delete();
                    break;
                case 4:
                    personManagement.searchById();
                    break;
                case 5:
                    personManagement.dissplayAll();
                    break;
                case 6:
                    System.out.println("\n Thank you! See you again!");
                    continue;
            }
        }while (choose != 6);
    }

    public void addStudent(){
        System.out.println("Enter the id: ");
        int id = Integer.parseInt(scanner.next());
        System.out.println("Enter the name: ");
        String name = scanner.nextLine();
        person.setId(id);
        person.setName(name);
        try{
            personAccessor.insert(person);
            System.out.println("\nInsert success!");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void update(){
        System.out.println("Enter the id person: ");
        int id = Integer.parseInt(scanner.next());
        System.out.println("Enter the name: " + id + ": ");
        String name = scanner.nextLine();
        person.setId(id);
        person.setName(name);
        try{
            personAccessor.update(person);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void delete(){
        System.out.println("Enter the Person id to delete: ");
        int id = Integer.parseInt(scanner.next());
        try{
            personAccessor.delete(id);
            System.out.println("\nDelete success!");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void searchById(){
        System.out.println("Enter the personId: ");
        int id = Integer.parseInt(scanner.next());
        try{
            person = personAccessor.findByPrimaryKey(id);
            System.out.println("PersonId: " + id + "| Name: " + person.getName());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void dissplayAll(){
        try{
            System.out.println("Show all person: ");
            String name = "";
            List<Person> personList = personAccessor.getPersons(name);
            personList.forEach(person1 -> {
                System.out.println(person.getId() + "\t\t" + person.getName());
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
