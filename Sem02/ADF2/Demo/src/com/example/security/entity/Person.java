package com.example.security.entity;

import java.util.Scanner;

public class Person {
    private int id;
    private String name;
    Scanner scanner = new Scanner(System.in);

    public Person(){}
    public Person(int id, String name){
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
