CREATE TABLE Products
(
   ID int not null generated always as identity(start WITH 1, increment by 1) primary key,
   ProName varchar(50),
   ProDesc varchar(50),
   Price double
)