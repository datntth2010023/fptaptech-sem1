package com.example.model;

import com.example.entity.Product;
import java.util.ArrayList;

public abstract class ProductDao {

    public void createProduct(Product product);

    public Product getProductById(int productId);

    public ArrayList<Product> getAllProduct();

    public void updateProduct(Product product);

    public abstract ArrayList<Product>getAllProducts();

    public boolean deleteProduct(int productId);
}
