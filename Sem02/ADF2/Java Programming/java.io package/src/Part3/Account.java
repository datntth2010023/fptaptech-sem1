package Part3;

import java.io.*;
import java.util.Scanner;
import java.io.IOException;

public  class Account {
    private String firstName;
    private String lastName;
    private int age;
    private float accountBalance;

    public Account() {}

    public Account(String firstName, String lastName, int age, float accountBalance) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.accountBalance = accountBalance;
    }

    public void addAccount() {
        // Scanner : dua du lieu tu ngoai vao
        Scanner scanner = new Scanner(System.in);
        System.out.println("First Name: ");
        this.firstName = scanner.nextLine();
        System.out.println("Last Name: ");
        this.lastName = scanner.nextLine();
        System.out.println("Age: ");
        this.age = scanner.nextInt();
        System.out.println("Account Balance: ");
        this.accountBalance = scanner.nextFloat();
        this.saveState();
        System.out.println("Add new Account DONE!");
    }

    public void saveState() {
        //BufferedWriter: ghi du lieu
        try {
            FileWriter  fileWriter = new FileWriter("C:\\Users\\thdat\\OneDrive\\Documents\\Account.txt");
            BufferedWriter bufferedWriter = null;
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("First Name \t Last Name \t Age \t Balance\n");
            bufferedWriter.write(this.firstName);
            bufferedWriter.write("\t");
            bufferedWriter.write(this.lastName);
            bufferedWriter.write("\t");
            bufferedWriter.write(Integer.toString(this.age));
            bufferedWriter.write("\t");
            bufferedWriter.write(Float.toString(this.accountBalance));
            bufferedWriter.write("\t");
            bufferedWriter.newLine();
            bufferedWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void display(){
        //BufferedReader : doc du lieu
        System.out.println("The Details Account: ");
        try{
            FileReader fileReader = new FileReader("C:\\Users\\thdat\\OneDrive\\Documents\\Account.txt");
            BufferedReader bufferedReader =new BufferedReader(fileReader);
            /*
            * this.firstName = bufferedReader.readLine();
            this.lastName = bufferedReader.readLine();
            this.age = bufferedReader.read();
            this.accountBalance = bufferedReader.read();*/
            System.out.println("First Name: " + this.firstName);
            System.out.println("Last Name: " + this.lastName);
            System.out.println("Age: " + this.age);
            System.out.println("Account Balance: " + this.accountBalance);
            fileReader.close();
            bufferedReader.close();

        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println("Your Account information!!!");
    }
}