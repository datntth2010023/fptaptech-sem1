import java.util.*;

public class LyricWord {
    // The final keyword is a non-access modifier used for classes, attributes and methods,
    // which makes them non-changeable (impossible to inherit or override).("gia tri trong chuoi khong the ghi de
    // va khong the ke thua")
    public static final String[] lyric = {"you", "say", "it", "best", "when", "you", "say", "nothing", "at", "all"};

    public static void main(String[] args) {
        // Create a list that's implemented by ArrayList
        List words = new ArrayList();
        // Add all String in string array to list
        for (String w : lyric) {
            words.add(w);
        }

        // Traverse the list
        for (Object o : words) {
            System.out.println(o + " ");
        }
        System.out.println("\n--------------");

        System.out.println("Contains [you]?: " + words.contains("you"));
        System.out.println("Contains [me]?: " + words.contains("me"));
        System.out.println("Where's [say]?: " + words.indexOf("say"));
        System.out.println("Where's the last [say]?: " + words.lastIndexOf("say"));
        // lastIndexOf: tim vi tri xuat hien cuoi cung cua chuoi trong ds
        // indexOf: tim vi tri cua chuoi trong danh sach
        // Contains: kiem tra chuoi co trong danh sach khong

        Collections.sort(words);

        for (Object o : words) {
            System.out.println(o + " ");
        }
    }
}