import java.util.*;

public class LyricSet {
    public static final String[] lyric = {"you", "say", "it", "best", "when", "you", "say", "nothing", "at", "all"};


    /*public static void main(String[] args) {
      *  // Create a set from HashSet that's implemented Set
        Set words = new HashSet();
        // Add all words to set
        for (String w : lyric)
            words.add(w);

        for (Object o : words)
            System.out.println(o + " ");// in ra cac chuoi trong ds
        System.out.println("\n-------------");

        System.out.println("Contains [you]?: " + words.contains("you"));
        System.out.println("Contains [me]?: " + words.contains("me"));
    }*/
    /*
    * HashSet
    * thu tu sap xep cac tu tinh tu cuoi danh sach do len va co ghi de len nhau
    * */


    /* *public static void main(String[] args) {
        // Create a set from LinkedHashSet that’s implemented
        Set words = new LinkedHashSet();

        for (String w : lyric)
            words.add(w);

        for (Object o : words)
            System.out.println(o + " ");
        System.out.println("\n------------");

        System.out.println("Contains [you]?: " +  words.contains("you"));
        System.out.println("Contains [me]?: " + words.contains("me"));
    }*/
    /*
    * LinkedHashSet
    * thu tu in ra tinh tu dau danh sach va co ghi de len nhau
    */

    public static void main(String[] args) {
        // Create a set from TreeSet that’s implemented
        Set words = new TreeSet();

        for(String w : lyric)
            words.add(w);

        for (Object o : words)
            System.out.println(o + " ");
        System.out.println("\n------------");

        System.out.println("Contains [you]?: " + words.contains("you"));
        System.out.println("Contains [me]?: " + words.contains("me"));

        List words2 = new ArrayList();
        words2.addAll(words);
        System.out.println(words);

        Object retval = words2.get(3);
        System.out.println("Retrieved element is = " + retval);
    }
    /*
     * TreeSet
     * thu tu sap xep cac tu tinh tu cuoi danh sach do len va co ghi de len nhau
     * */
}