var sx = document.getElementById('sx'); // Element to hold ScreenX: yếu tố để giữ screenX
var sy = document.getElementById('sy'); // Element to hold ScreenY: yếu tố để giữ ScreenY
var px = document.getElementById('px'); // Element to hold pageX
var py = document.getElementById('py'); // Element to hold pageY
var cx = document.getElementById('cx'); // Element to hold clientX
var cy = document.getElementById('cy'); // Element to hold clientY

function showPosition(event) {          // Declare function : Khai báo hàm
    sx.value = event.screenX;           // Update element with screenX : update phần tử tới screenX
    sy.value = event.screenY;           // Update element with screenY 
    px.value = event.pageX;             // Update element with pageX
    py.value = event.pageY;             // Update element with pageY
    cx.value = event.clientX;           // Update element with clientX
    cy.value = event.clientY;           // Update element with clientY
}

var el = document.getElementById('body'); // get body Element 
el.addEventListener('mousemove', showPosition, false); // Move updates position
