var request;                                                //latest image to be requested: hình ảnh mới nhất đc yêu cầu
var $current;                                               //Image currently being shown :Hình ảnh hiện đang đc hiển thị  
var cache = {};                                             //Cache object : Đối tượng bộ nhớ đệm 
var $frame = $('#photo-viewer');                            //Container for image : thùng chứa hình ảnh 
var $thumbs = $('.thumb');                                 //Thumbnails : hình ảnh thu nhỏ 

function crossfade($img) {                                  //Function to fade between images: chức năng làm mờ chính giữa các hình ảnh 
                                                            // Pass in new image as paramenter : Chuyển hình ảnh làm tham số
    if ($current) {                                         //If  there is currently an image showing : Nếu hiện tại có một hình ảnh hiển thị     
        $current.stop().fadeOut('slow');                    //Stop any animation & fade it out : Dừng bất kỳ hoạt ảnh nào và làm mờ đi
    }

    $img.css({                                              // Set the CSS margins for the image: Thiết lập lề CSS của hình ảnh           
        marginLeft: -$img.width() / 2,                      // Negative margin of half image's width : Lề âm của một nửa chiều rộng hình ảnh
        marginTop: -$img.height() / 2                       // Negative margin of half image's height : Lề âm của một nửa chiều cao của hình ảnh
    });

    $img.stop().fadeTo('slow', 1);                          // Stop animation on new image & fade in: Dừng hoạt ảnh trên hình ảnh mới và mờ dần

    $current = $img;                                        // New image becomes current image : Hình ảnh mới trở thành hình ảnh hiện tại   
}

$(document).on('click', '.thumb',function(e){               // When a thumb is clicked on
    var $img,                                               // Create local variable called $img
        src = this.href;                                    // Store path to image 
        request = src;                                      // Store latest image request

    e.preventDefault();                                     // stop default link behavior 

    $thumbs.removeClass('active');                          // remove active link behavior 
    $(this).addClass('active');                             // Add active to clicked thumb
    if (cache.hasOwnProperty(src)){                         // If cache contains this image 
        if (cache[src].isLoading === false){                // And if isLoading is false 
            crossfade(cache[src].$img);                     // Call crossfade() function
        }
    } else {                                                // Otherwise it is  not in cache
        $img = $('<img/>');                                 // Store empty <img/> element in $img
        cache[src] = {                                      // Store this image in cache 
            $img: $img,                                     // Add tha path to the image 
            isLoading: true                                 // Set isLoading property to false
        };

        // Next few lines will run when image has loaded but are prepared first 
        $img.on('load', function() {                        // When image has loaded 
            $img.hide();                                    // Hide it 
            // Remove is-Loading class from frame & append new image to it
            $frame.removeClass('is-loading').append($img);
            cache[src],isLoading = false;                   // Update isLoading in cache 
            // If still most recently requested iamge then
            if (request === src) {
                crossfade($img);                            // Call crossfade() function
            }                                               // Solves asynchronous loading issue  
        });

        $frame.addClass('is-loading');                      // Add is-Loading  class to frame

        $img.attr({                                         // Set attributes on <img> element
            'src': src,                                     // Add src attributes to load image 
            'alt': this.title || ''                         // Add title if one was given in link
        });
    }

});

//Final line runs once when rest of script has loaded to show the first image 
$('.thumb').eq(0).click();                                  // Simulate click on first thumb