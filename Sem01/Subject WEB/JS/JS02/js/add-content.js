var today = new Date();// Tạo ra một đối tượng kiểu Date(Thời gian)
//new: toán tử dùng để cấp phát bộ nhớ cho đối tượng.
//Date() : method() aka hành vi của đối tượng, trong trường hợp này Date()
// là Constructor(hàm khởi tạo đối tượng.)

var hourNow = today.getHours();//Giờ hiện tại
//Kl: Để gọi ra thuộc tính (properties) hoặc hành vi(Method) 
//của một đối tượng.
//tên_đối_tượng. Tên_thuộc_ tính Or tên_đối_tượng.Tên_hành_vi().

var greeting;

if(hourNow>18){
    greeting = 'Good Evening!!!';
}else if(hourNow>12){
    greeting = 'Good Afternoon!!!';
}else if(hourNow>0){
    greeting = 'Good Morning!!!';
}else{
    greeting = 'Welcome!!!';
}
document.write("<h3>" + greeting + "</h3>");