$('accordion').on('click', '.accordion-control', function (e) {     // When clicked : Khi nhấp chuột 
    e.preventDefault();                                             // Prevent default action of button : Ngăn chặn hành động mặc định của nút
    $(this)                                                         // Get the element the user clicked on : Nhận phần tử mà người dùng đã nhấp vào  
        .next('.accordion-panel')                                   // Select following panel : Chọn bảng điều kiển sau  
        .not(':animated')                                           // If it is not currently animating : Nếu nó hiện không hoạt hình 
        .slideToggle();                                             // Use slide toggle to show or hide it : Sử dụng chuyển đổi trượt để hiển thị hoặc ẩn nó
});