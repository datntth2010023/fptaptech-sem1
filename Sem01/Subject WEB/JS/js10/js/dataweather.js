
  $.getJSON("http://dataservice.accuweather.com/forecasts/v1/daily/1day/353412?apikey=CxILqfbYMdKI30fs02iXyl2JZJdF2MeU&language=vi-vn&metric=true", function(data) {
      icon = "https://developer.accuweather.com/sites/default/files/0" + data.DailyForecasts[0].Day.Icon + "-s.png";
      iconPhrase = data.DailyForecasts[0].Day.IconPhrase;
      date = data.DailyForecasts[0].Date;
      tempMIN = "/ "+ data.DailyForecasts[0].Temperature.Minimum.Value + "°C";
      tempMAX = data.DailyForecasts[0].Temperature.Maximum.Value + "°C";
      text = data.Headline.Text;
      $('.date').html(date);
      $('.icon').attr('src', icon);
      $('.iconPhrase').html(iconPhrase);
      $('.tempMAX').html(tempMAX);
      $('.tempMIN').html(tempMIN);
      $('.text').html(text);
  })