$('.slider').each(function() {                                                // For every slider :Đối với mõi thanh trượt 
    var $this = $(this);                                                     // Current slider : Trang trình bày hiện tại 
    var $group = $this.find('.slide-group');                                 // Get the slide-group (container) : lấy vùng chứa nhóm thanh trượt 
    var $slides = $this.find('.slide');                                      // Create jQuery object to hold all sildes : tạo đối tượng jQuery để  giữ tất cả thanh trượt 
    var buttonArray = [];                                                    // Create array to hold navigation buttons : tạo mảng để giữ các nút điều hướng  
    var currentIndex = 0;                                                    // Hold index number ò the current slide : giữ số chỉ mục của trang trình bày hiện tại  
    var timeout;                                                             // Sets gap between auto-sliding : đặt khoảng cách giữa trượt tự động 
    
    function move(newIndex) {                                                // Creates the slide from old to new one : Tạo một trang trình bày từ cũ sang mới
        var animateLeft, slideLeft;                                          // Declare variables : Khai báo các biến
        
        advance();                                                           // When slide moves, call advance () again : khi thanh truợt di chuyển , hãy gọi lại trước

        // If it  the current slide / animating do nothing :Nếu đó là trang trình bày / hoạt ảnh hiện tại không làm gì cả 
        if ($group.is(':animated') || currentIndex === newIndex) {
            return;
        }
        
        buttonArray[currentIndex].removeClass('active');                     // Remove class from item : Xóa lớp khỏi mục  
        buttonArray[newIndex].addClass('active');                            // Add class to new item  : Thêm lớp vào mục mới

        if (newIndex > currentIndex) {                                       // If new item > current : Nếu mục mới > dòng 
            slideLeft = '100%';                                              // Sit  the new slide to the right : Đặt trang trình bày mới sang bên phải  
            animateLeft = '-100%';                                           // Animate the current group  to the left : Hoạt ảnh nhóm hiện tại sang bên trái
        } else {                                                             // Otherwise : Nếu không thì             
            slideLeft = '-100%';                                             // Sit the new slide to the left : Đặt trang trình bày mới sang bên trái
            animateLeft = '100%';                                            // Animate the current group to the right : Hoạt ảnh nhóm hiện tại sang bên phải   
        }
        // Position new slide to the left (if less) or right (if more) of current : Dịnh vị trang trình bày mới sang trái (nếu ít hơn ) hoặc sang phải (nếu nhiều hơn) hiện tại
        $slides.eq(newIndex).css({
            left: slideLeft,
            display: 'block'
        });

        $group.animate({
            left: animateLeft
        }, function() {                                                        // Animate slides and : Tạo hiệu ứng cho các trang trình bày và 
            $slides.eq(currentIndex).css({
                display: 'none'
            });                                                               // Hide previous silde : Ẩn trang trình bày trước đó
            $slides.eq(newIndex).css({
                left: 0
            });                                                               // Set position of the new item : Đặt lại vị trí của mục mới
            $group.css({
                left: 0
            });                                                               // Set position of the group of slides : Đặt lại vị trí của nhóm các trang trình bày
            currentIndex = newIndex;                                          // Set currentIndex to the new image : Đặt chỉ mục hiện tại thành hình ảnh mới       
        });
    }
    function advance() {                                                      // Used to set : Được sử dụng để thiết lập
        clearTimeout(timeout);                                                // Clear previous timeout : Xóa thời gian chờ trước đó
        timeout = setTimeout(function() {                                      // Set new timer : Thiết lập hẹn giờ mới 
            if (currentIndex < ($slides.length - 1)) {                        // If slide < total slides : Nếu 
                move(currentIndex + 1);                                       // Move to next slide : Chuyển sang trang trình bày tiếp theo
            } else {                                                          // Otherwise  : Nếu không thì
                move(0);                                                      // Move to the first slide : Chuyển sang trang trình bày đầu tiên
            }
        }, 4000);                                                             // Milliseconds timer will wait : Bộ hẹn giờ mili giây sẽ đợi 
    }
    $.each($slides, function(index) {
        // Create a button element for the button : Tạo một phần tử nút cho nút
        var $button = $('<button type="button" class="slide-btn">&bull;</button>');
        if (index === currentIndex) {                                          // If index is the current item : Nếu chỉ mục là mục hiện tại 
            $button.addClass('active');                                        // Add the active class : Thêm lớp hoạt động 
        }
        $button.on('click', function() {                                       // Create event handler for the button : Tạo trình xử lí sự kiện cho nút  
            move(index);                                                       // It calls the move() function : Nó gọi hàm di chuyển 
        }).appendTo('.slide-buttons');                                         // Add to the buttons holder : Thêm vào giá đỡ nút 
        buttonArray.push($button);                                             // Add it to the button array : Thêm nó vào mảng nút
    });
    advance();                                                                 // Script is set up, advance() to move it : tập lệnh được thiết lập , Tiến tới việc di chuyển nó
});