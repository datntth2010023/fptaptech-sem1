function formValidation() {
    let name = document.forms["RegForm"]["name"];
    
    let courses = document.forms["RegForm"]["subject"];
    let confirmPass = document.getElementById("confirmPass");
    let email = document.getElementById("email");
    var letter = /^[A-Za-z]+$/;

    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2-4})+$/;
    let password = document.forms["RegForm"]["password"];
    var password_expression = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*-])/;
    if (name.value == "") {
      alert("Pls, Enter Your Name");
      name.focus();
      return false;
    }
    else if(!letter.test(name)){
        alert("Chi nhap ky tu tu a-z A-Z")
    }
    else if(!filter.test(email)){
      alert("Invalid Email");
    }
    else if (password.value == "") {
      alert("Pls Enter Password");
      password.focus();
      return false;
    }
    else if (password.value.length < 6) {
      alert("Pass ko dc it hon 6 ki tu");
      return false;
    }
    else if (password.value.length > 12) {
      alert("Pass ko dc nhieu hon 12 ki tu");
      return false;
    }
    else if (password.value !== confirmPass.value) {
      alert("Password not match");
      return false;
    }
    else if (courses.selectedIndex < 1) {
      alert("Pls Enter khoa hoc");
      courses.focus();
      return false;
    }
    else {
      return true;
    }
  }
  