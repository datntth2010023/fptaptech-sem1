(function () {

    var $imgs = $('#gallery img');                               // Store all images: lưu trữ  tất cả hình ảnh
    var $buttons = $('#buttons');                                // Store buttons element : Lưu trữ các phần tử nút
    var tagged = {};                                             // Create tagged object: tạo đối tượng được gắn thẻ

    $imgs.each(function() {                                      // Loop through images and: lặp qua hình ảnh và
        var img = this;                                          // Store  img in variable : luu giữ hình ảnh trong biến
        var tags = $(this).data('tags');                         // Get this element's tags : gửi các phần tử vào các thẻ

        if (tags) {                                              // If the element had tags : Nếu phần tử có thẻ
            tags.split(',').forEach(function(tagName) {          // Split at comma and : có tách ở dấu phẩy và
                if (tagged[tagName] == null) {                   // If object doesn't have tag: Nếu đối tượng không có thẻ, 
                    tagged[tagName] = [];                        // Add empty array to object: hãy thêm mảng trống vào đối tượng  
                }
                tagged[tagName].push(img);                       // Add the image to the array: Thêm hình ảnh vào mảng   
            });
        }
    });

    $('<button/>', {                                             // Create empty button : Tạo nút trống
        text: 'Show All',                                        // Add text 'Show All': Thêm văn bản 'Hiển thị Tất cả'
        class: 'active',                                         // Make it active : Làm cho nó hoạt động
        click: function() {                                      // Add onclick handler to : Thêm trình xử lý onclick vào
            $(this)                                              // Get the clicked on button : Nhận nút đã nhấp vào
                .addClass('active')                              // Add the class of active : Thêm vào loại hoạt động
                .siblings()                                      // Get its siblings :
                .removeClass('active');                          // Remove active from sibings:
            $imgs.show();                                        // Show all images: Hiển thị toàn bộ ảnh  
        }
    }).appendTo($buttons);                                       // Add to buttons : Thêm vào các nút

    $.each(tagged, function(tagName) {                           // For each tag name : Đối với mỗi tên thẻ 
        $('<button/>', {                                         // Create empty button : tạo nút trống
            text: tagName + '(' + tagged[tagName].length + ')',  // Add tag name : thêm tên thẻ 
            click: function() {                                  // Add click handler : thêm trình xử lí nhấp chuột
                $(this)                                          // The button clicked on : Nút được nhấp vào 
                    .addClass('active')                          // Make clicked item active : làm cho mục được nhấp hoạt động 
                    .siblings()                                  // Get its siblings : 
                    .removeClass('active');                      // Remove active from siblings
                $imgs                                            // Will all of the images :Tất cả hình ảnh 
                    .hide()                                      // Hide them : ẩn chúng 
                    .filter(tagged[tagName])                     // Find ones with this tag : tìm tất cả ảnh có trong thẻ này 
                    .show();                                     // Show just those images : chỉ  hiển thị  những hình ảnh đó 
            }
        }).appendTo($buttons);                                   // Add to the buttons : thêm vào các nút
    });

}());