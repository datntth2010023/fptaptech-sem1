(function () { //lives in a IIFE    
    var $imgs = $('#gallery img');//Get the images : lấy hình ảnh
    var $search = $('#filter-search');//Get the input element : lấy phần tử đầu vào 
    var cache = [];

    $imgs.each(function() {//For each image : cho mỗi hình ảnh 
        cache.push({//Add an object to the cache array: thêm vào một đối tượng cho bộ nhớ đệm của mảng
            element: this,//This image : hình ảnh này
            text: this.alt.trim().toLowerCase()//Its alt text (lowerCase trimmed) : Văn bản they thế của nó(lowerCase được cắt bớt )
        });
    });

    function filter() {//Declare filter() function : Khai báo chức năng bộ lọc
        var query = this.value.trim().toLowerCase();//Get the query : nhận được truy vấn
        cache.forEach(function (img) {//For each entry in cache pass image : cho mỗi mục nhập trong hình ảnh vượt qua bộ nhớ đệm    
            var index = 0;//Set index to 0 : đặt chỉ mục thành 0

            if (query) {//If there is some query text : Nếu có một số văn bản truy vấn  
                index = img.text.indexOf(query);//Find if query text is in there : tìm văn bản truy vấn xem có ở đó không 
            }

            img.element.style.display  = index === -1 ? 'none' : '';// Show/hide : cho xem / ẩn giấu
        });
    }
    
    if ('oninput' in $search[0]) {     //If browser supports input event : nếu trình duyệt hộ trợ sự kiện đầu vào 
        $search.on('input', filter);  //Use input event to call filter(): sử dụng sự kiện đầu vào để gọi bộ lọc 
    } else {                          //Otherwise  
        $search.on('keyup', filter);  //Use key up event to call filter() : sự dụng chìa khóa để gọi bộ lọc 
    }
}());