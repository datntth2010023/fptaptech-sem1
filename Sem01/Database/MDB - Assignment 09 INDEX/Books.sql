﻿IF EXISTS (SELECT * FROM sys.databases WHERE Name='BOOKS')
DROP DATABASE BOOKS
GO
CREATE DATABASE Books
GO
USE Books
GO

--BẢNG CUSTOMERS
CREATE TABLE Customers(
CustomerID INT PRIMARY KEY IDENTITY(1,1),
CustomerName VARCHAR(50),
Adress VARCHAR(100),
Phone VARCHAR(12)
)
GO

--BẢNG BOOK
CREATE TABLE Books(
BookCode INT PRIMARY KEY,--mã sách
Category VARCHAR(50),--thể loại
Author VARCHAR(50),--tên tác giả
Publisher VARCHAR(50),--Tên nhà Xuất Bản
Title VARCHAR(50),--tiêu đề cuốn sách
Price INT , --Giá bán của cuốn sách
InStore INT,--số lượng sách còn trong Kho
)
GO

--BẢNG BOOKSOLD(sách đã bán)
CREATE TABLE BookSolds (
BookSoldID INT PRIMARY KEY,--MÃ HÓA ĐƠN SÁCH
CustomerID INT ,--mã khách hàng
BookCode INT ,--mã sách
Date DATETIME,--ngày bán sách
Price INT ,--giá sách tại thời điểm hiện tại
Amount INT --số lượng sách đã bán
CONSTRAINT BS_C FOREIGN KEY (CustomerID) REFERENCES Customers(CustomerID),
CONSTRAINT BS_B FOREIGN KEY (BookCode) REFERENCES Books(BookCode)
) 
GO

-- Chèn dữ liệu CUSTOMER
SET IDENTITY_INSERT Customers ON

INSERT INTO Customers(CustomerID,CustomerName,Adress,Phone)  VALUES(1001,'NAM','QUẢNG NAM',029374727389)
INSERT INTO Customers(CustomerID,CustomerName,Adress,Phone)  VALUES(1002,'MINH','QUẢNG BÌNH',029574727388)
INSERT INTO Customers(CustomerID,CustomerName,Adress,Phone)  VALUES(1003,'VŨ','HÀ NAM',025374727386)
INSERT INTO Customers(CustomerID,CustomerName,Adress,Phone)  VALUES(1004,'THỦY','BẮC KẠN',029374727345)
INSERT INTO Customers(CustomerID,CustomerName,Adress,Phone)  VALUES(1005,'LONG','HỒ CHÍ MINH',029374827389)
INSERT INTO Customers(CustomerID,CustomerName,Adress,Phone)  VALUES(1006,'ĐỨC','HÀ NỘI',099374727382)

SET IDENTITY_INSERT Customers OFF 

--chèn dữ liệu cho books
INSERT INTO Books VALUES (2001,'Văn Học','Nguyễn Du','Giáo Dục','Truyện Kiều',30000,100)
INSERT INTO Books VALUES (2002,'Kinh Tế','BBC','BCC','Tạp Chí Phố Wall',50000,50)
INSERT INTO Books VALUES (2003,'Thời Trang','Vmode','Tuổi Trẻ','Tạp Chí thời trang Vmode',50000,100)
INSERT INTO Books VALUES (2004,'Văn Học','Tô Hoài','Sư Phạm','Dế mèn phưu lưu ký',30000,100)
INSERT INTO Books VALUES (2005,'Kịch','J.K.Rowlling','Bloomsbury','Harrypotter',200000,100)

-- chèn dữ liệu vào bảng BookSold
INSERT INTO BookSolds(BookSoldID,CustomerID,BookCode,Date,Price,Amount) VALUES (011,1001,2005,16-3-2021,200000,7)
INSERT INTO BookSolds(BookSoldID,CustomerID,BookCode,Date,Price,Amount) VALUES (012,1001,2003,16-3-2021,50000,2)
INSERT INTO BookSolds(BookSoldID,CustomerID,BookCode,Date,Price,Amount) VALUES (013,1002,2004,17-3-2021,30000,2)
INSERT INTO BookSolds(BookSoldID,CustomerID,BookCode,Date,Price,Amount) VALUES (014,1002,2001,18-3-2021,30000,3)
INSERT INTO BookSolds(BookSoldID,CustomerID,BookCode,Date,Price,Amount) VALUES (015,1003,2005,18-3-2021,200000,5)
INSERT INTO BookSolds(BookSoldID,CustomerID,BookCode,Date,Price,Amount) VALUES (016,1003,2002,18-3-2021,50000,4)
INSERT INTO BookSolds(BookSoldID,CustomerID,BookCode,Date,Price,Amount) VALUES (017,1004,2003,19-3-2021,50000,10)
INSERT INTO BookSolds(BookSoldID,CustomerID,BookCode,Date,Price,Amount) VALUES (018,1004,2005,19-3-2021,200000,2)
INSERT INTO BookSolds(BookSoldID,CustomerID,BookCode,Date,Price,Amount) VALUES (019,1005,2002,20-3-2021,50000,10)
INSERT INTO BookSolds(BookSoldID,CustomerID,BookCode,Date,Price,Amount) VALUES (020,1006,2003,21-3-2021,50000,10)

SELECT * FROM Customers
SELECT * FROM Books
SELECT * FROM BookSolds
GO

--Tạo một khung nhìn chứa danh sách các cuốn sách (BookCode, Title, Price) kèm theo số lượng đã
--bán được của mỗi cuốn sách.
CREATE  VIEW BookSoldsList 
AS SELECT 
B.BookCode,
Title,
Price FROM 


