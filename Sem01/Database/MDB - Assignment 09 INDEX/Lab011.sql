CREATE DATABASE Lab11
GO
USE Lab11
GO

CREATE VIEW ProductList AS
SELECT ProductID, Name FROM AdventureWorks2014.Production.Product

SELECT * FROM ProductList

CREATE VIEW SalesOrderDetail
AS
SELECT pr.ProductID, pr.Name, od.UnitPrice, od.OrderQty, od.UnitPrice*od.OrderQty as[Total Price]
FROM AdventureWorks2014.Sales.SalesOrderDetail od
JOIN AdventureWorks2014.Production.Product pr
ON OD.ProductID=pr.ProductID

SELECT * FROM  SalesOrderDetail