﻿IF EXISTS (SELECT * FROM sys.databases WHERE Name= 'Bank')
DROP DATABASE Bank
GO
--tạo data Bank
CREATE DATABASE Bank
GO
USE Bank
GO

--tạo bảng khách hàng Customers
CREATE TABLE Customers(
Cust_ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,--NOT NUL:Bắt buộc phải có dữ liệu
Address VARCHAR(50),
City VARCHAR(20) NOT NULL,
FED_ID VARCHAR(20) NOT NULL,
)
GO


--tạo bảng thông tin tài khoản khách hàng
CREATE TABLE Account(
Account_ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
Balance FLOAT,
Open_Date DATETIME,
Close_Date DATETIME,
STATUS VARCHAR(10),
Cust_ID INT,
Open_Branch_ID INT NOT NULL,-- mở tại chi nhánh nào
Open_EMP_ID INT NOT NULL,-- id nhân viên mở tài khoản
Product_CD VARCHAR(10) NOT NULL,--loại tiền
)
GO

DROP TABLE Account

--Tạo bảng chi nhánh
CREATE TABLE Branch(
Branch_ID INT IDENTITY(1,1) NOT NULL,
Address VARCHAR(30),
Name VARCHAR(30) NOT NULL,
PRIMARY KEY(Branch_ID)
)
GO

--Tạo bảng nhân viên
CREATE TABLE Employee(
Employee_ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
Name VARCHAR(50) NOT NULL,
Address VARCHAR(50) NOT NULL,
NgaySinh DATETIME,
Branch_ID INT,
CONSTRAINT Employee_Branch_fk FOREIGN KEY (Branch_ID) REFERENCES Branch(Branch_ID)
)
GO

--tạo bảng Product
CREATE TABLE Product(
Product_CD VARCHAR(10) NOT NULL,
DATE_OFFERED DATETIME,--Ngày mở sản phẩm
DATE_RETIED DATETIME,-- Ngày đóng sản phẩm
Name VARCHAR(50) NOT NULL,
Product_TYE_CD VARCHAR(100),--loại sản phẩm
PRIMARY KEY(Product_CD)
)
GO

--tạo bảng product_type
CREATE TABLE Product_Type(
Product_Type_CD VARCHAR(100) PRIMARY KEY NOT NULL,
Name VARCHAR(50)
)
GO

-- THÊM CÁC KHÓA NGOẠI
ALTER TABLE Account ADD CONSTRAINT Account_Customers_FK 
FOREIGN KEY (Cust_ID) REFERENCES Customers(Cust_ID);

ALTER TABLE Account ADD CONSTRAINT Account_Branch_FK
FOREIGN KEY (Open_Branch_ID) REFERENCES Branch;

ALTER TABLE Account ADD CONSTRAINT Account_Product_FK
FOREIGN KEY (Product_CD) REFERENCES Product;
GO

--đưa dữ liệu vào bảng branch
SET IDENTITY_INSERT Branch ON
INSERT INTO Branch(Branch_ID,Name,Address) VALUES (01,'Hội Sở','Hai Bà Trưng')
SET IDENTITY_INSERT Branch OFF

SELECT * FROM Branch