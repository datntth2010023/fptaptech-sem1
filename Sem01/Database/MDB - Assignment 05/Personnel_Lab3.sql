﻿CREATE DATABASE Personnel
GO
USE Personnel
GO

--TẠo bảng  PhongBan
CREATE TABLE PhongBan(
     MaPB varchar(7) PRIMARY KEY,
	 TenPB nvarchar(50)
)
GO

--Tạo bảng NHANVIEN
CREATE TABLE NhanVien(
    MaNV VARCHAR(7) NOT NULL PRIMARY KEY,
	TenNV NVARCHAR(50),
	NgaySinh DATETIME CONSTRAINT checkBirthDate CHECK (DATEDIFF(day, NgaySinh, GETDATE()) > 1),
	SoCMND CHAR(9) CONSTRAINT beNumber CHECK (SoCMND NOT LIKE '%[^0-9]%'),
	GioiTinh CHAR(1) CONSTRAINT beMF CHECK (GioiTinh IN ('M','F')) DEFAULT 'M',
	DiaChi NVARCHAR(100),
	NgayVaoLam DATETIME,
	MaPB VARCHAR(7) CONSTRAINT NhanVien_PhongBan_FK FOREIGN KEY (MaPB) REFERENCES PhongBan(MaPB),
	CONSTRAINT checkJoinDate CHECK (DATEDIFF(year, NgaySinh, NgayVaoLam) > 20)
)
GO

-- tạo bảng Lượng dự án
CREATE TABLE LuongDA(
     MaDA VARCHAR(8) NOT NULL,--Mã dữ án
	 MaNV VARCHAR(7) NOT NULL ,--mã nhân viên
	 NgayNhan DATETIME NOT NULL DEFAULT GETDATE() ,
	 SoTien MONEY, -- số tiền của dự án
	 CONSTRAINT FK_LuongDA_NV FOREIGN KEY (MaNV) REFERENCES NHANVIEN(MaNV),
	 CONSTRAINT SoTien CHECK (SoTien > 0),
	 PRIMARY KEY(MaDA, MaNV)
)
GO
/*1. Thực hiện chèn dữ liệu vào các bảng vừa tạo (ít nhất 5 bản ghi cho mỗi bảng).*/
--Bảng PHONGBAN
INSERT INTO PHONGBAN(MaPB,TenPB) VALUES ('PB1', 'PB1');
INSERT INTO PHONGBAN(MaPB,TenPB) VALUES ('PB2', 'PB2');
INSERT INTO PHONGBAN(MaPB,TenPB) VALUES ('PB3', 'PB3');
INSERT INTO PHONGBAN(MaPB,TenPB) VALUES ('PB4', 'PB4');
INSERT INTO PHONGBAN(MaPB,TenPB) VALUES ('PB5', 'PB5');

--Bảng NHANVIEN
INSERT INTO NhanVien(MaNV,TenNV,NgaySinh,SoCMND,GioiTinh,DiaChi,NgayVaoLam,MaPB) VALUES ('NV1', 'NV1', '20000101 00:00:00', '012345678','M', 'DiaChi1','20210101 00:00:00','PB1');
INSERT INTO NhanVien(MaNV,TenNV,NgaySinh,SoCMND,GioiTinh,DiaChi,NgayVaoLam,MaPB) VALUES ('NV2', 'NV2', '20000102 00:00:00', '014243242','F', 'DiaChi2','20210102 00:00:00','PB2');
INSERT INTO NhanVien(MaNV,TenNV,NgaySinh,SoCMND,GioiTinh,DiaChi,NgayVaoLam,MaPB) VALUES ('NV3', 'NV3', '20000103 00:00:00', '123219923','F', 'DiaChi3','20210103 00:00:00','PB3');
INSERT INTO NhanVien(MaNV,TenNV,NgaySinh,SoCMND,GioiTinh,DiaChi,NgayVaoLam,MaPB) VALUES ('NV4', 'NV4', '20000104 00:00:00', '312343243','F', 'DiaChi4','20210104 00:00:00','PB4');
INSERT INTO NhanVien(MaNV,TenNV,NgaySinh,SoCMND,GioiTinh,DiaChi,NgayVaoLam,MaPB) VALUES ('NV5', 'NV5', '20000105 00:00:00', '544363453','M', 'DiaChi5','20210105 00:00:00','PB5');
INSERT INTO NhanVien(MaNV,TenNV,NgaySinh,SoCMND,GioiTinh,DiaChi,NgayVaoLam,MaPB) VALUES ('NV6', 'NV6', '20000106 00:00:00', '324252432','M', 'DiaChi6','20210106 00:00:00','PB1');

--Bảng LuongDA
INSERT INTO LuongDA(MaDA,MaNV,NgayNhan,SoTien) VALUES ('DA1', 'NV1', '20150101 00:00:00', 30000);
INSERT INTO LuongDA(MaDA,MaNV,NgayNhan,SoTien) VALUES ('DA2', 'NV2', '20160101 00:00:00', 20000);
INSERT INTO LuongDA(MaDA,MaNV,NgayNhan,SoTien) VALUES ('DA3', 'NV3', '20170101 00:00:00', 20000);
INSERT INTO LuongDA(MaDA,MaNV,NgayNhan,SoTien) VALUES ('DA4', 'NV4', '20180101 00:00:00', 10000);
INSERT INTO LuongDA(MaDA,MaNV,NgayNhan,SoTien) VALUES ('DA5', 'NV5', '20190101 00:00:00', 50000);
INSERT INTO LuongDA(MaDA,MaNV,NgayNhan,SoTien) VALUES ('DA6', 'NV1', '20150401 00:00:00', 30000);
INSERT INTO LuongDA(MaDA,MaNV,NgayNhan,SoTien) VALUES ('DA2', 'NV6', '20170701 00:00:00', 10000);

/*2. Viết một query để hiển thị thông tin về các bảng LUONGDA, NHANVIEN, PHONGBAN.*/
SELECT * FROM PhongBan
SELECT * FROM NhanVien
SELECT * FROM LuongDA;
GO
/*3. Viết một query để hiển thị những nhân viên có giới tính là ‘F’.*/
SELECT * FROM NhanVien WHERE GioiTinh = 'F'
GO
/*4. Hiển thị tất cả các dự án, mỗi dự án trên 1 dòng.*/
SELECT MaDA FROM LuongDA
GO
/*5. Hiển thị tổng lương của từng nhân viên (dùng mệnh đề GROUP BY).*/
SELECT NhanVien.TenNV AS Ten_Nhan_Vien, SUM(LuongDA.SoTien) AS Tong_Luong 
FROM LuongDA INNER JOIN NhanVien ON NhanVien.MaNV = LuongDA.MaNV
GROUP BY NhanVien.TenNV
GO
/*6. Hiển thị tất cả các nhân viên trên một phòng ban cho trước (VD: ‘Hành chính’).*/
SELECT NhanVien.TenNV AS Ten_Nhan_Vien,PhongBan.TenPB  AS Ten_Phong_Ban
FROM NhanVien INNER JOIN PhongBan ON PhongBan.MaPB = NhanVien.MaPB
WHERE PhongBan.TenPB ='PB1' 
GO
/*7. Hiển thị mức lương của những nhân viên phòng hành chính.*/
SELECT NhanVien.TenNV AS Ten_Nhan_Vien , SUM(LuongDA.SoTien) AS Tien_Luong
FROM LuongDA INNER JOIN NhanVien ON NhanVien.MaNV = LuongDA.MaNV
INNER JOIN PhongBan ON PhongBan.MaPB = NhanVien.MaPB
WHERE PhongBan.MaPB = 'PB1'
GROUP BY TenNV,TenPB,SoTien
GO

/*8. Hiển thị số lượng nhân viên của từng phòng.*/
SELECT COUNT(MaNV) FROM NhanVien 
INNER JOIN PhongBan ON NhanVien.MaPB = PhongBan.MaPB
WHERE PhongBan.MaPB = 'PB2'
GO

/*9. Viết một query để hiển thị những nhân viên mà tham gia ít nhất vào một dự án.*/
SELECT NhanVien.TenNV FROM NhanVien
INNER JOIN LuongDA ON NhanVien.MaNV = LuongDA.MaNV
WHERE LuongDA.MaDA ='DA2'
GO

/*10. Viết một query hiển thị phòng ban có số lượng nhân viên nhiều nhất.*/
SELECT COUNT(MaNV) FROM NhanVien
INNER JOIN PhongBan ON NhanVien.MaPB = PhongBan.MaPB
WHERE 