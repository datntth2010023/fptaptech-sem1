﻿--Lấy ra dữ liệu của bảng Person có BusinessEntityID >= 00 và  BusinessEntityID<= 200
SELECT * FROM Person.Person WHERE BusinessEntityID >=100 AND BusinessEntityID <=200
-- Lấy ra dữ liệu của bảng Person có BusinessEntityID trong khoảng [100, 200]
SELECT * FROM Person.Person WHERE BusinessEntityID BETWEEN 100 AND 200
-- Lấy ra những person có LastName kết thúc bởi ký tự e
SELECT * FROM Person.Person WHERE LastName LIKE '%e'
--Lấy ra những person có Lastname bắt đầu bởi ký tự R hoặc A kết thúc bởi ký tự e
SELECT * FROM Person.Person WHERE LastName LIKE '[RA]%e'
-- lấy ra những person có lastname có 4 ký tự bắt đầu bởi ký tự R hoặc A kết thúc bởi ký tự e
SELECT * FROM Person.Person WHERE LastName LIKE '[RA]__e'
--Sử dụng INNER JOIN
SELECT Person.Person. * FROM Person.Person INNER JOIN HumanResources.Employee ON 
Person.Person.BusinessEntityID=HumanResources.Employee.BusinessEntityID
SELECT Title, COUNT(*) [Title Number] FROM Person.Person WHERE Title LIKE 'Mr%'
GROUP BY ALL Title 
-- GROUP BY với HAVING: mệnh đề HAVING sẽ lọc kết quả trong lúc được gộp nhóm 
SELECT Title, COUNT(*) [ Title Number]
FROM Person.Person GROUP BY ALL Title HAVING Title LIKE 'Mr%'