﻿IF EXISTS (SELECT * FROM sys.databases WHERE Name = 'Example5')
DROP DATABASE Example5
GO
CREATE DATABASE Example5
GO
USE Example5
GO
--Tạo bảng Lớp học
CREATE TABLE LopHoc(
MaLopHoc INT PRIMARY KEY IDENTITY,
TenLopHoc VARCHAR(10)
)
GO
-- Tạo bảng Sinh viên có khóa ngoại là cột MaLopHoc, Nối với bảng LopHoc
CREATE TABLE SinhVien(
MaSV INT PRIMARY KEY ,
TenSV VARCHAR(40),
MaLopHoc INT,
CONSTRAINT fk FOREIGN KEY (MaLopHoc) REFERENCES LopHoc(MaLopHoc)
)
GO
-- Tạo bảng SanPham với một cột NULL,một cột NOT NULL
CREATE TABLE SanPham(
MaSP INT NOT NULL,
TenSP VARCHAR(40) NULL
)
GO
-- tạo bảng với thuộc tính default cho cột Price
CREATE TABLE StoreProduct(
ProductID INT NOT NULL,
Name VARCHAR(40) NOT NULL,
Price MONEY NOT NULL DEFAULT (100)
)
-- Thử kiểm tra xem giá trị default có được sử dụng hay không
INSERT INTO StoreProduct(ProductID, Name) VALUES (111, 'Rivets')
GO
--Tạo bảng ContactPhone với thuộc tính IDENTITY
CREATE TABLE ContactPhone(
Person_ID INT IDENTITY(500,1) NOT NULL,
MobileNumber BIGINT NOT NULL
)
GO
--Tạo cột nhận dạng duy nhất tổng thể 
CREATE TABLE CellularPhone(
Person_ID UNIQUEIDENTIFIER DEFAULT NEWID() NOT NULL,
PersonName VARCHAR(60) NOT NULL
)
-- chèn một record'ghi nhớ' vào
INSERT INTO CellularPhone(PersonName) VALUES ('William Smith')
GO
-- Kiểm tra giá trị của cột Person_ID tự động sinh
SELECT * FROM CellularPhone
GO
--Tạo Bảng ContactPhone với một cột MobileNumber có thuộc tính UNIQUE
CREATE TABLE ContactPhone(
Person_ID INT PRIMARY KEY,
MobileNumber BIGINT UNIQUE,
ServiceProvider VARCHAR(30),
LandlineNumber BIGINT UNIQUE
)
--Chèn 2 bản ghi có giá trị giống nhau ở cột MobileNumber và LandliNumber để quan sát lỗi
INSERT INTO ContactPhone VALUES (101, 983345674, 'Hutch', NULL)
INSERT INTO ContactPhone VALUES (102, 983345674, 'Alex', NULL)
GO
--Tạo bảng PhoneExpenses có một CHECT ở cột Amount
CREATE TABLE PhoneExpenses(
Expense_ID INT PRIMARY KEY,
MobileNumber BIGINT FOREIGN KEY REFERENCES ContactPhone(MobileNumber),
Amount BIGINT CHECK (Amount >0)
) 
GO
-- Chỉnh sửa cột trong bảng
ALTER TABLE ContactPhone 
DROP COLUMN SevriceProvider
GO
-- Thêm một ràng buộc vào bảng
ALTER TABLE ContactPhone ADD CONSTRAINT CHK_RC CHECK(RentalCharges >0)
GO
-- Xóa một ràng Buộc
ALTER TABLE Per.ContactPhone 
DROP CONSTRAINT CHK_RC