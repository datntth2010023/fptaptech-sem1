﻿IF EXISTS (SELECT * FROM sys.databases WHERE Name ='BookLibrary')
DROP DATABASE BookLibrary
GO

-- tạo database BookLibrary
CREATE DATABASE BookLibrary
GO
USE BookLibrary
GO

-- Tạo bảng Book(Lưu thông tin các cuốn sách)
CREATE TABLE Book(
BookCode INT  NOT NULL IDENTITY(1,1),--Dùng để xđ mỗi cuốn sách là duy nhất 
BookTitle VARCHAR(100) NOT NULL,--TIÊU ĐỀ SÁCH
Author VARCHAR(50) NOT NULL, -- tên tác giả
Edition INT ,--lần xuất bản
BookPrice MONEY, --giá bán
Copies INT,-- Số cuốn có trong thư viện
CONSTRAINT PK_BookCode
PRIMARY KEY(BookCode)
)
GO

-- Tạo bảng Member(Lưu thông tin người mượn)
CREATE TABLE Member(
MemberCode INT  IDENTITY(1,1),--Dùng để xđ người mượn là duy nhất
Name VARCHAR(50) NOT NULL,--TÊN NGƯỜI MƯỢN
Address VARCHAR(100) NOT NULL,--ĐỊA CHỈ NGƯỜI MƯỢN
PhoneNumber INT --số điện thoại
CONSTRAINT PK_MemberCode 
PRIMARY KEY(MemberCode)
)
GO

--Tạo bảng IssueDetails(Thông tin mượn sách)
CREATE TABLE IssueDetails(
BookCode INT,--mã cuốn sách có trong bảng Book
MemberCode INT,--mã người mượn có trong bảng Member
IssueDate DATETIME,--ngày mượn sách
ReturnDate DATETIME, --ngày trả sách
CONSTRAINT IB FOREIGN KEY (BookCode) REFERENCES Book(BookCode),
CONSTRAINT IM FOREIGN KEY (MemberCode) REFERENCES Member(MemberCode)
)
GO

--Xóa bỏ các ràng buộc Khóa ngoại của Bảng IssueDetails 
ALTER TABLE IssueDetails DROP CONSTRAINT IB
ALTER TABLE IssueDetails DROP CONSTRAINT IM;
GO

--Xóa bỏ Ràng buộc khóa chính của bảng Member và Book
ALTER TABLE Book DROP CONSTRAINT PK_BookCode
ALTER TABLE Member DROP CONSTRAINT PK_MemberCode;
GO

--Thêm mới Ràng Buộc Khóa chính cho bảng Member và Book
ALTER TABLE Book ADD CONSTRAINT PK_BookCode PRIMARY KEY(BookCode)
ALTER TABLE Member ADD CONSTRAINT PK_MemberCode PRIMARY KEY(MemberCode);
GO

--Thêm mới các ràng buộc Khóa ngoại cho bảng IssueDetails
ALTER TABLE IssueDetails ADD CONSTRAINT IB FOREIGN KEY (BookCode) REFERENCES Book(BookCode)
ALTER TABLE IssueDetails ADD CONSTRAINT IM FOREIGN KEY (MemberCode) REFERENCES Member(MemberCode)
GO

--Bổ sung thêm ràng buộc  giá bán sách >0 và <200(chưa đc)
ALTER TABLE Book ADD CONSTRAINT CHK_BookPrice CHECK ( 0 < BookPrice AND BookPrice <200 );
GO

--Bổ sung thêm ràng buộc duy nhất cho PhoneNumber của bảng Member
ALTER TABLE Member ADD UNIQUE (PhoneNumber);
GO

--Bổ sung thêm ràng buộc NOT NULL cho BookCode, MemberCode trong Bảng IssueDetails
ALTER TABLE dbo.IssueDetails ALTER COLUMN BookCode INT NOT NULL
ALTER TABLE dbo.IssueDetails ALTER COLUMN MemberCode INT NOT NULL;
GO

--Tạo khóa chính gồm 2 cột BookCode, MemberCode cho bảng IssueDetails
ALTER TABLE IssueDetails ADD CONSTRAINT I_BM PRIMARY KEY(BookCode,MemberCode)
GO

--Chèn dữ liệu hợp lý cho các Bảng (sử dụng SQL)
--Bảng Book 
SET IDENTITY_INSERT Book ON
INSERT INTO Book(BookCode,BookTitle,Author,Edition,BookPrice,Copies) VALUES(001,'LEARNING HOW TO LEARN','B.Oakley,Terry Sejnowski, Al McConville',3,139,30)
INSERT INTO Book(BookCode,BookTitle,Author,Edition,BookPrice,Copies) VALUES(002,'SHERLOCK HOLEMS','Athur Connan Doyle',20,180,100)
SET IDENTITY_INSERT Book OFF

--Bảng Member
SET IDENTITY_INSERT Member ON
INSERT INTO Member(MemberCode,Name,Address,PhoneNumber) VALUES(01,'THANH DAT','HA NOI','0293729289')
INSERT INTO Member(MemberCode,Name,Address,PhoneNumber) VALUES(02,'VU THAN','HA NAM','0293724579')
INSERT INTO Member(MemberCode,Name,Address,PhoneNumber) VALUES(03,'MINH LONG','SAI GON','0969729289')
SET IDENTITY_INSERT Member OFF

--Bảng IssueDetails
INSERT INTO IssueDetails(BookCode,MemberCode,IssueDate,ReturnDate) VALUES (001,01,23-12-2020,30-12-2020)
INSERT INTO IssueDetails(BookCode,MemberCode,IssueDate,ReturnDate) VALUES (002,02,12-1-2021,30-1-2021)
INSERT INTO IssueDetails(BookCode,MemberCode,IssueDate,ReturnDate) VALUES (002,03,23-2-2021,15-3-2021)
GO

SELECT * FROM Book
SELECT * FROM Member
SELECT * FROM IssueDetails