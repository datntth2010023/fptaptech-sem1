﻿CREATE DATABASE The_FPT_Aptech_Library
GO
USE The_FPT_Aptech_Library
GO

/*1. Create the tables to the above designing.*/

CREATE TABLE Books(
       BookID int identity(100,1) NOT null
	  CONSTRAINT PK_BooKID PRIMARY KEY(BookID),
	  Title nvarchar(200) not null,
	  Author nvarchar(100) not null,--tên tác giả
	  ISBN varchar(50),--Mã tiêu chuẩn quốc tế Book
	  Image varchar(150),
	  Summary nvarchar(300)
)
GO

CREATE TABLE Users(
       UserID varchar(20) 
	   CONSTRAINT PK_UsersID PRIMARY KEY(UserID),
	   FullName nvarchar(100),
	   Email varchar(30)
	   CONSTRAINT UC_Email UNIQUE(Email),
	   Address nvarchar(200)
)
GO

CREATE TABLE Loans(
       LoanID int identity
	   CONSTRAINT PK_LoanID PRIMARY KEY (LoanID),
	   UserID varchar(20)
	   CONSTRAINT FK_UserID FOREIGN KEY REFERENCES Users(UserID),
	   BookID int 
	   CONSTRAINT FK_BookID FOREIGN KEY REFERENCES Books(BookID),
	   DueDate datetime,
	   Created datetime  DEFAULT GETDATE(),
	   Modified datetime
)
GO

/*2. Create an unique constraint named UQ_Books_ISBN on ISBN column on Books
table.*/

ALTER TABLE BooKs 
ADD CONSTRAINT UQ_Books_ISBN  UNIQUE(ISBN)
GO

/*3. Insert into above tables at least 3 records per table.*/

--table Books
SET IDENTITY_INSERT Books ON

INSERT INTO Books(BookID,Title,Author,ISBN,Image,Summary)
       VALUES(100,'Title1','Author1','ISBN01','Image01','Summary01')
INSERT INTO Books(BookID,Title,Author,ISBN,Image,Summary)
       VALUES(101,'Title2','Author2','ISBN02','Image02','Summary02')
INSERT INTO Books(BookID,Title,Author,ISBN,Image,Summary)
       VALUES(102,'Title3','Author3','ISBN03','Image03','Summary03')
INSERT INTO Books(BookID,Title,Author,ISBN,Image,Summary)
       VALUES(103,'My Books','Author4','ISBN04','Image04','Summary04')
SET IDENTITY_INSERT Books OFF

--table Users

INSERT INTO Users(UserID,FullName,Email,Address)
       VALUES ('User01','DoDinhViet','Viet123@gmail.com','HaNoi')
INSERT INTO Users(UserID,FullName,Email,Address)
       VALUES ('User02','VuHoaiNam','Nam789@gmail.com','HaNoi')
INSERT INTO Users(UserID,FullName,Email,Address)
       VALUES ('User03','NguyenThanTai','TaiLoc@gmail.com','HaNoi')

-- table Loans
SET IDENTITY_INSERT Loans ON

INSERT INTO Loans(LoanID,UserID,BookID,DueDate,Created,Modified)
       VALUES(1001,'User01',100,'2021/04/30','2021/03/25','2021/05/01')
INSERT INTO Loans(LoanID,UserID,BookID,DueDate,Created,Modified)
       VALUES(1002,'User02',101,'2021/04/12','2021/03/25','2021/04/25')
INSERT INTO Loans(LoanID,UserID,BookID,DueDate,Created,Modified)
       VALUES(1003,'User03',102,'2021/05/12','2021/03/25','2021/05/30')

SET IDENTITY_INSERT Loans OFF

SELECT * FROM dbo.Books
SELECT * FROM dbo.Users
SELECT * FROM dbo.Loans
DROP Table dbo.Loans

/*4. Searching for book’s titles starting with ‘M’.*/
SELECT * FROM dbo.Books B
WHERE B.Title LIKE 'M%';
GO

/*5. Create an unique index, named IX_Book_Info on Title on Books table.*/
ALTER TABLE BooKs 
ADD CONSTRAINT IX_Book_Info  UNIQUE(Title)
GO

/*6. Create a view, named v_DueDate includes Users (UserID, FullName, Email,
Address) loan overdue book (the book is limited information on loan payment
(DueDate) greater than the current time).*/



CREATE VIEW V_DueDate
AS
SELECT u.UserID,u.FullName,u.Email,u.Address FROM dbo.Users u
JOIN dbo.Loans l 
ON l.UserID = u.UserID
WHERE DATEDIFF(MONTH ,GETDATE(), l.Created ) > 0
GO

/*7. Create a view named v_TopLoan includes information of the five most borrowed
book.*/

CREATE VIEW v_TopLoan
AS
SELECT        
         
GO
drop view v_TopLoan
/*8. Create a stored procedure named sp_BookAuthor have input parameter is the name
of the author and retrived the information of this book.*/

CREATE PROCEDURE sp_BookAuthor 
AS SELECT b.Author,b.Title 
FROM  dbo.Books b 
GO

/*9. Create a stored procedure named sp_ChangeDueDate have input parameter is the
loan identity and number of days on loan extended to make up for borrowed shares.*/

CREATE PROCEDURE sp_ChangeDueDate @LoanID int
AS SELECT LoanID, DATEDIFF(DAY, Created, Modified) AS days_loan_extended
FROM Loans
WHERE LoanID = @LoanID
GO

/*10.Create a Trigger named tg_LoanModified to change column Modified on table
Loans to the present time when something of votes borrow changes.*/

CREATE TRIGGER tg_LoanModified
ON Loans
FOR UPDATE 
AS
    BEGIN
	    IF(UPDATE(Modified))
		BEGIN
		     PRINT ' Modified on table';
			 ROLLBACK TRANSACTION;
        END
END
GO