﻿USE master
GO
IF EXISTS (SELECT * FROM sys.databases WHERE Name ='CitySoftware')
DROP DATABASE CitySoftware
GO
CREATE DATABASE CitySoftware 
GO
USE CitySoftware
GO

-- Tao bang Employee 
CREATE TABLE Employee(
   EmployeeID INT PRIMARY KEY  not null,--ma nhan vien
   Name VARCHAR(100) not null,-- ten nhan vien
   Tel CHAR(11) not null,-- dt cua nhan vien
   Email VARCHAR(30) not null--dia chi email cua nhan vien
)
GO

-- tao bang Project
CREATE TABLE Project(
    ProjectID INT PRIMARY KEY , --Ma du an
	ProjectName VARCHAR(30), -- ten du an
	StartDate DATETIME , --Ngay du an bat dau
	EndDate DATETIME ,  --Ngay du an ket thuc
	Period INT ,--Tong thoi gian hoan thanh du an (tinh bang thang)
	Cost MONEY,-- Tong gia tri du an 
)
--tao bang Group
CREATE TABLE Groups(
    GroupID INT  PRIMARY KEY  ,--ma nhom lam viec
	GroupName VARCHAR(30), --ten nhom
	LeaderID INT , --ma nhan vien cua truong nhom
	ProjectID INT , -- Ma du an nhom dang lam
	CONSTRAINT fk FOREIGN KEY (ProjectID) REFERENCES Project(ProjectID)
)
GO
--tao bang GroupDetail
CREATE TABLE GroupDetail(
    GroupID INT ,--ma nhom lam viec
	EmployeeID INT, -- ma nhan vien cua nhom
	Status CHAR(20),-- trang thai lam viec cua nhan vien(da lam, dang lam, sap lam)
	CONSTRAINT GD_E FOREIGN KEY (EmployeeID) REFERENCES Employee(EmployeeID)
)
GO
/*1. Xác định thuộc tính khóa (khóa chính, khóa ngoại) và viết câu lệnh thay đổi các trường với thuộc
tính khóa vừa xác định.*/

/*2. Thêm dữ liệu cho các bảng*/
INSERT INTO Employee(EmployeeID,Name,Tel,Email) VALUES (01,'NV1','09812673543''NV1@gmail.com')
INSERT INTO Employee(EmployeeID,Name,Tel,Email) VALUES (02,'NV2','09812677843''NV2@gmail.com')
INSERT INTO Employee(EmployeeID,Name,Tel,Email) VALUES (03,'NV3','09812343543''NV3@gmail.com')
INSERT INTO Employee(EmployeeID,Name,Tel,Email) VALUES (04,'NV4','09867673543''NV4@gmail.com')
INSERT INTO Employee(EmployeeID,Name,Tel,Email) VALUES (05,'NV5','09812673589''NV5@gmail.com')
INSERT INTO Employee(EmployeeID,Name,Tel,Email) VALUES (06,'Nv6','09812673529''NV6@gmail.com')

INSERT INTO Groups(GroupID,GroupName,LeaderID,ProjectID) VALUES (01,'TEAM1',01,01)
INSERT INTO Groups(GroupID,GroupName,LeaderID,ProjectID) VALUES (02,'TEAM2',03,02)
INSERT INTO Groups(GroupID,GroupName,LeaderID,ProjectID) VALUES (03,'TEAM3',05,03)

INSERT INTO Project(ProjectID,ProjectName,StartDate,EndDate,Period,Cost) VALUES(01,'Project1','13-7-2010','13-7-2015',60,7000000000)
INSERT INTO Project(ProjectID,ProjectName,StartDate,EndDate,Period,Cost) VALUES(02,'Project2','10--12-2009','20-12-2016',84,100000000)
INSERT INTO Project(ProjectID,ProjectName,StartDate,EndDate,Period,Cost) VALUES(03,'Project3','13-7-2012','13-7-2014',24,200000000)

INSERT INTO GroupDetail(01,01,'Finish')
/*3. Viết câu lệnh truy vấn để:*/
--a. Hiển thị thông tin của tất cả nhân viên
SELECT * FROM Employee
GO
/*b. Liệt kê danh sách nhân viên đang làm dự án “Chính phủ điện tử”*/
SELECT * FROM Project WHERE ProjectID= 02
GO
/*c. Thống kê số lượng nhân viên đang làm việc tại mỗi nhóm*/
SELECT * FROM Groups WHERE GroupID = 01
SELECT * FROM Groups WHERE GroupID = 02
GO
/*d. Liệt kê thông tin cá nhân của các trưởng nhóm*/
SELECT * FROM Groups WHERE LeaderID = 01
GO
Select * from Employee