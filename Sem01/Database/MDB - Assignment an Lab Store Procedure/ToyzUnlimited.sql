﻿CREATE DATABASE ToyzUnlimited
GO
USE ToyzUnlimited
GO

--taọ bảng toys
CREATE TABLE Toys(
ProductCode VARCHAR(5) PRIMARY KEY ,
Name VARCHAR(30) ,
Category VARCHAR(30),-- Category: thể loại
Manufacturer VARCHAR(40),--manufacturer:nhà chế tạo
AgeRange VARCHAR(15),
UnitPrice MONEY,
Netweight INT, -- Netweight : Trọng lượng
QtyOnHand INT,-- QtyOnHand : Quantity 
)
GO

/*1. Tạo bảng Toys với cấu trúc giống như trên. Thêm dữ liệu (15 bản ghi) vào bảng với giá trị của
trường QtyOnHand ít nhất là 20 cho mỗi sản phẩm đồ chơi.*/

INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('A123','o to','lap ghep','a1','5 tuoi',20000,900,20)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('B234','can cau ca','bo cau ca','b1','10 tuoi',30000,1200,25)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('C345','Ak47','sung nhua','C3','13 tuoi',50000,600,30)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('D456','robot','bo lap ghep','D4','14 tuoi',100000,1200,35)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('E12','o to dieu khien','dieu khien tu xa','D5','15 tuoi',200000,1700,40)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('F23','bo game','bo game','BA','9 tuoi',40000,800,22)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('Gk12','fnfk','das','C31','9 tuoi',90000,500,34)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('kd123','jdnaskc','nnsc','C35','14 tuoi',80000,2000,50)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('Ji132','nkdvnkv','vjav','D5','8 tuoi',60000,1300,43)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('C987','jNKF','njvnsd','C323','13 tuoi',150000,1800,45)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('negg','Ak47','NFNVA','C3871','17 tuoi',66000,1400,61)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('9fa','fvs','jfafk','A832','3 tuoi',90000,1900,100)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('njc','1vsv','mvkv','nca','19 tuoi',55000,1100,98)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('nck','ncc','babv','C393','13 tuoi',80000,600,42)
INSERT INTO Toys(ProductCode,Name,Category,Manufacturer,AgeRange,UnitPrice,NetWeight,QtyOnHand) VALUES('vvn89','ndnvsdv','ncnva','C38','13 tuoi',70000,1800,52)


SELECT * FROM dbo.Toys;
GO

/*2. Viết câu lệnh tạo Thủ tục lưu trữ có tên là HeavyToys cho phép liệt kê tất cả các 
loại đồ chơi có trọng lượng lớn hơn 500g.*/

CREATE PROCEDURE HeavyToys @Netweight INT
AS 
SELECT * FROM Toys WHERE Netweight > 500 AND @Netweight = 500
GO

EXECUTE HeavyToys @Netweight = 500;


/*3. Viết câu lệnh tạo Thủ tục lưu trữ có tên là PriceIncreasecho phép tăng giá của 
tất cả các loại đồ chơi lên thêm 10 đơn vị giá.
*/

CREATE PROCEDURE PriceIncrease 
AS
UPDATE Toys SET  UnitPrice = UnitPrice +10 
GO 

EXECUTE PriceIncrease ;

/*4. Viết câu lệnh tạo Thủ tục lưu trữ có tên là QtyOnHand làm giảm số lượng đồ chơi 
còn trong của hàng mỗi thứ 5 đơn vị.*/

CREATE PROC QtyOnHand
AS
UPDATE Toys SET QtyOnHand = QtyOnHand -5
GO

EXECUTE QtyOnHand;

/*5. Thực thi 3 thủ tục lưu trữ trên.*/
EXECUTE HeavyToys @Netweight = 500;
EXECUTE PriceIncrease ;
EXECUTE QtyOnHand;