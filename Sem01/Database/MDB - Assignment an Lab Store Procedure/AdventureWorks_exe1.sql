﻿USE AdventureWorks2014
--Bài 1 Thực hiện câu lệnh và quan sát kết quả thu được
CREATE PROCEDURE Display_Customers
AS
SELECT CustomerID,AccountNumber,rowguid,ModifiedDate
FROM Sales.Customer
GO

EXECUTE Display_Customers;

--Bài 2 Thực thi một thủ tục lưu trữ mở rộng 
EXECUTE xp_fileexist 'c:\myTest.txt';

--Bài 3: Thực thi thủ tục lưu trữ hệ thống:

EXECUTE sys.sp_who