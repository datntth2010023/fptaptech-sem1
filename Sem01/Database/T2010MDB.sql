﻿USE master
GO
IF EXISTS ( SELECT * FROM sys.databases WHERE Name = 'T2010MDB')
   DROP DATABASE T2010MDB
GO
CREATE DATABASE T2010MDB
GO
USE T2010MDB
GO
-- Tạo bảng lớp học 
CREATE TABLE LopHoc(
    MaLopHoc INT PRIMARY KEY IDENTITY(1,1),-- Không bị trùng bản ghi
	TenLopHoc VARCHAR(10),
)
GO
-- Tạo khóa ngoại của bảng LopHoc
CREATE TABLE SinhVien(
    MaSV INT PRIMARY KEY IDENTITY(1,1),
	TenSV VARCHAR(50),
	MaLopHoc INT,
	CONSTRAINT ls FOREIGN KEY(MaLopHoc) REFERENCES LopHoc(MaLopHoc)
)
GO 
-- Tạo bảng students
CREATE TABLE Students(
    Student_ID INT PRIMARY KEY IDENTITY(1,1),
	Grade FLOAT,
	Create_at DATETIME
)
GO
CREATE TABLE Teachers(
     Teacher_ID INT PRIMARY KEY IDENTITY(1,1),
	 Name VARCHAR(50),
	 Student_ID INT,
	 CONSTRAINT ts FOREIGN KEY(Student_ID) REFERENCES Students(Student_ID)
)
GO
CREATE TABLE Course(
    Course_ID INT PRIMARY KEY IDENTITY(1,1),
	Name VARCHAR(50),
	Created_at DATETIME,
	Updated_at DATETIME
)
GO
CREATE TABLE StudentCourses(
    Id INT PRIMARY KEY IDENTITY(1,1),
	Student_ID INT, 
	Course_ID INT,
	CONSTRAINT ss FOREIGN KEY(Student_ID) REFERENCES Students(Student_ID),
	CONSTRAINT cs FOREIGN KEY(Course_ID) REFERENCES Course(Course_ID),
)