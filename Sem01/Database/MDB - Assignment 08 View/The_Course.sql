﻿CREATE DATABASE The_Course
GO
USE The_Course
GO

CREATE TABLE Class(
       ClassCode varchar(10) PRIMARY KEY,
	   HeadTeacher varchar(30),--tên của giáo viên chủ nhiệm lớp
	   Room varchar(30),--tên phòng học
	   TimeSlot char,--Giờ học của lớp
	   CloseDate datetime--ngày kết thúc khóa học
)
GO

CREATE TABLE Student(
       RollNo varchar(10) PRIMARY KEY,--mã sinh viên
	   ClassCode varchar(10)   --mã lớp học
	   CONSTRAINT fk_ClassCode FOREIGN KEY REFERENCES Class(ClassCode),
	   FullName varchar(30),
	   Male bit --Giới tính của sinh viên, con trai là 1, con gái là 0
	   CONSTRAINT CHK_Male CHECK(Male IN(1,0)),
	   BirthDate datetime,
	   Address varchar(30),
	   Province char(2),--tỉnh thành phố của sinh viên
	   Email varchar(30)
) 
GO

CREATE TABLE Subject(
       SubjectCode varchar(10) PRIMARY KEY,
	   SubjectName varchar(40),
	   WMark bit --Có thi lý thuyết hay không? Có: 1, Không: 0
	   CONSTRAINT CHK_WMark CHECK(WMark IN(1,0)),
	   PMark bit --Có thi thực hành hay không? Có: 1, Không: 0
	   CONSTRAINT CHK_PMark CHECK(PMark IN(1,0)),
	   WTest_per int,--hệ số điểm thi lý thuyết
	   PTest_per int --hệ số điểm thi thực hành
)
GO

CREATE TABLE Mark(
       RollNo varchar(10)
	   CONSTRAINT fk_RollNo FOREIGN KEY REFERENCES Student(RollNo),
	   SubjectCode varchar(10)
	   CONSTRAINT fk_SubjectCode FOREIGN KEY REFERENCES Subject(SubjectCode),
	   WMark float,--điểm thi lý thuyết
	   PMark float,--Điểm thi thực hành
	   Mark float,--Điểm thi trung bình
	   PRIMARY KEY (RollNo,SubjectCode)
)
GO

/*1. Chèn ít nhất 5 bản ghi cho từng bảng ở trên.*/
--bảng class
INSERT INTO Class(ClassCode,HeadTeacher,Room,TimeSlot,CloseDate) 
       VALUES('C1001A','Gv1','Class1','G','12-11-2021')
INSERT INTO Class(ClassCode,HeadTeacher,Room,TimeSlot,CloseDate) 
       VALUES('C1002A','Gv2','Class2','I','12-12-2021')
INSERT INTO Class(ClassCode,HeadTeacher,Room,TimeSlot,CloseDate) 
       VALUES('C1003A','Gv3','Class3','L','12-12-2021')
INSERT INTO Class(ClassCode,HeadTeacher,Room,TimeSlot,CloseDate) 
       VALUES('C1004A','Gv4','Class4','M','12-12-2021')
INSERT INTO Class(ClassCode,HeadTeacher,Room,TimeSlot,CloseDate) 
       VALUES('C1005A','Gv5','Class5','G','12-12-2022')
	   SELECT * FROM Class;
--bảng Student
INSERT INTO Student(RollNo,ClassCode,FullName,Male,BirthDate,Address,Province,Email)
       VALUES('St01','C1001A','A','0','1-6-2002','Ha Noi','HN','Aaaa@gmail.com')  
INSERT INTO Student(RollNo,ClassCode,FullName,Male,BirthDate,Address,Province,Email)
       VALUES('St02','C1002A','B','1','12-6-2002','Hai Phong','HP','Bbbb@gmail.com')
INSERT INTO Student(RollNo,ClassCode,FullName,Male,BirthDate,Address,Province,Email)
       VALUES('St03','C1003A','C','0','8-7-2002','Bac Ninh','BN','Cccc@gmail.com')  	   
INSERT INTO Student(RollNo,ClassCode,FullName,Male,BirthDate,Address,Province,Email)
       VALUES('St04','C1004A','D','1','10-8-2002','Nha Trang','KH','Dddd@gmail.com')
INSERT INTO Student(RollNo,ClassCode,FullName,Male,BirthDate,Address,Province,Email)
       VALUES('St05','C1005A','E','0','10-8-2002','Da Nang','DN','Eeee@gmail.com')  
	   SELECT * FROM Student;
--bảng Subject
INSERT INTO Subject(SubjectCode,SubjectName,WMark,PMark,WTest_per,PTest_per)
       VALUES('EPC','Elementary Programing with C','1','1',20,20)
INSERT INTO Subject(SubjectCode,SubjectName,WMark,PMark,WTest_per,PTest_per)
       VALUES('BSJ','BootStrap and jQuery','1','0',10,0)
INSERT INTO Subject(SubjectCode,SubjectName,WMark,PMark,WTest_per,PTest_per)
       VALUES('BNGW','Building Next Generation Web','1','0',10,0)
INSERT INTO Subject(SubjectCode,SubjectName,WMark,PMark,WTest_per,PTest_per)
       VALUES('DM','Database Management','1','1',20,20)
INSERT INTO Subject(SubjectCode,SubjectName,WMark,PMark,WTest_per,PTest_per)
       VALUES('LBEP',' Logic Building and Element Program','1','1',20,20)
	   SELECT * FROM Subject;
--Bảng mark
INSERT INTO Mark(RollNo,SubjectCode,WMark,PMark,Mark)
       VALUES ('St01','EPC',11.5,14.5,15.2)  
INSERT INTO Mark(RollNo,SubjectCode,WMark,PMark,Mark)
       VALUES ('St02','BSJ',10.5,15.5,15.9) 
INSERT INTO Mark(RollNo,SubjectCode,WMark,PMark,Mark)
       VALUES ('St03','BNGW',11.5,14.5,15.8) 
INSERT INTO Mark(RollNo,SubjectCode,WMark,PMark,Mark)
       VALUES ('St04','DM',12.5,16.5,17.2) 
INSERT INTO Mark(RollNo,SubjectCode,WMark,PMark,Mark)
       VALUES ('St05','LBEP',13.5,14.5,15.5) 
	   SELECT * FROM Mark;