﻿CREATE DATABASE Lab10
GO

/*sao lưu dữ liệu WorkOrder của cơ sở dữ liệu AdventueWorks sang cơ sở dữ liệu Lab10
vừa tạo bằng cách sử dụng những câu lệnh sau: */
USE AdventureWorks2014
SELECT * INTO Lab10.dbo.WorkOrder FROM Production.WorkOrder
GO

/*Sao dữ liệu từ bảng WorkOrder của cơ sở dữ liệu Lab10 sang bảng WorkOrderIX của 
cơ sở dữ liệu Lab10:*/

USE Lab10 SELECT * INTO WorkOrderIX FROM WorkOrder
GO

/*Xem dữ liệu ở cả hai bảng:*/

SELECT * FROM WorkOrder
SELECT * FROM WorkOrderIX
GO

/*Tại SQL Management Studio từ Menu chọn Query->Display Estimated Excution Plan. Cửa sổ
hiển thị như sau: */

/*Tạo một chỉ mục trên cột WorkOrderID của bảng WorkOrderIX bằng câu lệnh sau:*/
CREATE INDEX IX_WorkOrderID ON  WorkOrderIX(WorkOrderID)
GO

SELECT * FROM WorkOrder WHERE WorkOrderID = 72000
SELECT * FROM WorkOrderIX WHERE WorkOrderID = 72000
GO