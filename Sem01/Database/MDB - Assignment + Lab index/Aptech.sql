﻿CREATE DATABASE Aptech 
GO

--tạo bảng Classes
CREATE TABLE Classes(
ClassName CHAR(6),--tên lớp
Teacher VARCHAR(30),--tên giáo viên
TimeSlot VARCHAR(30),--giờ học
Class INT,--số của phòng lý thuyết 
Lab INT, --số của phòng học thực hành
)
GO

/*1. Tạo an unique, clustered index tên là MyClusteredIndex trên trường ClassName với
thuộc tính sau:
Pad_index = on
FillFactor =70
Ignore_ Dup_Key=on
*/


CREATE UNIQUE CLUSTERED INDEX MyClusteredIndex ON Classes(ClassName)
WITH(Pad_index = on,
FillFactor = 70,
Ignore_Dup_Key = on
);
GO

/*2. Tạo a nonclustered index tên là TeacherIndex trên trường Teacher*/


CREATE NONCLUSTERED INDEX TeacherIndex ON Classes(Teacher);
GO

/*3. Xóa chỉ mục TeacherIndex*/

DROP INDEX Classes.TeacherIndex
GO

/*4. Tạo lại MyClusteredIndexvới các thuộc tính sau:DROP_EXISTING, ALLOW_ROW_LOCKS,
ALLOW_PAGE_LOCKS= ON, MAXDOP = 2.*/
CREATE UNIQUE CLUSTERED INDEX MyClusteredIndex ON Classes(ClassName)
WITH(DROP_EXISTING = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS= ON, MAXDOP = 2)
GO

/*5. Tạo một composite index tên là ClassLabIndex trên 2 trường Class và Lab.*/
CREATE INDEX ClasslabIndex ON Classes(Class,Lab);
GO

/*6. Viết câu lệnh xem toàn bộ các chỉ mục của cơ sở dữ liệu Aptech.*/
SELECT * FROM Aptech.sys.indexes
GO