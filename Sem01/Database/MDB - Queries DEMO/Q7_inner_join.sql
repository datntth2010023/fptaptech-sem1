﻿USE FptAptechStore
GO

-- Get all data : lấy toàn bộ dữ liệu
SELECT product_name, list_price, category_id
--category: thể loại
FROM  production.products
ORDER BY
        product_name DESC;

-- INNER JOIN products vs Category
SELECT 
       product_name,
	   category_name,
	   list_price
FROM 
       production.products AS p
INNER JOIN production.categories AS c
ON c.category_id = p.category_id
ORDER BY
        product_name DESC;

/*
SELECT 
      list_col
FROM
      T1
INNER JOIN T2 ON dieu_kien_join(T2.id=T1.id)
*/
SELECT 
      product_name,
	  category_name,
	  brand_name,
	  list_price
FROM
      production.products AS p
INNER JOIN production.categories AS c 
ON c.category_id= p.category_id
INNER JOIN production.brands b ON b.brand_id = p.brand_id
ORDER BY
        product_name DESC;