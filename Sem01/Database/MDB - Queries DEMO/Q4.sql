USE FptAptechStore
GO
--GROUP BY: MIN; MAX
--SELECT * FROM production.products
--MIN
SELECT 
	product_name,
	MIN(list_price) AS min_price,
	MAX(list_price) AS max_price
FROM 
	production.products
GROUP BY
     product_name