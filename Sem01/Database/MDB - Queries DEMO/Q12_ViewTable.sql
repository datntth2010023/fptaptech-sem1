USE FptAptechStore
GO

CREATE VIEW sales.daily_sales
AS 
SELECT
            YEAR(order_date) AS y,
			MONTH(order_date) AS  m,
			Day(order_date) AS d,
			p.product_id,
			p.product_name
FROM 
            sales.orders AS o
INNER JOIN sales.order_items AS i
          ON o.order_id = i.order_id
INNER JOIN production.products AS p
          ON p.product_id = i.product_id

-- SELECT ON VIEW TABLE
SELECT * FROM sales.daily_sales 
ORDER BY
            d,m,y,product_name DESC;