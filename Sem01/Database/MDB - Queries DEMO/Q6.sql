﻿USE FptAptechStore
GO 

--GROUP BY with HAVING (Đk áp dụng trong một group)
-- WHERE đk áp dụng trong cột
--SELECT * FROM sales.order

SELECT 
      customer_id,
	  YEAR(order_date) AS year_order,
	  COUNT(order_id) AS order_count-- count:kiểm điếm
FROM
      sales.orders
GROUP BY 
      customer_id,
	  YEAR(order_date)
HAVING
      COUNT(order_id) >=2
ORDER BY
      customer_id DESC;