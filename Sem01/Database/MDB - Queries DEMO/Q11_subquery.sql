USE FptAptechStore
GO

SELECT 
       order_id, order_date
FROM
       sales.orders

SELECT 
       MAX(list_price) 
FROM
       sales.order_items

SELECT 
        order_id,
		order_date,
		(SELECT MAX(list_price) FROM sales.order_items i
		        WHERE i.order_id = o.order_id
				) AS max_price
FROM
        sales.orders o 
ORDER BY
        order_date DESC;    