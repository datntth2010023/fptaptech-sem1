﻿USE FptAptechStore
GO

--LEFT JOIN
/*
   SELECT
          select_col,...
   FROM
          T1
   LEFT JOIN T2 ON đk_join;
*/
SELECT product_name,
       order_id
FROM
       production.products  p
LEFT JOIN sales.order_items o ON o.product_id = p.product_id
AND o.order_id = 13
ORDER BY
        order_id;

--LEFT JOIN 3 TABLES
SELECT p.product_name,o.order_id,i.item_id, o.order_date
FROM 
       production.products AS p
LEFT JOIN sales.order_items AS i
     ON i.product_id = p.product_id
LEFT JOIN sales.orders AS o
     ON o.order_id = i.order_id
ORDER BY
        order_id;