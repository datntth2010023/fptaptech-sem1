USE FptAptechStore
GO

--SUM tinh tong function with GROUP BY
--AVG tinh Trung binh
--SELECT * FROM sales.order_items
SELECT 
	order_id,
		SUM(quantity * list_price *(1-discount)) AS net_value
	FROM 
	sales.order_items
	GROUP BY 
		order_id
	HAVING 
				SUM(quantity * list_price *(1-discount)) >25000

	ORDER BY
		net_value DESC ;
