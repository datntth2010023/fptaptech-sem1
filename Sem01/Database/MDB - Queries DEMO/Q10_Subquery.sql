USE FptAptechStore
GO

--SubQuery
SELECT 
       product_name,list_price
FROM
       production.products
WHERE 
       list_price >
	   (SELECT AVG(list_price) FROM production.products
	    
		WHERE brand_id IN(SELECT brand_id FROM production.brands
		      WHERE brand_name = 'Haro' OR brand_name = 'Electra'))
ORDER BY
        list_price;
