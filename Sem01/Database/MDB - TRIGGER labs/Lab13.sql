/*1. T?o m?t c? s? d? li?u c� t�n l� Lab13, sau ?� sao ch�p m?t ph?n c?u tr�c
v� d? li?u c?a b?ng Production.Product t? c? s? d? li?u AdventureWorks v�o c?
s? d? li?u Lab13.
*/
CREATE DATABASE Lab13 --taoj CSDL Lab13
GO
USE AdventureWorks2014
GO
--Sao ch�p 3 c?t ProductID, Name v� Color t? b?ng Production.Product c?a database
--AdventureWorks
--sang CSDL Lab13
SELECT ProductID, Name, Color INTO Lab13.dbo.Product FROM Production.Product
GO
USE Lab13
GO
--Hi?n th? b?ng Product v?a sao ch�p ???c
SELECT * FROM Product

/*2. T?o m?t UPDATE trigger cho b?ng Product nh?m ng?n c?n vi?c ng??i kh�c thay
??i t�n c?a s?n ph?m:*/
CREATE TRIGGER UpdateProduct
ON Product
FOR UPDATE
AS
BEGIN
IF(UPDATE(Name))
BEGIN
PRINT 'Khong duoc phep thay doi ten san pham';
ROLLBACK TRANSACTION;
END
END

/*3. Th? ti?n h�nh thay ??i t�n c?a m?t s?n ph?m nh?m ki?m tra s? ho?t ??ng
c?a trigger v?a t?o:*/
UPDATE Product SET Name = 'Cocacola' WHERE ProductID = 1