int nhap( int arr[] ){
	int i;
	for(i = 0; i< N; i++ ){
		scanf("%d", &arr[i]);
	}
}

void hienThi( const char arr[] ){
	int i;
	for(i = 0; i< N; i++ ){
		printf("%5s", arr[i]);
	}
}

void sort( char arr[] ){
	int i, j;
	for(i = 0; i< N-1; i++){
		for(j= N-1; j> i; j-- ){
			if(arr[j] < arr[j-1]){
				int tmp = arr[j];
				arr[j] = arr[j-1];
				arr[j-1] =tmp;
			}
		}
	}
}

int binarySearch( char arr[], char x ){
	int left = 0, right = N-1;
	while(left <= right){
		int mid = (left+right)/2;
		if(arr[mid] == x)
			return 1; // tim thay
		else if(arr[mid] < x) 
			left = mid+1;
		else right = mid-1;
	}
	return 0;
}

int main(){
	
	char arr[N];
	nhap(arr);
	sort(arr);
	hienThi(arr);
	char x;
	
	while(1){
		printf("\nNhap x= ");
		scanf("%s", &x);
		if(x == -1) break;
		if(binarySearch(arr, x))
			printf("\nTim thay x.");
		else printf("\nKhong tim thay x.");
	}
	
	return 0;
}