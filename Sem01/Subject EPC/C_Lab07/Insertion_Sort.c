#include <stdio.h>
#include <conio.h>
#include <string.h>

void main()
{
    char arr[100] ;
    int n;
    char temp;
    // NHAP VAO:
    printf("\n Nhap so luong phan tu :");
    scanf("%d",&n);
    for (int i = 0; i < n; i++)
    {
        printf("\n Nhap phan tu thu %d : ",i+1);
        scanf("%s",&arr[i]);
    }
    // INSERTION SORT tang
    int i,j;

    for ( j = 0; j <n ; j++)
    {
        i =j - 1;
        temp = arr[j];
        while ((i >= 0) &&(temp < arr[i]))
        {
            arr[i+1] =  arr[i];
            i--;
        }
        arr[i+1] = temp;
    }
    // HIEN THI 
    printf("\n Mang da sap xep tang dan : ");
    for (int i = 0; i < n; i++)
    {
        printf("%c\t",arr[i]);
    }
    
    getch();
}