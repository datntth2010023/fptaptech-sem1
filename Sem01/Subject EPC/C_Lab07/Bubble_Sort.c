#include <stdio.h>
#include <conio.h>

void main()
{
    int arr[100];
    int n;
    printf("\n Nhap cac phan tu : ");
    scanf("%d",&n);
    for (int i = 0; i < n; i++)
    {
        printf("\n Nhap phan tu thu %d : ",i+1);
        scanf("%d",&arr[i]);
    }
    
    for (int i = 0; i < n-1; i++)
    {
        for (int j = i +1; j < n; j++)
        {
            if (arr[i]>arr[j])
            {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }  
    }
    printf("\n Mang da sap xep : ");
    for (int i = 0; i < n; i++)
    {
        printf("%d\t",arr[i]);
    }
    
    getch();
}