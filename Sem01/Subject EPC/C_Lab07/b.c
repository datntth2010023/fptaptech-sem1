#include <stdio.h>
#include <string.h>

int main (void) {
   char string[100] ;
   char temp;

   int i, j;
   int n = strlen(string);
   printf("\n Nhap so luong phan tu :");
   scanf("%d",&n);
   for (int i = 0; i < n; i++)
    {
        printf("\n Nhap phan tu thu %d : ",i+1);
        scanf("%s",&string[i]);
    }
   printf("Chuoi truoc khi sap xep: %s \n", string);

   for (i = 0; i < n-1; i++) {
      for (j = i+1; j < n; j++) {
         if (string[i] > string[j]) {
            temp = string[i];
            string[i] = string[j];
            string[j] = temp;
         }
      }
   }
   
   printf("\nChuoi sau khi sap xep %s \n", string);
   return 0;
}