#include <stdio.h>
#include <malloc.h>
#include <conio.h> 

int main()
{
    int *ptr, n, i;
    int low = 0, high = 9, mid;
    printf("\n Nhap so phan tu cua mang: ");
    scanf("%d",&n);
    ptr = (int *)malloc(n * sizeof(int));
    printf("\n Nhap cac gia tri cua mang: \n");
    for ( i = 0; i < n; i++)
    {
        printf("\n Phan tu thu %d : %d",i +1);
        scanf("%d",ptr + i);
    }
    printf("\nHien thi cac phan tu cua mang: \n");
    for ( i = 0; i < n; i++)
    {
        printf("Phan tu thu %d: %d\n", i + 1, *(ptr + i));
    }
    int GiaTri, TimThay = 0;
    printf("Nhap 1 gia tri nguyen: ");
    scanf("%d", &GiaTri);
    while (low <= high)
    {
        mid = (low + high)/2;
        if (ptr[mid]  == GiaTri)
        {
            printf("\n Gia Tri %d duoc tim thay o vi tri %d cua mang!", GiaTri, mid );
            TimThay = 1;
            break;
        }
        else if (ptr[mid] > GiaTri)
        {
            high = mid -1;
        }
        else
        {
            low = mid + 1;
        }
    }
    if(TimThay == 0)
        printf("\nGia Tri %d khong duoc tim thay trong mang!", GiaTri);
    return 0;
}