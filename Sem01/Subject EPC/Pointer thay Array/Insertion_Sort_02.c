#include <stdio.h>
#include <malloc.h>

int main()
{
int *ptr, n;
int i, j, tmp;
printf("\n Nhap so phan tu cua mang: ");
scanf("%d",&n);
ptr = (int *)malloc(n * sizeof(int));
printf("\n Nhap cac gia tri cua mang: \n");
for ( i = 0; i < n; i++)
{
    printf("\n Phan tu thu %d : %d",i +1);
    scanf("%d",ptr + i);
}
printf("\nHien thi cac phan tu cua mang: \n");
for ( i = 0; i < n; i++)
{
    printf("Phan tu thu %d: %d\n", i +1, *(ptr + i));
}
for (i = 1; i < n; ++i)
{
    tmp = *(ptr +i);
    j = i - 1;
    while (j >= 0 && tmp < *(ptr +j))
    {
        *(ptr + j + 1) = *(ptr + j);
        j--;
    }
    *(ptr +j + 1) = tmp;
}

printf("Hien thi mang da dc sap xep: \n");

for (i = 0; i < 5; i++)
{
printf("%d\t", *(ptr + i));
}
printf("\n");
return 0;
}