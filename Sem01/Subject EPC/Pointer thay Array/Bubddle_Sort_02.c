#include <stdio.h>
#include <malloc.h>

int main()
{
    int *ptr, n;
    int i, j, tmp;
    printf("\n Nhap so phan tu cua mang: ");
    scanf("%d",&n);
    ptr = (int *)malloc(n * sizeof(int));
    printf("\nNhap cac gia tri cua mang: \n");
    for ( i = 0; i < n; i++)
    {
        printf("\n Phan tu thu %d: %d",i + 1);
        scanf("%d",ptr +i);
    }
    printf("\n Hien thi cac phan tu cua mang: \n");
    for ( i = 0; i < n; i++)
    {
        printf("\n Phan tu thu%d: %d", i + 1, *(ptr + i));
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n - i - 1; ++j)
        {
            if (*(ptr + j) > *(ptr + j + 1))
            {
                tmp = *(ptr + j);
                *(ptr + j)=*(ptr + j + 1);
                *(ptr + j + 1) = tmp;
            }
        }
    }
    printf("Hien thi mang da dc sap xep: \n");

    for (i = 0; i < n; i++)
    {
        printf("%d\t", *(ptr + i));
    }
    printf("\n");
    return 0;
}