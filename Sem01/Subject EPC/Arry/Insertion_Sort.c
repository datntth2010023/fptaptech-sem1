#include <stdio.h>

int main()
{
int ary[5] = {10, 3, 21, 6, 1};
int i, j, tmp;

for (i = 1; i < 5; ++i)
{
tmp = ary[i];
j = i - 1;
while (j >= 0 && tmp < ary[j])
{
ary[j + 1] = ary[j];
j--;
}
ary[j + 1] = tmp;
}

printf("Hien thi mang da dc sap xep: \n");

for (i = 0; i < 5; i++)
{
printf("%d\t", ary[i]);
}
printf("\n");
return 0;
}