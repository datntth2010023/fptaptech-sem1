#include <stdio.h>
#include <conio.h>

int tinhBCChuong(int n);

int tinhBCChuong(int n){
    int tich;
    for (int i = 1; i <= 10; i++)
    {
        tich = i * n;
        printf(" %d x %d = %d \n", i,n, tich);
        printf("\n");
    }
}
int main()
{
    int n;
    int choice;
    do
    { printf("\n Moi ban chon TU 1 den 10 trong MENU:");
        printf("\n __________MENU___________");
        printf("\n 1. Bang cuu chuong cua 1");
        printf("\n 2. Bang cuu chuong cua 2");
        printf("\n 3. Bang cuu chuong cua 3");
        printf("\n 4. Bang cuu chuong cua 4");
        printf("\n 5. Bang cuu chuong cua 5");
        printf("\n 6. Bang cuu chuong cua 6");
        printf("\n 7. Bang cuu chuong cua 7");
        printf("\n 8. Bang cuu chuong cua 8");
        printf("\n 9. Bang cuu chuong cua 9");
        printf("\n 10. Thoat");
        printf("\n _________________________");
        printf("\n Nhap choice : ");
        scanf("%d", &choice);   
        switch (choice)
        {
        case 1:
            n = 1;
            printf("\n Bang cuu chuong cua 1 : %d", tinhBCChuong(n));
            break;
        case 2:
            n = 2;
            printf("\n Bang cuu chuong cua 2 : %d", tinhBCChuong(n));
            break;
        case 3:
            n = 3;
            printf("\n Bang cuu chuong cua 3 : %d", tinhBCChuong(n));
            break;
        case 4:
            n = 4;
            printf("\n Bang cuu chuong cua 4 : %d", tinhBCChuong(n));
            break;    
        case 5:
            n = 5;
            printf("\n Bang cuu chuong cua 5 : %d", tinhBCChuong(n));
            break;
        case 6:
            n = 6;
            printf("\n Bang cuu chuong cua 6 : %d", tinhBCChuong(n));
            break;
        case 7:
            n = 7;
            printf("\n Bang cuu chuong cua 7 : %d", tinhBCChuong(n));
            break;
        case 8:
            n = 8;
            printf("\n Bang cuu chuong cua 8 : %d", tinhBCChuong(n));
            break;
        case 9:
            n = 9; 
            printf("\n Bang cuu chuong cua 9 : %d", tinhBCChuong(n));
            break; 
        case 10:
            break;                       
        default:
            printf("\n Ban chon sai.Vui long nhap lai tu 1 den 10 de su dung MENU ");
            break;
        }
    } while (choice != 10);
    return 0;
}