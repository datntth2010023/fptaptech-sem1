#include <stdio.h>
int power(int a, int n);
int main(){
    int a, n;
    printf("\n Nhap so nguyen a: ");
    scanf("%d",&a);
    printf("\n Nhap so nguyen n: ");
    scanf("%d",&n);
    int GiaTri = power(a, n);
    printf("\n Gia tri cua  a^n la: %d",GiaTri );
    return 0;
}

int power(int a, int n){
    int GiaTri;
    if (n == 1)
    {
        GiaTri = a;
    }else
    {
        int temp = power(a, n/2);
        if (n % 2 == 0)
        {
            GiaTri =temp * temp;
        }else{
            GiaTri = temp * temp *a;
        }
    }
    return GiaTri;
}