#include <stdio.h>
#include <math.h>

int inputNumber();
void checkNumber(int num);
int main(){
    int num;
    printf("\n Nhap vao so nguyen num : ");
    scanf("%d",&num);
    printf("\n So nguyen vua nhap la: %d", num);
    checkNumber(num);//goi ham va truyen gia tri vao cho ham
    return 0;
}

void  checkNumber(int num){
    int flag = 0;
    if(num < 2){
            printf("\n%d khong phai so nguyen to", num);
            return 0;
        }
    for(int i = 2; i <= sqrt(num); i++){
        if(num % i == 0){
            flag = 1;
        }
    }
    if (flag == 0){
        printf("\n%d la so nguyen to! ", num);
    }else{
        printf("\n%d khong phai so nguyen to! ", num);
    }
}