#include <stdio.h>
#include <malloc.h>

int main()
{
    int *mang, n, i;
    printf("Moi nhap so phan tu cua mang: ");
    scanf("%d", &n);
    mang = (int *)malloc(n * sizeof(int));
    printf("\nNhap cac gia tri cho mang: \n");
    for ( i = 0; i < n; i++)
    {
        printf("Phan tu thu %d: ", i + 1);
        scanf("%d", mang + i);
    }
    printf("\nHien thi cac phan tu cua mang: \n");
    for ( i = 0; i < n; i++)
    {
        printf("Phan tu thu %d: %d\n", i +1, *(mang + i));
        //toan tu * lay ra gia tri cua cac phan tu mang
    }
    //tim va liet ke cacphna tu co gia tri la so nguyen
    //so nguyen to la so chi co cac uoc la 1 va chinh no
    for ( i = 0; i < n; i++)
    {
        int dem = 0;// dem so luong cac so nguyen to
        int check = 1;//bien cuc bo, tra ve 1 neu so nhap vao la so nguyen to, 0 neu la boi so
        int pt = *(mang + i);//bien pt duoc gan boi gia tri cua phan tu cua mang o vi tri i 
        int j;

        //vong lap for kiem tra 1 phan tu cua mang co la so nguyen to hay khong
        for ( j = 2; i < pt; j++)
        {
            if (pt%j == 0)
            {
                check = 0; //neu pt co uoc so khac thi gia tri cua bien checkbi thay doi = 0
                break;
            }
        }
        if (check == 1)
        // pt khong co uoc so khac ngoai 1 va chinh no thi gia tri cua bien check duoc bao toan = 1
        {
            dem++;
            printf("\nSo nguyen to thu %d: %d", dem, pt);
        }
    }
    return 0;
}