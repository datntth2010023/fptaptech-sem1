#include <stdio.h>
int main()
{
    int a, b;
    int *ptr_a;
    int *ptr_b;
    ptr_a = &a;
    ptr_b = &b;
    *ptr_a = 3;
    *ptr_b = 5;
    printf("\n Gia tri cua a va b truoc khi trao doi: a = %d, b = %d\n", *ptr_a, *ptr_b);
    printf("\n Dia chi vung nho cua 2 bien a la: %u, cua b la: %u\n", ptr_a, ptr_b);
    int tmp = *ptr_a;
    *ptr_a = *ptr_b;
    *ptr_b = tmp;
    printf("\nGia tri cua a va b sau khi trao doi: a = %d, b = %d\n", *ptr_a, *ptr_b);
    printf("Dia chi vung nho cua 2 bien a la: %u, cua b la: %u\n", ptr_a, ptr_b);

    return 0;
}