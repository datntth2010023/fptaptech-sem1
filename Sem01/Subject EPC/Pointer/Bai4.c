#include <stdio.h>
#include <malloc.h>
int main()
{
    int *a, *b, *c, i;
    a = (int *)malloc(5 *  sizeof(int));
    b = (int *)malloc(5 *  sizeof(int));
    c = (int *)malloc(5 *  sizeof(int));

    printf("Nhap cac phan tu cho mang a: ");
    for ( i = 0; i < 5; ++i)
    {
        printf("\nPhan tu thu %d: ", i + 1);
        scanf("%d", a + i);
    }

     printf("\nNhap cac phan tu cho mang b: \n");
    for ( i = 0; i < 5; ++i)
    {
        printf("\nPhan tu thu %d: ", i + 1);
        scanf("%d", b + i);
    }
    //tinh tong cac phan tu tuong ung cua 2 mang a, b va luu ket qua vao mang c
    for ( i = 0; i < 5; ++i)
    {
        *(c + i) = *(a + i) + *(b + i);
    }
    printf("\nIn ra cac phan tu cua mang a: \n");
    for ( i = 0; i < 5; ++i)
    {
        printf("a[%d] = %d\n", i, *(a + i));
    }
    printf("\nIn ra cac phan tu cua mang b: \n");
    for ( i = 0; i < 5; ++i)
    {
        printf("b[%d] = %d\n", i, *(b + i));
    }
    printf("\nIn ra cac phan tu cua mang c: \n");
    for ( i = 0; i < 5; ++i)
    {
        printf("c[%d] = %d\n", i, *(c + i));
    }
    
    return 0;
}