#include <stdio.h>
#include <malloc.h>

int main()
{
    int *ptr;
    ptr = (int *)malloc(5 * sizeof(int));// cap phat bo nho cho mang
    int i;
    printf("Nhap cac phan tu cho mang; \n");
    for ( i = 0; i < 5; i++)
    {
        printf("Phan tu thu %d: ", i + 1);
        scanf("%d", ptr + i);
    }
    printf("\nHien thi cac phan tu cua mang: \n");
    for ( i = 0; i < 5; i++)
    {
        printf("Phan tu thu %d: %d\n ", i + 1, *(ptr + i));
    }
    free(ptr);
    return 0;
}