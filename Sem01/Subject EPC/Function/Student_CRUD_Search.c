#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
int i = 0;
struct info
{
    char firstname[50];
    char lastname[50];
    int id;
    float cgpa;      //diem trung binh mon
    int courseid[5]; //ma mon hoc => mot sinh vien co nhieu mon hoc.
} st[50];            //struct student st[50];

void add_student()
{

    printf("\n Ham them sv");
    printf("\n First Name :");
    scanf("%s", st[i].firstname);
    printf("\n Last name :");
    scanf("%s", st[i].lastname);
    printf("\n Enter student id :");
    scanf("%d", &st[i].id);
    printf("\n Enter the course :");
    scanf("%f", &st[i].cgpa);
    printf("\n Enter the course id : ");
    for (int j = 0; j < 5; j++)
    {
        scanf("%d", &st[i].courseid[j]);
    }
    i = i + 1;
} //search

void find_by_id()
{
    printf("\n Ham tim sv qua id ");
    int x = 1;
    printf("\n Enter the id to find : ");
    scanf("%d", &x);
    for (int j = 0; j < i; j++)
    {
        if (x == st[j].id)
        {
            printf("\n The student details are ");
            printf("\n The first name is : %s", st[j].firstname);
            printf("\n The last name  is : %s", st[j].lastname);
            printf("\n The course GPA is : %f", st[j].cgpa);
            printf("\n Course id : ");
            for (int k = 0; k < 5; k++)
            {
                printf("\n The course id are:%d ", st[j].courseid[k]);
            }
            break;
        }
        else //id khong ton tai
        {
            printf("The student can not found!\n "); // found : tim kiem
            break;
        }
    }

} //search

void find_by_firstname()
{
    printf("\n Ham tim sv qua firstname ");
    char firstname[30];
    printf("\n Enter the firstname to find : ");
    scanf("%s", &firstname);
    for (int j = 0; j < i; j++)
    {
        if (strcmp(st[j].firstname, firstname) == 0)
        {
            printf("\n The student details are : ");
            printf("\n The id is : %d ", st[j].id);
            printf("\n The first name is : %s ", st[j].firstname);
            printf("\n The last name is : %s ", st[j].lastname);
            printf("\n The course GPA is : %f ", st[j].cgpa);
            printf("\n The course id : ");
            for (int k = 0; k < 5; k++)
            {
                printf("\n The course id are : %d", st[j].courseid[k]);
            }
            break;
        }
        else
        {
            printf("The First name not found!\n");
        }
    }

} //search

void find_by_lastname()
{
    printf("\n Ham tim sv qua lastname  ");
    char lastname[30];
    printf("\n Enter the lastname to find : ");
    scanf("%s", &lastname);
    for (int j = 0; j < i; j++)
    {
        if (strcmp(st[j].lastname, lastname) == 0)
        {
            printf("\n The student details are : ");
            printf("\n The id is : %d ", st[j].id);
            printf("\n The first name is : %s ", st[j].firstname);
            printf("\n The last name is : %s ", st[j].lastname);
            printf("\n The course GPA is : %f ", st[j].cgpa);
            printf("\n The course id : ");
            for (int k = 0; k < 5; k++)
            {
                printf("\n The course id are : %d", st[j].courseid[k]);
            }
            break;
        }
        else
        {
            printf("The Last name not found!\n ");
        }
    }

} //search

void find_by_courseid()
{
    printf("\n Ham tim sv qua courseid ");
    int id;
    printf("\n Enter the course id to find : ");
    scanf("%d", &id);
    int c = 0;
    for (int j = 0; j < i; j++)
    {
        for (int d = 0; d < 5; d++)
        {
            if (id == st[j].courseid[d])
            {
                printf("\n The student details are : ");
                printf("\n The id is : %d ", st[j].id);
                printf("\n The first name is : %s ", st[j].firstname);
                printf("\n The last name is : %s ", st[j].lastname);
                printf("\n The course GPA is : %f", st[j].cgpa);
                c = 1;
                break;
            }
            else
            {
                printf("The course not Found!\n");
                break;
            }
        }
    }

} //search

void read_all_student()
{
    printf("\n Ham in ra danh sach sv ");
    printf("\n The total number of Student is %d", i); //total : toan bo, tat ca
    printf("\n You can have a max of 50 students\n");
    printf("\n you can have %d more student", 50 - i); // more: hon the nua,hon,nhieu hon

} //read all student R

//Delete student D
void delete_student()
{
    int a;
    printf("\n Enter your id you want to delete : ");
    scanf("%d", &a);
    for (int j = 0; j < i; j++)
    {
        if (a == st[j].id)
        {
            for (int k = 0; k < 49; k++)
            {
                st[k] = st[k + 1];
                i--;
            }
        }
        printf("\n The student have been removed successfully ");
    }
}

//update student U => sua thong tin
void update_student()
{
    printf("\n Enter the student id to update : ");
    int x;
    scanf("%d", &x);
    for (int j = 0; j < i; j++)
    {
        if (st[j].id == x)
        {
            printf("1. Update first name\n"
                   "2. Update last name\n "
                   "3. Update student id no.\n"
                   "4. Update CGPA\n"
                   "5. Update courses id\n");
            int z;
            scanf("%d", &z);
            switch (z)
            {
            case 1:
                printf("\n Enter the new first name :");
                scanf("%s", st[j].firstname);
                break;
            case 2:
                printf("\n Enter the new last name :");
                scanf("%s", st[j].lastname);
                break;
            case 3:
                printf("\n Enter the new id student :");
                scanf("%d", &st[j].id);
                break;
            case 4:
                printf("\n Enter the new Courese GPA :");
                scanf("%f", &st[j].cgpa);
                break;
            case 5:
                printf("\n Enter the new courses ");
                scanf("%d%d%d%d%d", &st[j].courseid[0], &st[j].courseid[1], &st[j].courseid[2], st[j].courseid[3], st[j].courseid[4]);
                break;

                printf("\n UPDATE SUCCESSFULLY"); //SUCCESSFULLY: thanh cong
            }
        }
    }
}
int main()
{
    int choice, count;
    while (1)
    {
        printf("\n ___________________MENU_____________________");
        printf("\n 1.    Add student details ");
        printf("\n 2.    Find the student by Id ");
        printf("\n 3.    Find the student by firstname ");
        printf("\n 4.    Find the student by lastname ");
        printf("\n 5.    Find the student by courseid ");
        printf("\n 6.    Read all The student ");
        printf("\n 7.    Delete student by id ");
        printf("\n 8.    Update student by id ");
        printf("\n 9.    Exit ");
        printf("\n _____________________________________________");
        printf("\n Enter the choice : ");
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
            add_student();
            break;
        case 2:
            find_by_id();
            break;
        case 3:
            find_by_firstname();
            break;
        case 4:
            find_by_lastname();
            break;
        case 5:
            find_by_courseid();
            break;
        case 6:
            read_all_student();
            break;
        case 7:
            delete_student();
            break;
        case 8:
            update_student();
            break;
        case 9:
            exit(0);
            break;
        default:
            break;
        }
    }

    return 0;
}