#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct student // ten cau cau truc aka mang
{
    char *name;//char name[50];
    int roll_number;
    int age;
    double marks;
};
int main()
{
    int n = 5, i = 0;
    struct student s[n];
    s[0].roll_number = 1;
    s[0].name = "HaiT2010M";
    s[0].age = 19;
    s[0].marks = 15;

    s[1].roll_number = 1;
    s[1].name = "PhuongT2010M";
    s[1].age = 20;
    s[1].marks = 10.5;

    printf("\n Danh sach sv : ");
    for ( i = 0; i < 2; i++)
    {
        printf("\n name = %s", s[i].name);
        printf("\n RollNumber = %d", s[i].roll_number);
        printf("\n Age = %d",s[i].age);
        printf("\n Marks = %.2f",s[i].marks);
    }
    
    return 0;
}
