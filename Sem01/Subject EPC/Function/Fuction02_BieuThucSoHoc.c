#include <stdio.h>

int conghaiso(int a, int b);
int truhaiso(int a, int b);
int nhanhaiso(int a, int b);
float chiahaiso(int a, int b);

int main(){
    //goi ham
    int kqcong = conghaiso(3,4);//cau return se tra kq ve day
    int kqtru = truhaiso(10,9);
    int kqnhan = nhanhaiso(5,6);
    float kqchia = chiahaiso(15,5);
    printf("\nCong: %d, Tru: %d, Nhan: %d, Chia: %f", kqcong, kqtru, kqnhan,kqchia);
    //printf("\nKet qua phep cong: %d",kqcong);
    return 0;
}
//trien khai
int conghaiso(int a, int b){
    int kq;
    kq = a + b;
    return kq;//return kq co kieu du lieu = kieu du lieu cua ham
}
int truhaiso(int a, int b){
    int kq;
    kq = a - b;
    return kq; 
}
int nhanhaiso(int a, int b){
    int kq;
    kq = a* b;
    return kq;
}
float chiahaiso(int a, int b){
    int kq;
    kq = a / b;
    return kq;
}
void Tinhtoan(){
    int kq = conghaiso(30,11);
    printf("\n%d",kq * 100);
    getch();
}