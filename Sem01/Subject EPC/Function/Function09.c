#include <stdio.h>
void sortArray(int n, int *ptr);
int main(){
    int n = 6;
    int arr[] = {0,35,23,12,9,45};
    sortArray(n,arr);
    return 0;
}
void sortArray(int n, int *ptr)
{
    int i, j ,t;
    for ( i = 0; i < n; i++)
    {
        for ( j = i + 1; i < n; j++)
        {
            //if(ptr[j]< ptr[i])
            if (*(ptr + j)<*(ptr + i))
            {
                t = *(ptr + j);
                *(ptr + j) = *(ptr + i);
                *(ptr + i) = t;
            }
        }
    }
    //in ra kq
    for ( i = 0; i < n; i++)
    {
        printf("%d",*(ptr +i));
    }
    
}