#include <stdio.h>
void display(int age1, int age2);

int main(){
    int ageArray[] = {2,8,4,16};
    display(ageArray[1], ageArray[2]);
    return 0;
}

void display(int age1, int age2){
    printf("\n%d la tuoi age1 ", age1);
    printf("\n%d la tuoi age2 ", age2);
}