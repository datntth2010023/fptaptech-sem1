#include <stdio.h>

int isPrime(int number);

int main()
{
    int m, n;

    printf("Nhap vao cac so : \n");
    printf("+ n: ");
    scanf("%d", &n);
    printf("+ m: ");
    scanf("%d", &m);
    printf("\n hien thi so nguyen to tu %d den %d:\n", n, m);

    for (int i = n; i <= m; i++)
    {
        if (isPrime(i))
        {
            printf("\n%d\n", i);
        }
    }
    return 0;
}

int isPrime(int number)
{
    if (number < 2)
    {
        return 0;
    }

    for (int i = 2; i < number; i++)
    {
        if (number % i == 0)
        {
            return 0;
        }
    }
    return 1;
}