#include <stdio.h>
#include <math.h>

int isPrime(int n)
{
	int i;
	if (n < 2) return 0;
	for (i = 2; i <= sqrt(n); i++)
	{
		if (n % i == 0) return 0;
	}
	return 1;
}


int main()
{
	int m, n, count = 0;
	while (1)
	{
		printf("Nhap m va n: ");
		scanf("%d%d", &m, &n);
		if (m < n) break;
	}
	for (int i = m; i <= n; i++)
	{
		if (isPrime(i)) count++;
	}
	printf("Co tong cong %d so nguyen to\n", count);
	for (int i = m; i <= n; i++)
	{
		if (isPrime(i)) count++;
		printf(" %d ",i);
	}
	

	
	
	return 0;
}
